 //
//  HomeViewController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/21/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import GoogleMaps
import AVFoundation

class HomeViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate{
    
    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    @IBOutlet var menuBarBT: UIBarButtonItem!
    @IBOutlet weak var solititudesTB: UITableView!
    @IBOutlet weak var barraMenu: UIToolbar!
    @IBOutlet weak var lblNombre1: UILabel!
    @IBOutlet weak var lblNombre2: UILabel!
    @IBOutlet weak var imgBarra: UIImageView!
    @IBOutlet weak var sinSolicitudesVI: UIView!
    @IBOutlet weak var sinConexion: UIView!
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    
    var listaSolicitud : NSArray! = []
    
    var usuarioBean:UsuarioBean!
    var solicitudBean:SolicitudVerificacionBean!
    
    let locationManager = CLLocationManager()
    var ubicacionCell : NSIndexPath = NSIndexPath()
    //var actualLocation = CLLocation()
    var valorEventoActual = ""
    //var codigoSolicitud = ""
    
    var cache:NSCache<NSString, UIImage>!
    var escucharAudio = false
    
    var prueba = 0
    
    var estadoAudio = true
    
    var corriendoListado = false
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicializar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.hideActivityIndicator(uiView: self.view)
       // NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.terminoAudio(_:)), name: , object: self.audioPlayer)
        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)

        
         
    }
    override func viewWillDisappear(_ animated: Bool) {
       // NotificationCenter.default.removeObserver(self)
        self.corriendoListado = false
    }
 
    override func viewDidAppear(_ animated: Bool) {
        
        barraMenu.frame = CGRect(x: 5, y: 20, width: self.view.frame.size.width - 5, height: 44)
        
//        //barraMenu.frame = CGRectMake(0, 20, self.view.frame.size.width - 0, 44)
        barraMenu.sizeToFit()

        
        self.hideActivityIndicator(uiView: self.view)
        
        //self.view.addSubview(barraMenu)
        
        self.setearDatos()
        
    }
    

    override func viewDidDisappear(_ animated: Bool) {
        
        self.locationManager.stopUpdatingLocation()
        
        self.stopAudio()
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        
        self.solititudesTB.reloadData()
    }
    // MARK: - Methods
    
    func inicializar()
    {
        
        //self.audioIV.animationImageSecuence()
        //self.parlanteBT.imageView?.animationImageSecuence();
        
        let image1:UIImage = UIImage(named: "ic_pause")!
        //let image2:UIImage = UIImage(named: "ic_audio2")!
        //let image3:UIImage = UIImage(named: "ic_audio3")!
        self.parlanteBT.setImage(image1, for: .normal)
        //self.parlanteBT.imageView!.animationImages = [image1, image2, image3]
        //self.parlanteBT.imageView!.animationDuration = 1.5
        //self.parlanteBT.imageView!.startAnimating()
        //estadoAudio = true
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        self.lblNombre1.text = "Hola \(self.usuarioBean!.UseNom!):"
        self.lblNombre2.text = "Hola \(self.usuarioBean!.UseNom!):"
        
        self.sinSolicitudesVI.isHidden = true
        self.solititudesTB.isHidden = true
        self.sinConexion.isHidden = true
        
        
        self.cache = NSCache()
        
        self.solititudesTB.delegate = self
        self.solititudesTB.dataSource = self
        self.solititudesTB.separatorColor = UIColor.clear
        
        if (OriginData().hasConnectivity())
        {
            self.sinConexion.isHidden = true
            
            if (self.validarDatosSolicitudEnProceso())
            {
                self.solicitudBean = nil
                
                GlobalVariables.sharedManager.infoSolicitudVerificacionBean = self.solicitudBean
                
                prueba = 1
            }
            else
            {
                barraMenu.frame = CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: 44)
                barraMenu.sizeToFit()
                self.iniListarSolicitudVerificacion()
                
            }
        }
        else
        {
            self.sinConexion.isHidden = false
            
            
            barraMenu.frame = CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: 44)
            barraMenu.sizeToFit()
        }

        
    }
    
    func terminoAudio(myNotification: NSNotification){
        print("CAMBIOBOTON")
    }
    
    
    func setearDatos()
    {
        if prueba == 1 {
            self.delegate?.changeViewController(menu: LeftMenu.Alertas)
        }
        else
        {
            if self.corriendoListado == false {
                self.iniListarSolicitudVerificacion()
            }
        }
        
        
        UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0

    }
    
    func validarDatosSolicitudEnProceso() -> Bool
    {
        let empresaTipDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
        let empresaNroDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
        let solicitudCodigo = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_CODIGO)
        let solicitudCodigoEmp = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
        
        

        
        if (empresaTipDoc != "" && empresaNroDoc != "" && solicitudCodigo != "" && solicitudCodigoEmp != "")
        {
            return true
        }
        
        return false
    }
    
    func validarLocalizacion() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.denied)
            {
                let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado. ¿Desea ir al menú de configuración?", preferredStyle: .alert)
                
                alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                    
                    UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!);
                })
                alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                
                self.present(alertaLocalizacion, animated: true, completion: nil)
                
                return false
            }
            else if (status == CLAuthorizationStatus.notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == CLAuthorizationStatus.authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado. ¿Desea ir al menú de configuración?", preferredStyle: .alert)
            
            alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                
                UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
            })
            alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alertaLocalizacion, animated: true, completion: nil)
            
            return false
        }
    }
    
    @objc func seleccionarSolicitud(sender: AnyObject) {
        
        self.valorEventoActual = ""
        
        if (!self.validarLocalizacion())
        {
            return
        }
        
        //Cargamos el tag del boton previamente asignado al boton para identificar la celda
        let buttonRow = sender.tag!
        
        self.solicitudBean = self.listaSolicitud[buttonRow] as! SolicitudVerificacionBean
        
        GlobalVariables.sharedManager.infoSolicitudVerificacionBean = self.solicitudBean
        
        switch self.solicitudBean.estado! {
        case "PEN":
            
            switch self.solicitudBean.ultimaOpcion! {
            case "0":
                
                self.valorEventoActual = "EMPSOL"
                
                self.iniInsertarEvento()
                
                break
            case "1", "2":
                
                self.valorEventoActual = "CONRUT"
                
                self.iniInsertarEvento()
                
                break
            case "3":
                
                self.delegate?.changeViewController(menu: LeftMenu.Fotos)
                
                break
            case "4":
                
                //self.delegate?.changeViewController(LeftMenu.Fotos)
                self.delegate?.changeViewController(menu: LeftMenu.InicioVerificacion)
                
                break
            default:
                break
            }

            break
            
        case "EPR":
            
            self.delegate?.changeViewController(menu: LeftMenu.Alertas)
            break
            
        case "COM":
            
            break
            
        case "INC":
            
            let nroAlerta: Int = Int(self.solicitudBean.nroAlerta!)!
            
            if (nroAlerta == 3)
            {
                self.delegate?.changeViewController(menu: LeftMenu.ConfirmarDomicilio)
            }
            else
            {
                
                //self.delegate?.changeViewController(LeftMenu.Fotos)
                
                self.delegate?.changeViewController(menu: LeftMenu.InicioVerificacion)
            }
            
            break
            
        case "CAN":
            
            break
            
        default:
            break
        }
    }
    
    // MARK: - Actions
    
    @IBAction func accionMenu(sender: AnyObject) {
        
        self.slideMenuController()?.openLeft()
    }
    
    var intentosLog: Int = 0
    @IBAction func accionTitulo(sender: AnyObject) {
        
        self.intentosLog += 1
        
        if (self.intentosLog == 5)
        {
            self.intentosLog = 0
            delegate?.changeViewController(menu: LeftMenu.TextLog, backTo: LeftMenu.Home)
        }
    }
    
    @IBAction func accionReintentar(sender: AnyObject) {
        
        //self.setearDatos()
    }
    
    // MARK: - WebServices Methods
    
    func iniListarSolicitudVerificacion(){
        
        self.showActivityIndicator(uiView: self.view)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.endListarSolicitudVerificacion(notification:)), name:NSNotification.Name(rawValue: "endListarSolicitudVerificacion"), object: nil)

        print(usuarioBean!.UseSeIDSession! as String)
        
        let parametros = [
            "usuario" : usuarioBean!.UseNroDoc! as String,
            "sesionId" : usuarioBean!.UseSeIDSession! as String,
            "plataforma" : "IOS"
        ]
        
        
        OriginData.sharedInstance.listarSolicitudVerificacion(notificacion: "endListarSolicitudVerificacion", parametros: parametros as NSDictionary)
    }
    
    @objc func endListarSolicitudVerificacion(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as? SolicitudVerificacionBean
        self.corriendoListado = true
        
        if (data!.error.count == 0)
        {
            self.sinSolicitudesVI.isHidden = true
            self.solititudesTB.isHidden = false
            
            //Cargando Listado para tableview
            self.listaSolicitud = data!.result as NSArray
            
            self.solititudesTB.reloadData()
            
            self.validarLocalizacion()
            
            self.escucharAudio = false
            
            for i in 0 ..< listaSolicitud.count {
                let item: SolicitudVerificacionBean! = (self.listaSolicitud[i] as! SolicitudVerificacionBean)
                
                switch item.estado! {
                case "PEN":
                    self.escucharAudio = true
                    break
                case "EPR":
                    self.escucharAudio = true
                    break
                default:
                    break
                }
            }
            
            if (self.escucharAudio)
            {
                self.playAudio(nombreAudio: nombreAudio.A1y2)
                
            }
            else
            {
                self.playAudio(nombreAudio: nombreAudio.A1)
            }
        }
        else
        {
            self.sinSolicitudesVI.isHidden = false
            self.solititudesTB.isHidden = true
            
            let errorBean: ErrorBean = data!.error[0] as! ErrorBean
            
            self.mostrarAlertaErrorWS(codigo: errorBean.code, mensaje: errorBean.message)
            
            self.playAudio(nombreAudio: nombreAudio.A1)
            
        }
        
    }
    
    func iniInsertarEvento(){
        
        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.endInsertarEvento(notification:)), name:NSNotification.Name(rawValue: "endInsertarEvento"), object: nil)
        
        let parametros = [
            "usuario"   :  usuarioBean!.UseNroDoc! as String,
            "sesionId"  :  usuarioBean!.UseSeIDSession! as String,
            "empresaTipDoc" :  solicitudBean.empresaTipDoc! as String,
            "empresaNroDoc" :  solicitudBean.empresaNroDoc! as String,
            "solicitudCodigo"   :  solicitudBean.solicitud! as String,
            "solicitudCodigoEmp"  :  solicitudBean.solicitudEmpresa! as String,
            "tipo" :  self.valorEventoActual as String,
            "latitud" : String(format: "%.14f", self.actualLocation.coordinate.latitude),
            "longitud" :  String(format: "%.14f", self.actualLocation.coordinate.longitude),
            "plataforma" :  "IOS"
        ]
        
        OriginData.sharedInstance.insertarEvento(notificacion: "endInsertarEvento", parametros: parametros as NSDictionary)
    }
    
    @objc func endInsertarEvento(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as? ResultadoBean
        
        if(data!.error.count == 0)
        {
            switch self.valorEventoActual {
            case "EMPSOL":
                
                delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
                
                break
            case "CONRUT":
                
                delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
                
                break
            default:
                break
            }
            
        }
        else
        {
            let errorBean: ErrorBean = data!.error[0] as! ErrorBean
            
            self.mostrarAlertaErrorWS(codigo: errorBean.code, mensaje: errorBean.message)
        }
    }

    // MARK: - UITableViewDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.listaSolicitud.count //+ 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //if (indexPath.row < self.listaSolicitud.count)
        //{
        let cell: SolicitudesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellVerificacion", for: indexPath) as! SolicitudesTableViewCell
        
            let item: SolicitudVerificacionBean! = (self.listaSolicitud[indexPath.row] as! SolicitudVerificacionBean)
        
            cell.btnEmpezar.tag = indexPath.row
            
        cell.btnEmpezar.addTarget(self, action: #selector(HomeViewController.seleccionarSolicitud(sender:)), for: UIControl.Event.touchUpInside)
            
            //Pintando datos en los objetos
            //Borrar
            //cell.lblMensaje!.text = "El \(item.empresaRazonSocial!) ha solicitado su verificación domiciliaria. \(item.ultimaOpcion!) DIR: \(item.usuario.direccionUsuario!)"
            cell.lblMensaje!.text = "\(item.empresaRazonSocial!) ha solicitado que Ud. realice su auto-verificación domiciliaria."
            
            /*
             Pendiente   	PEN
             Enproceso 	EPR
             Completo 	        COM
             Incompleto 	INC
             Cancelado 	CAN
             */
            
            //let colorVerde = UIColor.fromHex(rgbValue: 0x00A35E)
            let colorNaranja = UIColor.fromHex(rgbValue: 0xFF5800)
            let colorAzul = UIColor.fromHex(rgbValue: 0x0074FF)
            let colorRojo = UIColor.fromHex(rgbValue: 0xE21232)
        
            // ic_punto_naranja, ic_punto_verde
            
            switch item.estado! {
            case "PEN":
                
                cell.lblEstado!.text = "Pendiente"
                cell.lblEstado?.textColor = colorNaranja
                cell.btnEmpezar!.setTitle("EMPEZAR", for: .normal)
                cell.btnEmpezar.isHidden = false
                cell.puntoVI.backgroundColor = colorNaranja
                break
                
            case "EPR":
                
                cell.lblEstado!.text = "En proceso"
                cell.lblEstado?.textColor = colorNaranja
                cell.btnEmpezar!.setTitle("CONTINUAR", for: .normal)
                cell.btnEmpezar.isHidden = false
                cell.puntoVI.backgroundColor = colorNaranja
                break
                
            case "COM":
                
                cell.lblEstado!.text = "Completo"
                cell.lblEstado?.textColor = colorAzul
                //cell.btnEmpezar!.setTitle("CONTINUAR", forState: .Normal)
                cell.btnEmpezar.isHidden = true
                cell.puntoVI.backgroundColor = colorAzul
                break
                
            case "INC":
                
                cell.lblEstado!.text = "Incompleto"
                cell.lblEstado?.textColor = colorRojo
                cell.btnEmpezar!.setTitle("CONTINUAR", for: .normal)
                cell.btnEmpezar.isHidden = false
                cell.puntoVI.backgroundColor = colorRojo
                break
                
            case "CAN":
                
                cell.lblEstado!.text = "Cancelado"
                cell.lblEstado?.textColor = colorNaranja
                //cell.btnEmpezar!.setTitle("CONTINUAR", forState: .Normal)
                cell.btnEmpezar.isHidden = true
                cell.puntoVI.backgroundColor = colorNaranja
                break
                
            default:
                break
            }
            
            if(item.logoUrl! != ""){
                
                let imageCache = self.cache.object(forKey: item.logoUrl! as NSString)
                
                if (imageCache != nil)
                {
                    cell.imageEmpresa.image = imageCache as? UIImage
                }
                else
                {
                    //cell.imageEmpresa.imageFromUrl(item.logoUrl!, cache: &self.cache)
                    UIImage.loadFromURL(urlString: item.logoUrl!, callback: { (image: UIImage) -> () in
                        cell.imageEmpresa.image = image
                        self.cache.setObject(image, forKey: item.logoUrl! as NSString)
                    })
                }
                
            }
            else
            {
                cell.imageEmpresa.image = UIImage(named: "logoaqui")
            }
            
            cell.viewAzul.layer.cornerRadius = 5
            
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.backgroundColor = UIColor.clear
            
            cell.contentView.setNeedsLayout()
            cell.contentView.layoutIfNeeded()
            
            return cell
        /*
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellImagen", forIndexPath: indexPath)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.backgroundColor = UIColor.clear
            
            cell.contentView.setNeedsLayout()
            cell.contentView.layoutIfNeeded()
            
            return cell
        }
        */
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath:IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - CLLocationManagerDelegate Methods
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if (status == CLAuthorizationStatus.authorizedAlways)
        {
            print("GPS habilitado siempre.")
        }
        else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            print("GPS habilitado cuando esté en uso.")
        }
        else if (status == CLAuthorizationStatus.denied)
        {
            print("GPS denegado.")
            //UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        //let longitud = location!.coordinate.longitude
        //let latitud = location!.coordinate.latitude
        
        //print("Home lon: \(longitud) y lat: \(latitud)")
        
        self.actualLocation = location!
    }
    
    @IBAction func parlanteAction(sender: AnyObject) {
        
        if (self.audioPlayer.isPlaying == false)
        {
            //let tiempo = self.audioPlayer.currentTime
            //self.resumeAudio(tiempo)
            //self.playAudio(nombreAudio.A1y2)
            self.audioPlayer.play()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            //self.parlanteBT.imageView!.startAnimating()
            //self.estadoAudio = true
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
            self.parlanteBT.imageView!.stopAnimating()
            //self.estadoAudio = false
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        //self.stopAudio()
        //let image1:UIImage = UIImage(named: "ic_audio1")!
        //self.parlanteBT.setImage(image1, forState: .normal)
        //self.parlanteBT.imageView!.stopAnimating()
        self.view.superview?.endEditing(true)
        //self.estadoAudio = false
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }
}
