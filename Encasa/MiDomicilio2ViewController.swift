//
//  MiDomicilio2ViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 26/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import CoreLocation
import AVFoundation

class MiDomicilio2ViewController: ParentViewController,CLLocationManagerDelegate , GMSMapViewDelegate,UITextFieldDelegate {
    
    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    @IBOutlet var direccionLB: UILabel!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var viewFondo: UIView!
    @IBOutlet var direccionSiVI: UIView!
    @IBOutlet var direccionNoVI: UIView!
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    var usuarioBean: UsuarioBean!
    var solicitudBean: SolicitudVerificacionBean!
    
    let locationManager = CLLocationManager()
    //var actualLocation = CLLocation()
    var marcadorLocation = CLLocation()
    
    // MARK: - GoogleMaps Variables
    
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    var selectedRoute: Dictionary<String, Any>!
    var overviewPolyline: Dictionary<String, Any>!
    var routePolyline: GMSPolyline!
    var activarLocalizacion: Bool = false
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicializar()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.setearDatos()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.locationManager.stopUpdatingLocation()
        
        self.stopAudio()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inicializar()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Methods
    
    func inicializar()
    {
//        let image1:UIImage = UIImage(named: "ic_audio1")!
//        let image2:UIImage = UIImage(named: "ic_audio2")!
//        let image3:UIImage = UIImage(named: "ic_audio3")!
//        self.parlanteBT.setImage(image1, forState: .normal)
//        self.parlanteBT.imageView!.animationImages = [image1, image2, image3]
//        self.parlanteBT.imageView!.animationDuration = 1.5
//        self.parlanteBT.imageView!.startAnimating()
        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)

        
        //self.direccionLB.delegate = self
        self.locationManager.delegate = self
        self.mapView.delegate = self
        
        self.viewFondo.backgroundColor = UIColorFromHex(rgbValue: 0x333333, alpha: 0.9)
        
        self.direccionSiVI.layer.shadowRadius = 2
        self.direccionSiVI.layer.shadowOpacity = 0.5
        self.direccionSiVI.layer.shadowOffset = .zero;
        self.direccionSiVI.layer.shadowColor = UIColor.black.cgColor
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean!
    }
    
    func setearDatos()
    {
        self.activarLocalizacion = false
        
        self.mostrarDireccionSi(mostrar: false)
        self.mostrarDireccionNo(mostrar: false)
        
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        self.viewFondo.isHidden = true
        
        self.validarLocalizacion()
        
        self.direccionLB.text = self.solicitudBean.usuario.direccionUsuario
        
        if (self.solicitudBean.usuario.direccionUsuarioLat != "" && self.solicitudBean.usuario.direccionUsuarioLon != "")
        {
            /*
            let selectLatitud = Double(self.solicitudBean.usuario.direccionUsuarioLat!)!.roundToPlaces(14)
            let selectLongitud = Double(self.solicitudBean.usuario.direccionUsuarioLon!)!.roundToPlaces(14)
            self.marcadorLocation = CLLocation(latitude: selectLatitud, longitude: selectLongitud)

            self.configurarMapa(selectLatitud, long: selectLongitud, titleTask: "", addrTask: "")
            */
            //self.validarDistancia()
        }
        else
        {
            delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
        }
        
        self.playAudio(nombreAudio: nombreAudio.A6)
    }
    
    func validarLocalizacion() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == .authorizedWhenInUse || status == .denied)
            {
                let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado para Aquí, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
                
                alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                    
                    UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL);
                    })
                alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                
                self.present(alertaLocalizacion, animated: true, completion: nil)
                
                return false
            }
            else if (status == .notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == .authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
            
            alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                
                UIApplication.shared.openURL(NSURL(string: "prefs:root=LOCATION_SERVICES")! as URL)
                })
            alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alertaLocalizacion, animated: true, completion: nil)
            
            return false
        }
    }
    
    private func configurarMapa(lati:Double, long:Double, titleTask:String, addrTask:String) {
        
        let camera = GMSCameraPosition.camera(withLatitude: lati,
                                                          longitude: long, zoom: 17)
        self.mapView.camera = camera
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.clear()
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(lati, long)
        marker.title = titleTask
        marker.snippet = addrTask
        marker.icon = UIImage(named: "ic_map_mark24")
        marker.map = self.mapView
    }
    
    func mostrarDireccionSi(mostrar: Bool)
    {
        if (mostrar)
        {
            self.viewFondo.isHidden = false
            self.direccionSiVI.isHidden = false
            self.direccionNoVI.isHidden = true
        }
        else
        {
            self.viewFondo.isHidden = true
            self.direccionSiVI.isHidden = true
            self.direccionNoVI.isHidden = true
        }
        
    }
    
    func mostrarDireccionNo(mostrar: Bool)
    {
        if (mostrar)
        {
            self.viewFondo.isHidden = false
            self.direccionSiVI.isHidden = true
            self.direccionNoVI.isHidden = false
        }
        else
        {
            self.viewFondo.isHidden = true
            self.direccionSiVI.isHidden = true
            self.direccionNoVI.isHidden = true
        }
    }
    
    func crearRuta()
    {
        self.showActivityIndicator(uiView: self.view)
        
        let marcadorOrigen = CLLocationCoordinate2D(latitude: self.actualLocation.coordinate.latitude, longitude: self.actualLocation.coordinate.longitude)
        
        let latitudDestino: Double! = Double(self.solicitudBean.usuario.direccionUsuarioLat!)
        let longitudDestino: Double! = Double(self.solicitudBean.usuario.direccionUsuarioLon!)
        let marcadorDestino = CLLocationCoordinate2D(latitude: latitudDestino, longitude: longitudDestino)
        
        self.getDirections(origin: marcadorOrigen, destination: marcadorDestino, waypoints: nil, travelMode: nil, completionHandler: { (status, success) -> Void in
            
            if success
            {
                self.drawRoute()
                self.centrarMapaCamara(origin: marcadorOrigen, destination: marcadorDestino)
            }
            else
            {
                print(status)
            }
            
            self.hideActivityIndicator(uiView: self.view)
        })
    }
    
    func centrarMapaCamara(origin: CLLocationCoordinate2D!, destination: CLLocationCoordinate2D!)
    {
        let bounds = GMSCoordinateBounds(coordinate: origin, coordinate: destination)

        let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets.init(top: 100, left: 50, bottom: 100, right: 50))
        self.mapView.camera = camera!;
    }
    
    func validarDistancia()
    {
        let distanceInMeters = self.actualLocation.distance(from: self.marcadorLocation)
        print("Distancia entre puntos: \(distanceInMeters) m.")
        
        // Si esta dentro de radio
        let radioControl = Double(self.solicitudBean.configuracion.radioControl!) ?? 0
        if (distanceInMeters <= radioControl)
        {
            self.mostrarDireccionSi(mostrar: true)
        }
        else
        {
            self.mostrarDireccionNo(mostrar: true)
        }
    }
    
    func getDirections(origin: CLLocationCoordinate2D!, destination: CLLocationCoordinate2D!, waypoints: Array<String>!, travelMode: AnyObject!, completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        
        if let originLocation = origin {
            if let destinationLocation = destination {
                var directionsURLString = baseURLDirections + "origin=" + String(originLocation.latitude) + "," + String(originLocation.longitude) + "&destination=" + String(destinationLocation.latitude) + "," + String(destinationLocation.longitude)
                
                directionsURLString = (directionsURLString as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue)!
                
                let directionsURL = NSURL(string: directionsURLString)
                
                DispatchQueue.main.async {
                    let directionsData = NSData(contentsOf: directionsURL! as URL)
                    
                    do
                    {
                        let dictionary: Dictionary<String, Any> = try JSONSerialization.jsonObject(with: directionsData! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                        
                        let status = dictionary["status"] as! String
                        
                        if status == "OK" {
                            self.selectedRoute = (dictionary["routes"] as! Array<Dictionary<String, Any>>)[0]
                            self.overviewPolyline = self.selectedRoute!["overview_polyline"] as! Dictionary<String, Any>
                            
                            completionHandler(status, true)
                        }
                        else {
                            completionHandler(status, false)
                        }

                    }
                    catch
                    {
                        completionHandler("", false)
                    }
                    
                }
            }
            else {
                completionHandler("Destination is nil.", false)
            }
        }
        else {
            completionHandler("Origin is nil", false)
        }
    }
    
    func drawRoute() {
        
        let route = self.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)!
        routePolyline = GMSPolyline(path: path)
        routePolyline.strokeWidth = 8
        routePolyline.strokeColor = UIColor.red
        routePolyline.map = self.mapView
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }

    // MARK: - Actions
    
    @IBAction func accionRegresar(sender: AnyObject) {
        
        delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
    }
    
    @IBAction func accionSiguiente(sender: AnyObject) {
        
        self.validarDistancia()
    }

    @IBAction func accionAceptarSi(sender: AnyObject) {
        
        self.mostrarDireccionSi(mostrar: false)
        
        delegate?.changeViewController(menu: LeftMenu.Fotos)
    }
    
    @IBAction func accionCancelarSi(sender: AnyObject) {
    
        self.mostrarDireccionSi(mostrar: false)
    }
    
    @IBAction func accionAceptarNo(sender: AnyObject) {
        
        self.mostrarDireccionNo(mostrar: false)
        
        //self.crearRuta()
    }

    // MARK: - WebServices Methods
    
    
    // MARK: - CLLocationManagerDelegate Methods
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if (status == CLAuthorizationStatus.authorizedAlways)
        {
            print("GPS habilitado siempre.")
            self.mapView.isMyLocationEnabled = true
        }
        else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            print("GPS habilitado cuando esté en uso.")
            self.mapView.isMyLocationEnabled = true
        }
        else if (status == CLAuthorizationStatus.denied)
        {
            print("GPS denegado.")
            //UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        //let longitud = location!.coordinate.longitude
        //let latitud = location!.coordinate.latitude
        
        //print("MiDomicilio2 lon: \(longitud) y lat: \(latitud)")
        
        self.actualLocation = location!
        
        if (!self.activarLocalizacion)
        {
            if (self.solicitudBean.usuario.direccionUsuarioLat != "" && self.solicitudBean.usuario.direccionUsuarioLon != "")
            {
                var selectLatitud = Double(self.solicitudBean.usuario.direccionUsuarioLat!)!
                var selectLongitud = Double(self.solicitudBean.usuario.direccionUsuarioLon!)!
                self.marcadorLocation = CLLocation(latitude: selectLatitud, longitude: selectLongitud)
                
                self.configurarMapa(lati: selectLatitud, long: selectLongitud, titleTask: "", addrTask: "")
                
                self.crearRuta()
            }
            else
            {
                delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
            }
        }
        
        self.activarLocalizacion = true
    }
    @IBAction func parlanteAction(sender: UIButton) {
        
        if (self.audioPlayer.isPlaying == false)
        {
            self.audioPlayer.play()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
        }
    }

}
