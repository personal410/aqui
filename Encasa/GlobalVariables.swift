//
//  GlobalVariables.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 4/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import CoreLocation

class GlobalVariables: NSObject {
    
    // These are the properties you can store in your singleton
    //public var myName: String = "bob"
    internal var infoUsuarioBean: UsuarioBean!
    internal var infoEntidadFinancieraBean:NSArray?
    internal var infoDocumentosBean:NSArray?
    internal var infoIndicadoresBean:NSArray?
    internal var infoFuentesBean:NSArray?
    internal var infoEntidadBean:NSArray?
    internal var infoDatoServicioBean:DatoServicioBean?
    internal var infoSBSResumidaBean:NSArray!
    internal var infoVencidosResumidoBean:NSArray!
    internal var infoMisEmpresasResumidoBean:NSArray!
    internal var infoVencidoOtrosBean:NSArray!
    
    internal var infoListaWebServices : ListaInicialBean!

    internal var infoPersonasBean:NSArray!
    internal var infoSolicitudVerificacionBean:SolicitudVerificacionBean!
    internal var infoMarker:GMSMarker = GMSMarker()
    // Here is how you would get to it without there being a global collision of variables.
    // , or in other words, it is a globally accessable parameter that is specific to the
    // class.
    
    class var sharedManager: GlobalVariables {
        struct Static {
            static let instance = GlobalVariables()
        }
        return Static.instance
    }
}
