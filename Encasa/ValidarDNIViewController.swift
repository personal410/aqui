//
//  ValidarDNIViewController.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 1/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class ValidarDNIViewController: UIViewController,UITextFieldDelegate{
    
    
    var usuarioBean:UsuarioBean?
    
    @IBOutlet weak var itxtCorreo: UITextField!
    @IBOutlet weak var itxtDNI: UITextField!
    
    let grayBorderColor = UIColor.fromHex(rgbValue: 0xEFEFF4).cgColor
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        itxtDNI.delegate = self
        itxtCorreo.delegate = self
        
        
        itxtDNI.setBottomBorder(color: grayBorderColor)
        itxtCorreo.setBottomBorder(color: grayBorderColor)
        
        
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Siguiente", style: UIBarButtonItem.Style.done, target: self, action: #selector(ValidarDNIViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.itxtDNI.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.itxtDNI.resignFirstResponder()
        self.textFieldShouldReturn(itxtDNI)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === itxtDNI) {
            itxtCorreo.becomeFirstResponder()
        } else if (textField === itxtCorreo) {
            itxtCorreo.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func accionContinuar(sender: AnyObject) {
        
        
        if(itxtCorreo.text != "" && itxtDNI.text != ""){
            
            if(itxtDNI.text?.count == 8){
                
                self .iniValidarDNI()
                
            }else{
                
                let alertController = UIAlertController(title: "Crear cuenta", message:
                    "Debe ingresar los 8 números de su DNI", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        }else if(itxtDNI.text == ""){
            
            
            let alertController = UIAlertController(title: "Crear cuenta", message:
                "Ingrese su número de DNI", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }else if(itxtDNI.text?.count != 8){
            
            
            let alertController = UIAlertController(title: "Crear cuenta", message:
                "Debe ingresar los 8 números de su DNI", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }else if(itxtCorreo.text == ""){
            
            
            let alertController = UIAlertController(title: "Crear cuenta", message:
                "Ingrese su correo electrónico.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    func iniValidarDNI(){
        
        //usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ValidarDNIViewController.endValidarDNI(notification:)), name:NSNotification.Name(rawValue: "endValidarDNI"), object: nil)
        
        let parametros = [
            "AFAfiTDoc" : "D" as String,
            "AFAfiNroDoc" : itxtDNI.text! as String,
            "Usuario" : itxtDNI.text! as String,
            "EstadoUsu" : "R" as String,
            "Origen" : "S" as String
        ]
        
        
        OriginData.sharedInstance.validarDNI(notificacion: "endValidarDNI", parametros: parametros as NSDictionary)
    }
    
    @objc func endValidarDNI(notification: NSNotification){
        
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        
        usuarioBean = notification.object as? UsuarioBean;
        
        if(usuarioBean!.codigoValidacion == "0"){
            
            if(usuarioBean!.UsuarioRegistrado == "N"){
                
                if(usuarioBean!.encontro == "S"){
                    
                    GlobalVariables.sharedManager.infoUsuarioBean = usuarioBean
                    self.iniValMail()
                    
                }else{
                    
                    self.hideActivityIndicator(uiView: self.view.superview!)
                    
                    let alertController = UIAlertController(title: "Crear cuenta", message:
                        "No se encontraron datos para el DNI ingresado", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                
                
            }else{
                
                self.hideActivityIndicator(uiView: self.view.superview!)
                
                let alertController = UIAlertController(title: "Crear cuenta", message:
                    "El nro. de DNI ingresado ya se encuentra registrado", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
                
                let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as UIViewController
                self.present(vc, animated: false, completion: nil)
                
            }
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
            
            self.obtenerMensajeError(codigo: usuarioBean!.codigoValidacion!)
            
        }
        
    }
    
    func iniValMail(){
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ValidarDNIViewController.endValMail(notification:)), name:NSNotification.Name(rawValue: "endValMail"), object: nil)
        
        let parametros = [
            "AFAfiTDoc" : "D" as String,
            "AFAfiNroDoc" : itxtDNI.text! as String,
            "AFAfiEmail1" : itxtCorreo.text! as String,
            "Usuario" : itxtDNI.text! as String,
            "SesionId" : usuarioBean!.SesionId! as String
        ]
        
        OriginData.sharedInstance.validarMail(notificacion: "endValMail", parametros: parametros as NSDictionary)
    }
    
    @objc func endValMail(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        usuarioBean = notification.object as? UsuarioBean;
        
        if(usuarioBean?.FlgErrorRet == "S"){
            
            GlobalVariables.sharedManager.infoUsuarioBean?.UseNroDoc = itxtDNI.text
            GlobalVariables.sharedManager.infoUsuarioBean?.UseMail = itxtCorreo.text
            
            self.irValidacionCelular()
            
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
            
            let alertController = UIAlertController(title: "Crear cuenta", message:
                "El correo ingresado no es válido", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        
    }
    
    func irValidacionCelular(){
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ValidarCelularVC") as UIViewController
        self.present(vc, animated: false, completion: nil)
        
    }
    
    @IBAction func accionBack(sender: AnyObject) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    

}
