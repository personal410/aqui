//
//  ActualizarDNIViewController.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 1/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class ActualizarDNIViewController: UIViewController,UITextFieldDelegate{
    
    //weak var delegate: LeftMenuProtocol?
    
    var usuarioBean:UsuarioBean?
    
    @IBOutlet weak var itxtCelular: UITextField!
    @IBOutlet weak var itxtCorreo: UITextField!
    
    let grayBorderColor = UIColor.fromHex(rgbValue: 0xEFEFF4).cgColor
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean

        
        itxtCorreo.delegate = self
        itxtCelular.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        itxtCelular.setBottomBorder(color: grayBorderColor)
        itxtCorreo.setBottomBorder(color: grayBorderColor)
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Siguiente", style: UIBarButtonItem.Style.done, target: self, action: #selector(ActualizarDNIViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.itxtCelular.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.itxtCelular.resignFirstResponder()
        self.textFieldShouldReturn(itxtCelular)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === itxtCelular) {
            itxtCorreo.becomeFirstResponder()
        } else if (textField === itxtCorreo) {
            itxtCorreo.resignFirstResponder()
        }
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    

    @IBAction func accionContinuar(sender: AnyObject) {
        
        if(itxtCelular.text != "" && itxtCorreo.text != ""){
            
            self.iniValidarCelular()
            
        }

    }
    func iniValidarCelular(){
        
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarDNIViewController.endValidarCelular(notification:)), name:NSNotification.Name(rawValue: "endValidarCelular"), object: nil)
        
        let parametros = [
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "SesionId" : usuarioBean!.SesionId! as String,
            "AFAfiTDoc" : "D" as String,
            "AFAfiNroDoc" : usuarioBean!.UseNroDoc! as String,
            "AFSolRefTel" : itxtCelular.text! as String
        ]
        
        OriginData.sharedInstance.validarCelular(notificacion: "endValidarCelular", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endValidarCelular(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.CelularValido=="S"){
            
            GlobalVariables.sharedManager.infoUsuarioBean?.UseCel = itxtCelular.text
            
            self.iniValMail()
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
            
            let alertController = UIAlertController(title: "Registro Usuario", message:
                dato?.Mensaje, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func iniValMail(){
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarDNIViewController.endValMail(notification:)), name:NSNotification.Name(rawValue: "endValMail"), object: nil)
        
        let parametros = [
            "AFAfiTDoc" : "D" as String,
            "AFAfiNroDoc" : usuarioBean!.Usuario! as String,
            "AFAfiEmail1" : usuarioBean!.UseMail! as String,
            "Usuario" : usuarioBean!.Usuario! as String,
            "SesionId" : usuarioBean!.SesionId! as String
        ]
        
        OriginData.sharedInstance.validarMail(notificacion: "endValMail", parametros: parametros as NSDictionary)
    }
    
    @objc func endValMail(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        usuarioBean = notification.object as? UsuarioBean;
        
        if(usuarioBean?.FlgErrorRet == "S"){
            
            GlobalVariables.sharedManager.infoUsuarioBean?.UseMail = itxtCorreo.text
            
            self.irConfirmarTerminosCondiciones()
            
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
            
            let alertController = UIAlertController(title: "Login", message:
                "El correo ingresado no es válido", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    func irConfirmarTerminosCondiciones(){
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ActualizarTerminosCondicionesVC") as UIViewController
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func accionBack(sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }
}
