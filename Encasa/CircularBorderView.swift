//
//  CircularBorderView.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 12/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class CircularBorderView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = layer.bounds.height/2
        
    }
}
