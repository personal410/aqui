//
//  TextField.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 17/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
class TextField:UITextField {
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5);
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
