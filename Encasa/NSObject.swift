//
//  NSObject.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/28/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
class MiObject<T>: NSObject {
    let type: T.Type
    init(type: T.Type) {
        self.type = type
    }
}
extension NSObject {
    convenience init(jsonStr:String , clase :AnyClass) {
        self.init()
        
        if let jsonData = jsonStr.data(using: String.Encoding.utf8, allowLossyConversion: false)
        {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                
                // Loop
                for (key, value) in json {
                    let keyName: String = key as String
                    var keyValue: Any = ""
                    
                    if let result_number = value as? NSNumber
                    {
                        keyValue = "\(result_number)"
                    }
                    else if let result_dict = value as? NSDictionary
                    {
                        keyValue = result_dict
                    }
                    else if let result_arr = value as? NSArray
                    {
                        //keyValue = result_arr
                        var new_array: NSMutableArray = []
                        keyValue = result_arr
                        
                        
                        if(clase == SolicitudVerificacionBean.self){
                        
                            
                            switch keyName{
                            case "Fotos":
                                for item in result_arr{
                                    let obj = item as! NSDictionary
                                    new_array.add(FotoBean(dictionary: obj))
                                }
                            case "result":
                                for item in result_arr{
                                    let obj = item as! NSDictionary
                                    new_array.add(SolicitudVerificacionBean(dictionary: obj))
                                }
                            case "error":
                                for item in result_arr{
                                    let obj = item as! NSDictionary
                                    new_array.add(ErrorBean(dictionary: obj))
                                }
                            default:
                                new_array = result_arr as! NSMutableArray
                            }
                            
                        
                        }
                        else if(clase == ListaInicialBean.self){
                            
                
                            switch keyName{
                            
                            case "result":
                                for item in result_arr{
                                    let obj = item as! NSDictionary
                                    new_array.add(ListaInicialBean(dictionary: obj))
                                }
                            case "error":
                                for item in result_arr{
                                    let obj = item as! NSDictionary
                                    new_array.add(ListaInicialBean(dictionary: obj))
                                }
                            default:
                                new_array = result_arr as! NSMutableArray
                            }
                            
                            
                        }
                        else{
                        
                            switch keyName{
                            case "SDTDatosservicio":
                                for item in result_arr{
                                    let obj = item as! NSDictionary
                                    new_array.add(DatoServicioBean(dictionary: obj))
                                }
//                            case "result":
//                                for item in result_arr{
//                                    let obj = item as! NSDictionary
//                                    new_array.add(ResultadoBean(dictionary: obj))
//                                }
                            case "error":
                                for item in result_arr{
                                    let obj = item as! NSDictionary
                                    new_array.add(ErrorBean(dictionary: obj))
                                }

                            default:
                                new_array = result_arr as! NSMutableArray
                            }

                        }
                       
                        keyValue = new_array
                        
                    }
                    else
                    {
                        keyValue = value as! String
                    }
                    
                    // If property exists
                    if responds(to: NSSelectorFromString(keyName)) {
                        self.setValue(keyValue, forKey: keyName)
                    }
                }
                
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        else
        {
            print("json is of wrong format!")
        }
    }
    
    convenience init(dictionary: NSDictionary) {
        self.init()
        
        // Loop
        for (key, value) in dictionary {
            
            let keyName = key as! String
            var keyValue: Any = ""
            
            if let result_number = value as? NSNumber
            {
                keyValue = "\(result_number)"
            }
            else if let result_dict = value as? NSDictionary
            {
                if keyName == "configuracion"
                {
                    keyValue =  ConfiguracionBean(dictionary: value as! NSDictionary)
                }
                else if keyName == "usuario"
                {
                    keyValue =  UsuarioBean(dictionary: value as! NSDictionary)
                }
                else
                {
                    keyValue = result_dict
                }
            }
            else if let result_arr = value as? NSArray
            {
                var new_array: NSMutableArray = []
                keyValue = result_arr
                
                if (keyName == "fotos")
                {
                    for item in result_arr{
                        let obj = item as! NSDictionary
                        new_array.add(FotoBean(dictionary: obj))
                    }
                }
                else
                {
                    new_array = result_arr as! NSMutableArray
                }
                
                keyValue = new_array
            }
            else
            {
                keyValue = value as! String
            }
            
            // If property exists
            if responds(to: NSSelectorFromString(keyName)) {
                self.setValue(keyValue, forKey: keyName)
            }
        }
        

    }
    
    convenience init(array: NSArray) {
        
        self.init()
        
            // Loop
            for item in array { // loop through data items
                
                let obj = item as! NSDictionary
                
                for (key, value) in obj {
                    
                    let keyName = key as! String
                    var keyValue: Any = ""
                    
                    if let result_number = value as? NSNumber
                    {
                        keyValue = "\(result_number)"
                    }
                    else if let result_dict = value as? NSDictionary
                    {
                        keyValue = result_dict
                    }
                    else if let result_dict = value as? NSArray
                    {
                        keyValue = result_dict
                    }
                    else
                    {
                        keyValue = value as! String
                    }
                    
                    // If property exists
                    if responds(to: NSSelectorFromString(keyName)) {
                        self.setValue(keyValue, forKey: keyName)
                    }
                    
                }
                
            }
        
    }
    
    func setearJson(jsonStr:String) {
        
        if let jsonData = jsonStr.data(using: String.Encoding.utf8, allowLossyConversion: false)
        {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: AnyObject]
                
                // Loop
                for (key, value) in json {
                    let keyName = key as String
                    var keyValue: String = ""
                    
                    if let result_number = value as? NSNumber
                    {
                        keyValue = "\(result_number)"
                    }
                    else
                    {
                        keyValue = value as! String
                    }
                    
                    // If property exists
                    if responds(to: NSSelectorFromString(keyName)) {
                        self.setValue(keyValue, forKey: keyName)
                    }
                }
                
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        else
        {
            print("json is of wrong format!")
        }
    }
}
