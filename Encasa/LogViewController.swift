//
//  LogViewController.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 8/11/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
class LogViewController: ParentViewController {
    @IBOutlet weak var textoTV: UITextView!
    var opcionLeftMenu: LeftMenu = LeftMenu.Home
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textoTV.isEditable = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.actualizarTexto()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func accionAtras(sender: AnyObject) {
        
        self.delegate?.changeViewController(menu: opcionLeftMenu)
    }

    @IBAction func accionActualizar(sender: AnyObject) {
        
        self.actualizarTexto()
    }
    
    @IBAction func accionCopiar(sender: AnyObject) {
        
        UIPasteboard.general.string = self.textoTV.text
    }
    
    func actualizarTexto()
    {
        self.textoTV.text = TextFileHelper.readFromLogFile()
        let count = self.textoTV.text.count
        self.textoTV.selectedRange = NSMakeRange(count, 0)
    }
}
