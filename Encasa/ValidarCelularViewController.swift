//
//  ValidarCelularViewController.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 1/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class ValidarCelularViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblDNI: UILabel!
    
    @IBOutlet weak var itxtCelular: UITextField!
    var usuarioBean:UsuarioBean?
    let grayBorderColor = UIColor.fromHex(rgbValue: 0xEFEFF4).cgColor

    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        
        usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        let textoDNI: String = (usuarioBean?.UseNroDoc!)!
        lblDNI.text = "DNI: \(textoDNI)"
        lblNombre.text = usuarioBean?.ApeNom?.uppercased()
        
        itxtCelular.delegate = self
        itxtCelular.setBottomBorder(color: grayBorderColor)
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Aceptar", style: UIBarButtonItem.Style.done, target: self, action: #selector(ValidarCelularViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.itxtCelular.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.itxtCelular.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func accionValidar(sender: AnyObject) {
        
        if(itxtCelular.text != ""){
            
            self.iniValidarCelular()
            
        }else{
        
            let alertController = UIAlertController(title: "Crear cuenta", message:
                "Ingrese su número de celular", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    func iniValidarCelular(){

        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ValidarCelularViewController.endValidarCelular(notification:)), name:NSNotification.Name(rawValue: "endValidarCelular"), object: nil)
        
        let parametros = [
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "SesionId" : usuarioBean!.SesionId! as String,
            "AFAfiTDoc" : "D" as String,
            "AFAfiNroDoc" : usuarioBean!.UseNroDoc! as String,
            "AFSolRefTel" : itxtCelular.text! as String
        ]
        
        OriginData.sharedInstance.validarCelular(notificacion: "endValidarCelular", parametros: parametros as NSDictionary)
                    
    }
    
    @objc func endValidarCelular(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.CelularValido=="S"){
        
            GlobalVariables.sharedManager.infoUsuarioBean?.UseCel = itxtCelular.text
            
            self.iniGenerarClaveSMS()
        }else{
        
            self.hideActivityIndicator(uiView: self.view.superview!)
            
            let alertController = UIAlertController(title: "Crear cuenta", message:
                dato?.Mensaje, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        
        }

    }
    func iniGenerarClaveSMS(){

        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ValidarCelularViewController.endGenerarClaveSMS(notification:)), name:NSNotification.Name(rawValue: "endGenerarClaveSMS"), object: nil)
        
        let parametros = [
            "AFAfiTDoc" : "D" as String,
            "AFAfiNroDoc" : usuarioBean!.UseNroDoc! as String,
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "SesionId" : usuarioBean!.SesionId! as String,
        ]

        OriginData.sharedInstance.generarClaveSMS(notificacion: "endGenerarClaveSMS", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endGenerarClaveSMS(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.codigoValidacion=="0"){
            
            GlobalVariables.sharedManager.infoUsuarioBean?.CLAVE_SMS = dato!.CLAVE_SMS
            GlobalVariables.sharedManager.infoUsuarioBean?.CLAVE_SMS_CORREO = dato!.CLAVE_SMS_CORREO

            self.iniGenerarAFPInsInvA()
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
        }
        
    }
    func iniGenerarAFPInsInvA(){

        
        NotificationCenter.default.addObserver(self, selector: #selector(ValidarCelularViewController.endGenerarAFPInsInvA(notification:)), name:NSNotification.Name(rawValue: "endGenerarAFPInsInvA"), object: nil)
        
        let parametros = [
            "pAFTipDocu" : "D" as String,
            "pAFNroDocu" : usuarioBean!.UseNroDoc! as String,
            "pAFApePat" : usuarioBean!.UseApePat! as String,
            "pAFApeMat" : usuarioBean!.UseApeMat! as String,
            "pAFNombres" : usuarioBean!.UseNom! as String,
            "pAFNroCel" : itxtCelular.text! as String,
            "pAFEmail" : usuarioBean!.UseMail! as String,
            "pAFNroSMS" : usuarioBean!.CLAVE_SMS! as String,
            "pAFEstReg" : "P" as String,
            "pAFCodInvita" : "" as String,
            "pAFCodInv" : "0" as String,
            "AFKey" : "" as String,
            "AFPswd" : "" as String,
            "caso" : "REG1" as String,
            "pAFNombreCompleto" : usuarioBean!.AFMVNomCor! as String,
            "pAFAfiFNac" : "" as String,
            "pAFAfiFNacD" : "0" as String,
            "pAFAfiFNacM" : "0" as String,
            "pAFAfiFNacY" : "0"as String,
            "AFRandom" : "0" as String,
            "AFRandomInvA" : "0" as String,
            "pAFNroSMSMail" : usuarioBean!.UseMail! as String,
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "SesionId" : usuarioBean!.SesionId! as String,
        ]

        OriginData.sharedInstance.generarAFPInsInvA(notificacion: "endGenerarAFPInsInvA", parametros: parametros as NSDictionary)
    
    
    }
    @objc func endGenerarAFPInsInvA(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.codigoValidacion=="0"){
            
            self.iniValidarCodigo()
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
        }
        
    }
    
    func iniValidarCodigo(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(ValidarCelularViewController.endValidarCodigo(notification:)), name:NSNotification.Name(rawValue: "endValidarCodigo"), object: nil)
        
        let parametros = [
            "CLAVESMS" : usuarioBean!.CLAVE_SMS! as String,
            "AFAfiTDoc" : "D" as String,
            "AFAfiNroDoc" : usuarioBean!.UseNroDoc! as String,
            "modo" : "2" as String,
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "SesionId" : usuarioBean!.SesionId! as String,
        ]

        OriginData.sharedInstance.validarCodigo(notificacion: "endValidarCodigo", parametros: parametros as NSDictionary)
    
    }

    @objc func endValidarCodigo(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.CLAVESMSVALIDA != "N"){

            self.irCrearPassword()
            
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
        }
        
    }
    
    func irCrearPassword(){
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CrearPasswordVC") as UIViewController
        self.present(vc, animated: false, completion: nil)
        
    }

    @IBAction func accionBack(sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
        
    }
}
