//
//  ErrorBean.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/28/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class ErrorBean: NSObject {

    var codMensajes : String? = ""
    var ErrorWSDes : String? = ""
    
    var code : String! = ""
    var message : String! = ""
}
