//
//  String.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 19/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
extension String {
    subscript (i: Int) -> Character {
        return self[i]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }


    func convertDateFormater(date: String) -> NSDate
    {

        let dateFormatter = DateFormatter()
        //the "M/d/yy, H:mm" is put together from the Symbol Table
        dateFormatter.dateFormat = "M/d/yy, H:mm"

        let date = dateFormatter.date(from: date)

        return date! as NSDate
    }

    func substringToIndex(index: Int) -> String
    {
        if (index < 0 || index > self.count)
        {
            return ""
        }
        
        let idx = self.index(self.startIndex, offsetBy: index)
        return String(self[..<idx])
    }

    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}
