//
//  OriginData.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/23/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class OriginData {

    static let sharedInstance = OriginData()
    let endInternetConnection = "endInternetConnection"
    
    func validarLogin(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.validarLogin(notificacion: notificacion, parametros: parametros)
            
        }else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }

    }
    
    func obtenerLoginDetalle(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.obtenerLoginDetalle(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func solicitarCambiarClave(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.solicitarCambiarClave(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func cambiarClave(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.cambiarClave(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func consultarServicio(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.consultarServicio(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func consultaDatosServicio(notificacion: String, parametros: NSDictionary){
    
        if (self.hasConnectivity()) {
    
            return WSLogin.consultaDatosServicio(notificacion: notificacion, parametros: parametros)
    
        }else{
    
            self.showAlertConnectivity()
    
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
    }
    
    }
    func setearToken(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.actualizarToken(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
            //NotificationCenter.default.post(name: "endInternetConnection", object: nil)
        }
    }
    
    func actualizarToken(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.actualizarToken(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
            //NotificationCenter.default.post(name: "endInternetConnection", object: nil)
        }
    }

    
    func setToken(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.setToken(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
        }
        
    }

    func validarDNI(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.validarDNI(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }

    func validarMail(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.validarMail(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func validarCelular(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.validarCelular(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func generarClaveSMS(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.generarClaveSMS(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func validarCodigo(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.validarCodigo(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }

    func generarPassword(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.generarPassword(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func generarAFPInsInvA(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.generarAFPInsInvA(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }

    func AceptaContratoUsu(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.AceptaContratoUsu(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    func aceptarContrato(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.aceptarContrato(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }

    func AFPActRegYa(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.AFPActRegYa(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
     func consultaDatoServicioTercero(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSConsulta.consultaDatoServicioTercero(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    
    func consultaBonificacion(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSConsulta.consultaBonificacion(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
     
    func obtenerMensaje(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.obtenerMensaje(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    
    //======================
    
    func listarSolicitudVerificacion(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.listarSolicitudVerificacion(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func obtenerSolicitudVerificacion(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.obtenerSolicitudVerificacion(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func ingresarDireccion(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.ingresarDireccion(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    
    func validarDireccion(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.validarDireccion(notificacion: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
        
    }
    
    func insertarEvento(notificacion: String, parametros: NSDictionary, notificationConnectivity: String = ""){
        
        if (self.hasConnectivity())
        {
            return WSLogin.insertarEvento(notificacion: notificacion, parametros: parametros)
        }
        else
        {
            self.showAlertConnectivity()
            
            if (notificationConnectivity == "")
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
            }
            else
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationConnectivity), object: nil)
            }
        }
    }
    
    func validarVigenciaSolicitud(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity())
        {
            return WSLogin.validarVigenciaSolicitud(notificacion: notificacion, parametros: parametros)
        }
        else
        {
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func showAlertConnectivity(){
    }
    
    func hasConnectivity() -> Bool {
        
        do{
            let reachability: Reachability = try Reachability.reachabilityForInternetConnection()
            let networkStatus: Int = reachability.currentReachabilityStatus.hashValue
            return networkStatus != 0
        }catch{
            return false
        }
    }
    
    func cargaListaInicial(notificacion: String, parametros: NSDictionary){
        
        if (self.hasConnectivity()) {
            
            return WSLogin.cargaListaWS(notification: notificacion, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
        }
        
    }
}
