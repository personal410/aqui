//
//  borderShadowView.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 19/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class borderShadowView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5
        layer.shadowRadius = 2
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero;
        layer.shadowColor = UIColor.gray.cgColor
        
        
    }

}
