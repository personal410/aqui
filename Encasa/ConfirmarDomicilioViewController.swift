//
//  ConfirmarDomicilioViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 25/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import AVFoundation

class ConfirmarDomicilioViewController: ParentViewController {

    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    var usuarioBean: UsuarioBean!
    var solicitudBean: SolicitudVerificacionBean!
    
    @IBOutlet weak var txtDireccionRegistrada: UILabel!
    @IBOutlet weak var txtDireccionSolicitada: UILabel!
    @IBOutlet var logoEmpresaIV: UIImageView!
    @IBOutlet weak var notaLB: UILabel!
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    var valorSiNo : String = ""
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.inicializar()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.setearDatos()
    }

    override func viewDidDisappear(_ animated: Bool) {
        
        self.stopAudio()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inicializar()
    }
    // MARK: - Methods
    
    func inicializar()
    {
        //self.audioIV.animationImageSecuence()
        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
    }
    
    func setearDatos()
    {
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        self.txtDireccionRegistrada.text = self.solicitudBean.direccion
        
        self.txtDireccionSolicitada.text = self.solicitudBean.usuario.direccionUsuario
        
        self.notaLB.text = "Por favor indique si la dirección que ha validado es la misma que registró \(self.solicitudBean.empresaRazonSocial!). No tiene que estar escrita de la misma manera, pero si debe ser la misma dirección."
        
        if (self.solicitudBean.logoUrl! != "")
        {
            UIImage.loadFromURL(urlString: self.solicitudBean.logoUrl!, callback: { (image: UIImage) -> () in
                
                self.logoEmpresaIV.image = image
            })
        }
        
        self.playAudio(nombreAudio: nombreAudio.A16)
    }
    
    // MARK: - WebServices Methods
    
    func iniValidarDireccion(){
        
//        let image1:UIImage = UIImage(named: "ic_audio1")!
//        let image2:UIImage = UIImage(named: "ic_audio2")!
//        let image3:UIImage = UIImage(named: "ic_audio3")!
//        self.parlanteBT.setImage(image1, forState: .normal)
//        self.parlanteBT.imageView!.animationImages = [image1, image2, image3]
//        self.parlanteBT.imageView!.animationDuration = 1.5
//        self.parlanteBT.imageView!.startAnimating()

        self.pauseAudio()
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)

        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ConfirmarDomicilioViewController.endValidarDireccion(notification:)), name:NSNotification.Name(rawValue: "endValidarDireccion"), object: nil)
        
        let parametros = [
            "usuario" : self.usuarioBean!.UseNroDoc! as String,
            "sesionId" : self.usuarioBean!.UseSeIDSession! as String,
            "empresaTipDoc" : self.solicitudBean.empresaTipDoc! as String,
            "empresaNroDoc" : self.solicitudBean.empresaNroDoc! as String,
            "solicitudCodigo" : self.solicitudBean.solicitud! as String,
            "solicitudCodigoEmp"   :  self.solicitudBean.solicitudEmpresa! as String,
            "siNo" : self.valorSiNo as String,
            "ultimaOpcion" : "9",
            "plataforma" : "IOS"
        ]
        
        OriginData.sharedInstance.validarDireccion(notificacion: "endValidarDireccion", parametros: parametros as NSDictionary)
    }
    
    @objc func endValidarDireccion(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as? ResultadoBean?
        
        if (data!?.error.count == 0)
        {
            delegate?.changeViewController(menu: LeftMenu.ProcesoFinalizado)
        }
        else
        {
            let errorBean: ErrorBean = data!?.error[0] as! ErrorBean
            
            if (errorBean.code == "19")
            {
                delegate?.changeViewController(menu: LeftMenu.ProcesoFinalizado)
            }
            else
            {
                self.mostrarAlertaErrorWS(codigo: errorBean.code, mensaje: errorBean.message, menu: LeftMenu.Home)
            }
        }
    }

    // MARK: - Actions

    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }
    @IBAction func accionSeguirSi(sender: AnyObject) {
        
        self.valorSiNo = "S"
        
        self.iniValidarDireccion()
    }
    
    @IBAction func accionSeguirNo(sender: AnyObject) {

        self.valorSiNo = "N"
        
        self.iniValidarDireccion()
    }
    
    @IBAction func parlanteAction(sender: UIButton) {
        
        if (self.audioPlayer.isPlaying == false)
        {
            self.audioPlayer.play()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
        }
    }
}
