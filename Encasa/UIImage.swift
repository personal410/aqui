//
//  UIImage.swift
//  Encasa
//
//  Created by Rommy Fuentes on 3/06/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import ImageIO
public enum ImageFormat {
    case PNG
    case JPEG(CGFloat)
}

extension UIImage{
    
    func toBase64(format: ImageFormat) -> String{
        var imageData: NSData
        switch format {
            case .PNG: imageData = self.pngData()! as NSData
            case .JPEG(let compression): imageData = self.jpegData(compressionQuality: compression)! as NSData
        }
        return imageData.base64EncodedString(options: .lineLength64Characters)
    }
    class func loadFromURL(urlString: String, callback: @escaping (UIImage)->()) {
        if let url = URL(string: urlString) {
            DispatchQueue.global(qos: .default).async {
                if let data = try? Data(contentsOf: url) {
                    DispatchQueue.main.async {
                        if let image = UIImage(data: data) {
                            callback(image)
                        }else{
                            callback(UIImage())
                        }
                    }
                }else{
                    callback(UIImage())
                }
            }
        }
    }
    
}
