//
//  ProcesoFinalizadoViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 25/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import AVFoundation

class ProcesoFinalizadoViewController: ParentViewController {

    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    var usuarioBean:UsuarioBean!
    var solicitudBean:SolicitudVerificacionBean!
    
    var indice = 0;
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicializar()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        self.setearDatos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inicializar()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.stopAudio()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func inicializar()
    {
//        let image1:UIImage = UIImage(named: "ic_audio1")!
//        let image2:UIImage = UIImage(named: "ic_audio2")!
//        let image3:UIImage = UIImage(named: "ic_audio3")!
//        self.parlanteBT.setImage(image1, forState: .normal)
//        self.parlanteBT.imageView!.animationImages = [image1, image2, image3]
//        self.parlanteBT.imageView!.animationDuration = 1.5
//        self.parlanteBT.imageView!.startAnimating()

        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
    }
    
    func setearDatos()
    {
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        self.playAudio(nombreAudio: nombreAudio.A17y18)
    }
    
    // MARK: - Actions
    
    @IBAction func accionAceptar(sender: AnyObject) {
        
        if (self.solicitudBean.direccion!.uppercased() != self.solicitudBean.usuario.direccionUsuario!.uppercased())
        {
            delegate?.changeViewController(menu: LeftMenu.Home)
        }
        else
        {
            delegate?.changeViewController(menu: LeftMenu.Home)
        }
    }
    @IBAction func parlanteAction(sender: UIButton){
        if (self.audioPlayer.isPlaying == false)
        {
            self.audioPlayer.play()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
            self.parlanteBT.imageView!.stopAnimating()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.view.superview?.endEditing(true)
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }
}
