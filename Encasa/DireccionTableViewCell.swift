//
//  DireccionTableViewCell.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 8/22/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class DireccionTableViewCell: UITableViewCell {

    @IBOutlet weak var iconoIV: UIImageView!
    @IBOutlet weak var direccionLB: UILabel!
    @IBOutlet weak var detalleLB: UILabel!
}
