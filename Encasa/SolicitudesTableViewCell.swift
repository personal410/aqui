//
//  SolicitudesTableViewCell.swift
//  Encasa
//
//  Created by Rommy Fuentes on 24/06/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
class SolicitudesTableViewCell: UITableViewCell {

    @IBOutlet weak var imageEmpresa: UIImageView!
    @IBOutlet weak var lblMensaje: UILabel!
    @IBOutlet weak var btnEmpezar: UIButton!
    @IBOutlet weak var viewAzul: UIView!
    @IBOutlet weak var lblEstado: UILabel!
    @IBOutlet weak var puntoVI: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        //contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(10, 10, 10, 10))
    }
}
