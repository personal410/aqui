//
//  FotoHeaderCollectionReusableView.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 9/8/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class FotoHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var tituloLB: UILabel!
    
}
