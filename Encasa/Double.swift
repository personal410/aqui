//
//  Double.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 7/1/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import Foundation
extension Double {
    mutating func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}
