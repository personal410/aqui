//
//  UILabel.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 21/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

extension UILabel {
/*
    func roundCorners(corners: UIRectCorner, radius: CGFloat){
        
        let bounds = self.bounds
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(radius, radius))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.CGPath
        self.layer.mask = maskLayer
        /*
        let frameLayer = CAShapeLayer()
        frameLayer.frame = bounds
        frameLayer.path = maskPath.CGPath
        frameLayer.strokeColor = UIColor.fromHex(rgbValue: 0xb3b3b3).cgColor
        frameLayer.fillColor = nil
        
        self.layer.addSublayer(frameLayer)
        */
    }
    
    func roundTopCornersRadius(radius: CGFloat){
        
        self.roundCorners(UIRectCorner.TopLeft.union(UIRectCorner.TopRight), radius: radius)
    }
    
    func roundBottomCornersRadius(radius: CGFloat){
        
        self.roundCorners(UIRectCorner.BottomLeft.union(UIRectCorner.BottomRight), radius: radius)
    }
    */
}
