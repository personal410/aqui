//
//  MenuLeftViewController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/21/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit

enum LeftMenu: Int {
    case CambiarContrasenia = 0
    case CerrarSesion
    case Home
    case MiDomicilio1
    case MiDomicilio2
    case BuscarDirecciones
    case Fotos
    case InicioVerificacion
    case Alertas
    case ProcesoFinalizado
    case ConfirmarDomicilio
    case Ninguna
    case TextLog
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
    func changeViewController(menu: LeftMenu, backTo: LeftMenu)
}

class MenuLeftViewController: ParentViewController, LeftMenuProtocol {
    
    
    @IBOutlet weak var imageFoto: UIImageView!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblCorreo: UILabel!
    
    var usuarioBean:UsuarioBean?
    var data: NSData?
    @IBOutlet var tableView: UITableView!
    
    var homeViewController: UIViewController!
    var loginViewController: UIViewController!
    var cambiarContrasenaViewController: UIViewController!
    var confirmarDomicilioViewController: UIViewController!
    var procesoFinalizadoViewController: UIViewController!
    var fotografiaViewController: UIViewController!
    var alertaViewController: UIViewController!
    var inicioVerificacionViewController: UIViewController!
    var miDomicilio1ViewController: UIViewController!
    var miDomicilio2ViewController: UIViewController!
    var buscarDireccionesViewController: UIViewController!
    var logViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        homeViewController.delegate = self
        self.homeViewController = homeViewController
        
        
        let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
        self.loginViewController = loginViewController
        
        let cambiarContrasenaViewController = loginStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaVC") as! CambiarContrasenaViewController
        cambiarContrasenaViewController.delegate = self
        self.cambiarContrasenaViewController = cambiarContrasenaViewController
       
        
        let confirmarDomicilioViewController = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmarDomicilioVC") as! ConfirmarDomicilioViewController
        confirmarDomicilioViewController.delegate = self
        self.confirmarDomicilioViewController = confirmarDomicilioViewController
        
        let procesoFinalizadoViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProcesoFinalizadoVC") as! ProcesoFinalizadoViewController
        procesoFinalizadoViewController.delegate = self
        self.procesoFinalizadoViewController = procesoFinalizadoViewController
        
        let fotografiaViewController = mainStoryboard.instantiateViewController(withIdentifier: "FotografiaVC") as! FotografiaViewController
        fotografiaViewController.delegate = self
        self.fotografiaViewController = fotografiaViewController
        
        let alertaViewController = mainStoryboard.instantiateViewController(withIdentifier: "AlertaVC") as! AlertaViewController
        alertaViewController.delegate = self
        self.alertaViewController = alertaViewController
        
        let inicioVerificacionViewController = mainStoryboard.instantiateViewController(withIdentifier: "InicioVerificacionVC") as! InicioVerificacionViewController
        inicioVerificacionViewController.delegate = self
        self.inicioVerificacionViewController = inicioVerificacionViewController
        
        let miDomicilio1ViewController = mainStoryboard.instantiateViewController(withIdentifier: "MiDomicilio1VC") as! MiDomicilio1ViewController
        miDomicilio1ViewController.delegate = self
        self.miDomicilio1ViewController = miDomicilio1ViewController
        
        let miDomicilio2ViewController = mainStoryboard.instantiateViewController(withIdentifier: "MiDomicilio2VC") as! MiDomicilio2ViewController
        miDomicilio2ViewController.delegate = self
        self.miDomicilio2ViewController = miDomicilio2ViewController
        
        let buscarDireccionesViewController = mainStoryboard.instantiateViewController(withIdentifier: "BuscarDireccionesVC") as! BuscarDireccionesViewController
        buscarDireccionesViewController.delegate = self
        self.buscarDireccionesViewController = buscarDireccionesViewController
        
        let logViewController = mainStoryboard.instantiateViewController(withIdentifier: "TextLogVC") as! LogViewController
        logViewController.delegate = self
        self.logViewController = logViewController
        
        self.cargarDatosPersonal()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.barStyle = .black
    }
    override func viewDidAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        tableView.reloadData()
    }
    
    func cargarDatosPersonal() {
        
        let valor_nombre = "\(usuarioBean!.UseNom!) \(usuarioBean!.UseApePat!) \(usuarioBean!.UseApeMat!)"
        let valor_correo = usuarioBean?.UseMail
        
        lblNombre!.text = valor_nombre
        lblCorreo!.text = valor_correo
        
        if let checkedUrl = NSURL(string: (usuarioBean?.foto)!) {
            imageFoto.contentMode = .scaleAspectFit
            self.imageFoto.layer.cornerRadius = self.imageFoto.frame.width/2.0
            self.imageFoto.clipsToBounds = true
            downloadImage(url: checkedUrl)
        }
    }
    
    func getDataFromUrl(url:NSURL, completion: @escaping ((_ data: NSData?, _ response: URLResponse?, _ error: NSError? ) -> Void)) {
        URLSession.shared.dataTask(with: url as URL) { (data, response, error) in
            completion(data as NSData?, response, error as NSError?)
        }.resume()
    }
    func downloadImage(url: NSURL){
        print("Download Started")
        print("lastPathComponent: " + (url.lastPathComponent ?? ""))
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async {
                guard let data = data, error == nil else { return }
                print(response?.suggestedFilename ?? "")
                print("Download Finished")
                self.imageFoto.image = UIImage(data: data as Data)
            }
        }
    }
    
    func setDefaultViewController(defaultViewController: HomeViewController){
        defaultViewController.delegate = self
        self.homeViewController = defaultViewController
    }
    func changeViewController(menu:LeftMenu) {
        switch menu {
        case .CambiarContrasenia:
            self.slideMenuController()?.changeMainViewController(self.cambiarContrasenaViewController, close: true)
        case .CerrarSesion:
            let userDefaults = UserDefaults.standard
            userDefaults.set("", forKey: defaultsKeys.USER_NAME_KEY)
            userDefaults.set("", forKey: defaultsKeys.USER_PASSWORD_KEY)
            userDefaults.synchronize()
            self.slideMenuController()?.changeMainViewController(self.loginViewController, close: true)
        case .Home:
            self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
        case .MiDomicilio1:
            self.slideMenuController()?.changeMainViewController(self.miDomicilio1ViewController, close: true)
        case .MiDomicilio2:
            self.slideMenuController()?.changeMainViewController(self.miDomicilio2ViewController, close: true)
        case .BuscarDirecciones:
            self.slideMenuController()?.changeMainViewController(self.buscarDireccionesViewController, close: true)
        case .Fotos:
            self.slideMenuController()?.changeMainViewController(self.fotografiaViewController, close: true)
        case .InicioVerificacion:
            self.slideMenuController()?.changeMainViewController(self.inicioVerificacionViewController, close: true)
        case .Alertas:
            self.slideMenuController()?.changeMainViewController(self.alertaViewController, close: true)
        case .ProcesoFinalizado:
            self.slideMenuController()?.changeMainViewController(self.procesoFinalizadoViewController, close: true)
        case .ConfirmarDomicilio:
            self.slideMenuController()?.changeMainViewController(self.confirmarDomicilioViewController, close: true)
        case .TextLog:
            self.slideMenuController()?.changeMainViewController(self.logViewController, close: true)
        default:
            break
        }
        
    }
    
    func changeViewController(menu: LeftMenu, backTo: LeftMenu) {
        
        switch menu {
        case .TextLog:
            let logViewController: LogViewController = self.logViewController as! LogViewController
            logViewController.opcionLeftMenu = backTo
            self.slideMenuController()?.changeMainViewController(logViewController, close: true)
            
        default:
            break
        }
        
    }
}

extension MenuLeftViewController : UITableViewDelegate {
    /*
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
     if let menu = LeftMenu(rawValue: indexPath.item) {
     switch menu {
     case .Main, .Swift, .Java, .Go, .NonMenu:
     return BaseTableViewCell.height()
     }
     }
     return 0
     }
     */
}

extension MenuLeftViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        GlobalVariables.sharedManager.infoUsuarioBean?.tipoAccesoCambiarContrasenia = "M"
        let identifier = "cell-item-\(indexPath.row)"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier)! as UITableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            self.changeViewController(menu: LeftMenu.Home)
            break
        case 1:
            self.changeViewController(menu: LeftMenu.CambiarContrasenia)
            break
        case 2:
            self.changeViewController(menu: LeftMenu.CerrarSesion)
            break
        default:
            break
        }
    }
}
extension MenuLeftViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}
