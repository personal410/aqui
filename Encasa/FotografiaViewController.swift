    //
//  FotografiaViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 25/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import AssetsLibrary
import GoogleMaps
import AVFoundation

class FotografiaViewController: ParentViewController, UICollectionViewDataSource, UICollectionViewDelegate ,UINavigationControllerDelegate, UIImagePickerControllerDelegate, NSURLConnectionDelegate, XMLParserDelegate,CLLocationManagerDelegate  {
    
    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    @IBOutlet weak var barraMenu: UIToolbar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var mensajeLB: UILabel!
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    var mutableData: NSMutableData  = NSMutableData()
    var currentElementName: NSString = ""
    
    var usuarioBean: UsuarioBean!
    var solicitudBean: SolicitudVerificacionBean!

    let locationManager = CLLocationManager()
    //var actualLocation = CLLocation()
    var marcadorLocation = CLLocation()
    
    let imagePicker =  UIImagePickerController()
    var contadorFotosSubidas: Int = 0
    var contadorFotos: Int = 0
    var tomandoFotos: Bool = false
    var activarLocalizacion: Bool = false
    
    var alertaFueraPerimetro = UIAlertController()
    let mensajeFueraPerimetro = "El GPS indica que no se encuentra en su domicilio. Si está en su domicilio, inténtelo nuevamente en unos minutos."
    var valorEventoActual = ""
    
    var fotosObligatorias = [FotoBean]()
    var fotosOpcionales = [FotoBean]()
    var cantEspaciosObligatorias: Int = 0
    var cantEspaciosOpcionales: Int = 0
    
    var indicesFotosObligatorias : [FotoBean] = [FotoBean]()
    var indicesFotosopcionales : [FotoBean] = [FotoBean]()
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.inicializar()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if (!self.tomandoFotos)
        {
            self.setearDatos()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.locationManager.stopUpdatingLocation()
        
        self.stopAudio()
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        
        self.setearVacios(isPortrait: toInterfaceOrientation.isPortrait)
        
        self.indicesFotosObligatorias = []
        self.indicesFotosopcionales = []
        
        self.collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.inicializar()
    }
    // MARK: - Methods
    
    func inicializar()
    {
        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean // AGREGADO
        
        //self.view.addSubview(self.collectionView)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.backgroundColor = .clear
        
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            self.imagePicker.delegate = self
            imagePicker.sourceType = .camera
        }
        else
        {
            print("The device has no camera")
            return
        }
        
        self.mensajeLB.isHidden = true
        
        //definimos orientacion scroll del collectionview
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
        
        //barraMenu.frame = CGRectMake(5, 20, self.view.frame.size.width, 44)
        //barraMenu.sizeToFit()
        //self.view.addSubview(barraMenu)
        
        //---
        self.alertaFueraPerimetro = UIAlertController(title: Constants.aplicacionNombre, message: self.mensajeFueraPerimetro, preferredStyle: .alert)
        self.alertaFueraPerimetro.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        //---
    }
    
    func setearDatos()
    {
        self.tomandoFotos = false
        self.activarLocalizacion = false
        
        self.validarLocalizacion()
        
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        if (self.solicitudBean.usuario.direccionUsuarioLat == "" && self.solicitudBean.usuario.direccionUsuarioLon == "")
        {
            delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
        }
        else
        {
            let selectLatitud = Double(self.solicitudBean.usuario.direccionUsuarioLat!)!
            let selectLongitud = Double(self.solicitudBean.usuario.direccionUsuarioLon!)!
            
            self.marcadorLocation = CLLocation(latitude: selectLatitud, longitude: selectLongitud)
        }
        
        self.contadorFotosSubidas = 0
        self.contadorFotos = 0
        
        if (self.solicitudBean.fotos.count == 0)
        {
            self.mensajeLB.isHidden = false
        }
        else
        {
            self.mensajeLB.isHidden = true
            
            self.fotosObligatorias = []
            self.fotosOpcionales = []
            
            for i in 0 ..< solicitudBean.fotos.count {
                let foto = self.solicitudBean.fotos[i]
                if (foto.obligatorio == "1") {
                    self.fotosObligatorias = NSArray().addAnyObject(array: self.fotosObligatorias as NSArray, object: foto) as! [FotoBean]
                }else{
                    self.fotosOpcionales = NSArray().addAnyObject(array: self.fotosOpcionales as NSArray, object: foto) as! [FotoBean]
                }
            }
            
            self.setearVacios(isPortrait: UIDevice.current.orientation.isPortrait)
            
            self.indicesFotosObligatorias = []
            self.indicesFotosopcionales = []
            
            self.collectionView.reloadData()
        }
    }
    
    func validarLocalizacion() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.denied)
            {
                let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado para Aquí, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
                
                alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                    
                    UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!);
                    })
                alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                
                self.present(alertaLocalizacion, animated: true, completion: nil)
                
                return false
            }
            else if (status == CLAuthorizationStatus.notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == CLAuthorizationStatus.authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
            
            alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                
                UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
                })
            alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alertaLocalizacion, animated: true, completion: nil)
            
            return false
        }
    }
    
    func validarPerimetro() -> Bool {
        if self.validarLocalizacion() {
            let distanceInMeters = self.actualLocation.distance(from: self.marcadorLocation)
            let radioControl = Double(self.solicitudBean.configuracion.radioControl!)!
            return distanceInMeters <= radioControl
        }else{
            return false
        }
    }
    
    func setearFotoTomada(imagen: UIImage, indice: Int)
    {
        let fotoBean = self.solicitudBean.fotos[indice]
        fotoBean.foto = imagen
        
        self.solicitudBean.fotos = NSArray().replaceObjectAtIndex(index: indice, array: self.solicitudBean!.fotos as NSArray, object: fotoBean) as! [FotoBean]
        self.setearVacios(isPortrait: UIDevice.current.orientation.isPortrait)
        
        self.indicesFotosObligatorias = []
        self.indicesFotosopcionales = []
        
        self.collectionView.reloadData()
    }
    
    func guardarFotos()
    {
        let alert = UIAlertController(title: Constants.aplicacionNombre, message: "¿Está seguro que desea enviar las fotografías?", preferredStyle: .alert)

        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
            
            self.showActivityIndicator(uiView: self.view)
            
            if (OriginData().hasConnectivity())
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    self.iniSubirFotos()
                }
            }
            else
            {
                self.hideActivityIndicator(uiView: self.view)
                
                self.showAlertConnectivity()
            }
 
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in

        }))
    }
    
    func abrirCamara(indice: Int)
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            self.tomandoFotos = true
            
            self.imagePicker.view.tag = indice
            
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        else
        {
            let alert = UIAlertController(title: "Alerta", message: "La aplicación: cámara no está disponible.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }
    
    func resizeImage(image:UIImage) -> UIImage
    {
        var actualHeight:Float = Float(image.size.height)
        var actualWidth:Float = Float(image.size.width)
        
        let maxHeight:Float = 1280.0 //your choose height
        let maxWidth:Float = 960.0  //your choose width
        
        var imgRatio:Float = actualWidth/actualHeight
        let maxRatio:Float = maxWidth/maxHeight
        let compressionQuality: CGFloat = 0.25;
        
        if (actualHeight > maxHeight) || (actualWidth > maxWidth)
        {
            if(imgRatio < maxRatio)
            {
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio)
            {
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        let rect:CGRect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth) , height: CGFloat(actualHeight) )
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = img.jpegData(compressionQuality: compressionQuality)!
        UIGraphicsEndImageContext()
        return UIImage(data: imageData)!
    }
    
    func setearVacios(isPortrait: Bool)
    {
        var columnas = 0.0
        
        if (isPortrait)
        {
            columnas = 2.0
        }
        else
        {
            columnas = 4.0
        }
        
        let cocienteObligatorias:Double = Double(self.fotosObligatorias.count) / columnas  //2 -> 0.5  - 0.25
        let cocienteOpcionales:Double = Double(self.fotosOpcionales.count) / columnas  // 1.5          - 0.75
        let ceilingObligatorias = ceil(cocienteObligatorias)   //1                                     -1
        let ceilingOpcionales = ceil(cocienteOpcionales)  //2                                          -1
        let totalObligatorias = columnas * ceilingObligatorias    //2                                  -4
        let totalOpcionales = columnas * ceilingOpcionales     //4                                     -4
        
        self.cantEspaciosObligatorias = Int(totalObligatorias) - self.fotosObligatorias.count  //1
        self.cantEspaciosOpcionales = Int(totalOpcionales) - self.fotosOpcionales.count         //1
        
//        let alert = UIAlertController(title: "EspaciosObligatorios", message: ("\(self.cantEspaciosObligatorias)"), preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
//        self.present(alert, animated: true, completion: nil)

    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func accionRegresar(sender: AnyObject) {
        
        self.tomandoFotos = false
        
        delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
    }
    
    @IBAction func accionSiguiente(sender: AnyObject) {
        
        if (self.validarFotos())
        {
            self.guardarFotos()
        }
    }
    
    func validarFotos() -> Bool
    {
        var contadorObligatorio = 0
        
        for i in 0 ..< solicitudBean.fotos.count {
            let fotoBean = self.solicitudBean.fotos[i]
            
            if (fotoBean.obligatorio == "1")
            {
                contadorObligatorio += 1
                
                if (fotoBean.foto == nil)
                {
                    let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                        "Complete las fotografías obligatorias", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                    return false
                }
            }
        }
        
        if (contadorObligatorio == 0)
        {
            print("No hay FOTOS terminado.")
            
            delegate?.changeViewController(menu: LeftMenu.InicioVerificacion)
            
            return false
        }
        
        return true
    }
    
    // MARK: - WebServices Methods
    
    func iniSubirFotos()
    {
        var urlString = ""
        
        let objetoResult: ListaInicialBean = GlobalVariables.sharedManager.infoListaWebServices
        
        for i in 0 ... objetoResult.result.count - 1 {
            let resultado : ListaInicialBean = objetoResult.result[i]
            if resultado.nombre == "subirFoto" {
                urlString = resultado.url as String
            }
        }
    
        //let urlString : String = "https://www.sentinelmype.com/vcasafilesqa/WsFileServer.asmx"
        //let urlString : String = "http://www.sentinelmype.com/vcasafilesqa/WsFileServer.asmx?wsdl"
        
        let methodName = "uploadFile"
        let url = URL(string: urlString)
        
        let usuario = self.usuarioBean.UseNroDoc!
        let sesionId = self.usuarioBean.UseSeIDSession!
        let empresaTipDoc = self.solicitudBean.empresaTipDoc!
        let empresaNroDoc = self.solicitudBean.empresaNroDoc!
        let solicitudCodigo = self.solicitudBean.solicitud!
        let solicitudCodigoEmp = self.solicitudBean.solicitudEmpresa!
        let latitud = self.actualLocation.coordinate.latitude
        let longitud = self.actualLocation.coordinate.longitude
        let plataforma = "IOS"
        
        var base64String = ""
        var codigoFoto = ""
        var soapMessage = ""
        var msgLength = 0
        var fileName = ""
        
        if (self.solicitudBean.fotos.count > 0)
        {
            for i in 0 ..< solicitudBean.fotos.count {
                let fotoBean = self.solicitudBean.fotos[i]
                
                if (fotoBean.foto != nil)
                {
                    self.contadorFotos += 1
                    
                    codigoFoto = fotoBean.codigo!
                    fileName = "\(self.solicitudBean.empresaTipDoc!)\(self.solicitudBean.empresaNroDoc!)_\(self.solicitudBean.solicitud!)_\(codigoFoto).jpg"
                    print("Nombre de la foto: \(fileName)")
                    
                    base64String = self.resizeImage(image: fotoBean.foto).toBase64(format: ImageFormat.PNG)
                    
                    soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:s='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mime='http://schemas.xmlsoap.org/wsdl/mime/' xmlns:tns='http://sentinelperu.com/' xmlns:soap='http://schemas.xmlsoap.org/wsdl/soap/' xmlns:tm='http://microsoft.com/wsdl/mime/textMatching/' xmlns:http='http://schemas.xmlsoap.org/wsdl/http/' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:wsdl='http://schemas.xmlsoap.org/wsdl/' targetNamespace='http://sentinelperu.com/'><soap12:Body><\(methodName) xmlns='http://sentinelperu.com/'><usuario>\(usuario)</usuario><sesionId>\(sesionId)</sesionId><empresaTipDoc>\(empresaTipDoc)</empresaTipDoc><empresaNroDoc>\(empresaNroDoc)</empresaNroDoc><solicitudCodigo>\(solicitudCodigo)</solicitudCodigo><solicitudCodigoEmp>\(solicitudCodigoEmp)</solicitudCodigoEmp><codigoFoto>\(codigoFoto)</codigoFoto><latitud>\(latitud)</latitud><longitud>\(longitud)</longitud><plataforma>\(plataforma)</plataforma><file>\(base64String)</file><fileName>\(fileName)</fileName></\(methodName)></soap12:Body></soap12:Envelope>"
                    
                    msgLength = soapMessage.count
                    
                    let theRequest = NSMutableURLRequest(url: url!)
                    theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type");
                    theRequest.addValue("\(msgLength)", forHTTPHeaderField: "Content-Length")
                    theRequest.addValue("http://sentinelperu.com/uploadFile", forHTTPHeaderField: "SOAPAction");
                    theRequest.httpMethod = "POST"
                    theRequest.httpBody = soapMessage.data(using: String.Encoding.ascii, allowLossyConversion: true)
                    
                    let connection = NSURLConnection(request: theRequest as URLRequest, delegate: self, startImmediately: true)
                    connection!.start()
                    
                    if ((connection) != nil) {
                        //let mutableData : Void = NSMutableData.initialize()
                    }
                    
                }
            }

        }
        else
        {
            delegate?.changeViewController(menu: LeftMenu.InicioVerificacion)
        }
    }

    func iniInsertarEvento(){
        
        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FotografiaViewController.endInsertarEvento(notification:)), name:NSNotification.Name(rawValue: "endInsertarEvento"), object: nil)
        
        let parametros = [
            "usuario"   :  self.usuarioBean!.UseNroDoc! as String,
            "sesionId"  :  self.usuarioBean!.UseSeIDSession! as String,
            "empresaTipDoc" :  self.solicitudBean.empresaTipDoc! as String,
            "empresaNroDoc" :  self.solicitudBean.empresaNroDoc! as String,
            "solicitudCodigo"   :  self.solicitudBean.solicitud! as String,
            "solicitudCodigoEmp"   :  self.solicitudBean.solicitudEmpresa! as String,
            "tipo" :  self.valorEventoActual, //"INIFOT",
            "latitud" : String(format: "%.14f", self.actualLocation.coordinate.latitude),
            "longitud" :  String(format: "%.14f", self.actualLocation.coordinate.longitude),
            "plataforma" :  "IOS"
        ]
        
        OriginData.sharedInstance.insertarEvento(notificacion: "endInsertarEvento", parametros: parametros as NSDictionary)
    }
    
    @objc func endInsertarEvento(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as! ResultadoBean

        if (data.error.count == 0)
        {
            switch self.valorEventoActual {
            case tipoEvento.INIFOT:
                print("Evento INIFOT terminado.")
                
                self.playAudio(nombreAudio: nombreAudio.A7y8)
                let image1:UIImage = UIImage(named: "ic_pause")!
                self.parlanteBT.setImage(image1, for: .normal)

                
                break
            case tipoEvento.FINCFO:
                print("Evento FINCFO terminado.")
                
                self.delegate?.changeViewController(menu: LeftMenu.InicioVerificacion)
                break
            default:
                break
            }
        }
        else
        {
            let errorBean: ErrorBean = data.error[0]
            
            self.mostrarAlertaErrorWS(codigo: errorBean.code, mensaje: errorBean.message)
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)
        picker.dismiss(animated: true, completion: nil)
        self.setearFotoTomada(imagen: info[UIImagePickerController.InfoKey.originalImage] as! UIImage, indice: picker.view.tag)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.indicesFotosObligatorias = []
        self.indicesFotosopcionales = []
        
        self.collectionView.reloadData()
        picker.dismiss(animated: true, completion: nil)
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)
    }
    
    // MARK: - NSURLConnectionDelegate Methods
    
    func connection(connection: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        self.mutableData.length = 0;
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.mutableData.append(data as Data)
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        
        let response = NSString(data: self.mutableData as Data, encoding: String.Encoding.utf8.rawValue)
        print(response)
        
        let xmlParser = XMLParser(data: self.mutableData as Data)
        xmlParser.delegate = self
        xmlParser.parse()
        xmlParser.shouldResolveExternalEntities = true
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)
    }
    
    // MARK: - NSXMLParserDelegate Methods
    
    func parser(parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        self.currentElementName = elementName as NSString
    }
    
    
    func parser(parser: XMLParser, foundCharacters string: String) {
        
        print("Resultado del ws soap: \(string)")
        
        let image1:UIImage = UIImage(named: "ic_play")!
        self.parlanteBT.setImage(image1, for: .normal)
        
        print(self.currentElementName)
        
        if self.currentElementName == "result" {
            
            self.contadorFotosSubidas += 1
            
            if (self.contadorFotosSubidas == self.contadorFotos)
            {
                self.hideActivityIndicator(uiView: self.view)
                
                print("Evento GUARDAR FOTOS terminado.")
                
                self.tomandoFotos = false
                
                self.valorEventoActual = tipoEvento.FINCFO
                
                let image1:UIImage = UIImage(named: "ic_play")!
                self.parlanteBT.setImage(image1, for: .normal)
                
                self.iniInsertarEvento()
            }
            
        }else{
            
            if(string == "18"){
                
                let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                    "Error desconocido", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(string == "19"){
                
                let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                    "No existe una solicitud para el usuario", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(string == "21"){
                
                let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                    "Excepción no controlada", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(string == "35"){
                
                let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                    "Sesión finalizada", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(string == "36"){
               
                let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                    "Sesión iniciada en otro dispositivo", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(string == "37"){
                
                let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                    "Sesión inválida", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(string == "39"){
                
                let alertController = UIAlertController(title: Constants.aplicacionNombre, message:
                    "Valor incorrecto", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    // MARK: - UICollectionViewDelegate Methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        if (self.fotosOpcionales.count == 0)
        {
            return 1
        }
        else
        {
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var nroItems: Int = 0
        
        if (self.solicitudBean != nil)
        {
            switch section {
            case 0:
                
                nroItems = self.fotosObligatorias.count + self.cantEspaciosObligatorias
                
                if UIDevice.current.orientation.isPortrait {
                    
                    if self.fotosObligatorias.count % 2 == 0 {
                        nroItems = self.fotosObligatorias.count
                    }
                    else if self.fotosObligatorias.count % 2 != 0 {
                        nroItems = self.fotosObligatorias.count + 1
                    }
                }
                else if UIDevice.current.orientation.isLandscape {
                    
                    nroItems = self.fotosObligatorias.count + self.cantEspaciosObligatorias
                }
                
                break
            case 1:
                nroItems = self.fotosOpcionales.count + self.cantEspaciosOpcionales
                
                if UIDevice.current.orientation.isPortrait {
                    
                    if self.fotosOpcionales.count % 2 == 0 {
                        nroItems = self.fotosOpcionales.count
                    }
                    else if self.fotosOpcionales.count % 2 != 0 {
                        nroItems = self.fotosOpcionales.count + 1
                    }
                }
                else if UIDevice.current.orientation.isLandscape {
                    
                    nroItems = self.fotosOpcionales.count + self.cantEspaciosOpcionales
                }
                break
            default:
                nroItems = self.fotosObligatorias.count + self.cantEspaciosObligatorias
                break
            }
        }
        else
        {
            nroItems = 0
        }
        
        return nroItems
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        //1
        switch kind {
        //2
        case UICollectionView.elementKindSectionHeader:
            //3
            let headerView =
                collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                withReuseIdentifier: "reusableHeader",
                                                                for: indexPath)
                    as! FotoHeaderCollectionReusableView
            
            print("section: \(indexPath.section)")
            switch indexPath.section {
            case 0:
                headerView.tituloLB.text = "Fotografías Obligatorias"
                headerView.tituloLB.textColor = UIColor.fromHex(rgbValue: 0x5AD47E)
                headerView.backgroundColor = UIColor.fromHex(rgbValue: 0x333340)
                
                //self.collectionView.backgroundColor = UIColor.fromHex(rgbValue: 0x333340)
                
                break
            case 1:
                headerView.tituloLB.text = "Fotografías Opcionales"
                
                headerView.tituloLB.textColor = UIColor.fromHex(rgbValue: 0x165C9A)
                headerView.backgroundColor = UIColor.fromHex(rgbValue: 0xEBEBEB)
                //self.collectionView.backgroundColor = UIColor.fromHex(rgbValue: 0xEBEBEB)
                break
            default:
                break
            }
            
            return headerView
        default:
            //4
            assert(false, "Unexpected element kind")
        }
        
        let headerView =
            collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                            withReuseIdentifier: "reusableHeader",
                                                            for: indexPath)
                as! FotoHeaderCollectionReusableView
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FotoCollectionViewCell
        
        var fotoBean: FotoBean = FotoBean()
        var esVacio = false
    
        switch indexPath.section {
        case 0:
            if (indexPath.row <= self.fotosObligatorias.count - 1)
            {
                fotoBean = self.fotosObligatorias[indexPath.row]
                fotoBean.sinDatos = false
                self.indicesFotosObligatorias.append(fotoBean)
            }
            else
            {
                fotoBean.obligatorio = "1"
                fotoBean.sinDatos = true
                self.indicesFotosObligatorias.append(fotoBean)
                esVacio = true
                
            }
            break
        case 1:
            if (indexPath.row <= self.fotosOpcionales.count - 1)
            {
                fotoBean = self.fotosOpcionales[indexPath.row]
                fotoBean.sinDatos = false
                self.indicesFotosopcionales.append(fotoBean)
            }
            else
            {
                fotoBean.obligatorio = "0"
                fotoBean.sinDatos = true
                self.indicesFotosopcionales.append(fotoBean)
                esVacio = true
            }
            break
        default:
            break
        }
        
        print("Index: \(indexPath.row)")
        
        if (fotoBean.obligatorio! == "1")
        {
            cell.fondoView.backgroundColor = UIColor.fromHex(rgbValue: 0x333340)
            cell.tituloLB.textColor = UIColor.fromHex(rgbValue: 0xFFFFFF)
            cell.fondoSuperiorView.backgroundColor = UIColor.fromHex(rgbValue: 0x333340)
            
            if (!esVacio)
            {
                cell.imageFoto.backgroundColor = UIColor.fromHex(rgbValue: 0x474747)
                cell.puntitoImg.image = UIImage(named: "ic_punto_verde")
                cell.subtituloLB.text = fotoBean.descripcion
            }
            else
            {
                cell.imageFoto.backgroundColor = UIColor.fromHex(rgbValue: 0x333340)
                cell.puntitoImg.image = nil
                cell.subtituloLB.text = ""
            }
        }
        else
        {
            cell.fondoView.backgroundColor = UIColor.fromHex(rgbValue: 0xEBEBEB)
            cell.tituloLB.textColor = UIColor.fromHex(rgbValue: 0x333340)
            cell.fondoSuperiorView.backgroundColor = UIColor.fromHex(rgbValue: 0xEBEBEB)
            
            if (!esVacio)
            {
                cell.imageFoto.backgroundColor = UIColor.fromHex(rgbValue: 0xc5c5c5)
                cell.puntitoImg.image = UIImage(named: "ic_punto_azul")
                cell.subtituloLB.text = fotoBean.descripcion
            }
            else
            {
                cell.imageFoto.backgroundColor = UIColor.fromHex(rgbValue: 0xEBEBEB)
                cell.puntitoImg.image = nil
                cell.subtituloLB.text = ""
            }
        }
        
        cell.tituloLB.text = fotoBean.nombre
        
        if (fotoBean.foto != nil)
        {
            cell.imageFoto.contentMode = UIView.ContentMode.scaleToFill
            cell.imageFoto.image = fotoBean.foto
        }
        else
        {
            if (!esVacio)
            {
                cell.imageFoto.contentMode = UIView.ContentMode.scaleAspectFit
                cell.imageFoto.image = UIImage(named: "ic_camara")
            }
            else
            {
                cell.imageFoto.contentMode = UIView.ContentMode.scaleAspectFit
                cell.imageFoto.image = nil
            }
        }

        cell.backgroundColor = .clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var fotoNil:FotoBean!
        
        if indexPath.section == 0 {
            fotoNil = indicesFotosObligatorias[indexPath.row]
        }
        else if indexPath.section == 1 {
            fotoNil = indicesFotosopcionales[indexPath.row]
        }
        
        //----------------------------
        
        if fotoNil.sinDatos == false {
            print("OBJETO CONTENEDOR")
            if (self.validarPerimetro())
            {
                            var indice: Int = 0
                
                            switch indexPath.section {
                            case 0:
                                indice = indexPath.row
                                break
                            case 1:
                                indice = indexPath.row + self.fotosObligatorias.count
                                break
                            default:
                                break
                            }
                
                self.abrirCamara(indice: indice)
            }
            else
            {
                
                self.iniInsertarEventoFueraPerimetro()
            }
        }
        else
        {
            print("OBJETO RELLENO")
        }
        
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let bounds = collectionView.bounds
        
        var ancho : CGFloat = 0
        var alto : CGFloat = 0
        
        if(UIDevice.current.orientation.isLandscape)
        {
            print("landscape")
            
            let width = bounds.size.width
            let height = bounds.size.height
            
            ancho = width / 4
            
            if(height < 150){
                
                alto = ancho + 0.20*(ancho)
                
            }else{
                
                alto = ancho
            }
        }else if(UIDevice.current.orientation.isPortrait)
        {
            let width = bounds.size.width
            let height = bounds.size.height
            
            print("Portrait")
            ancho = width / 2
            alto = (height / 2) - 100
            
        }else{
            let width = bounds.size.width
            let height = bounds.size.height
            
            if(width < height){
                
                ancho = width / 2
                alto = (height / 2) - 100
                
            }else{
                
                print("landscape")
                
                let width = bounds.size.width
                let height = bounds.size.height
                
                ancho = width / 4
                
                
                if(height < 150){
                    
                    alto = ancho + 0.20*(ancho)
                    
                }else{
                    
                    alto = ancho
                }
                
            }
            
        }
        
        return CGSize(width: ancho , height: alto )
        
    }

    // MARK: - CLLocationManagerDelegate Methods
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if (status == CLAuthorizationStatus.authorizedAlways)
        {
            print("GPS habilitado siempre.")
        }
        else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            print("GPS habilitado cuando esté en uso.")
        }
        else if (status == CLAuthorizationStatus.denied)
        {
            print("GPS denegado.")
            //UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let longitud = location!.coordinate.longitude
        let latitud = location!.coordinate.latitude
        
        //print("Fotos lon: \(longitud) y lat: \(latitud)")
        
        self.actualLocation = location!
        
        if (!self.activarLocalizacion)
        {
            self.valorEventoActual = tipoEvento.INIFOT
            
            self.iniInsertarEvento()
        }
        
        self.activarLocalizacion = true

    }
    
    @IBAction func parlanteAction(sender: UIButton) {
        
        if (self.audioPlayer.isPlaying == false)
        {
            self.audioPlayer.play()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)        }
    }
}
