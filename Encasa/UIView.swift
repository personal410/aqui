//
//  UIView.swift
//  Encasa
//
//  Created by Rommy Fuentes on 27/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

    
    extension UIView {
        
        func startRotating(duration: Double = 1) {
            let kAnimationKey = "rotation"
            
            if self.layer.animation(forKey: kAnimationKey) == nil {
                let animate = CABasicAnimation(keyPath: "transform.rotation")
                animate.duration = duration
                animate.repeatCount = Float.infinity
                animate.fromValue = 0.0
                animate.toValue = Float(M_PI * 2.0)
                self.layer.add(animate, forKey: kAnimationKey)
            }
        }
        func stopRotating() {
            let kAnimationKey = "rotation"
            
            if self.layer.animation(forKey: kAnimationKey) != nil {
                self.layer.removeAnimation(forKey: kAnimationKey)
            }
        }
        
        func roundCorners(corners: UIRectCorner, radius: CGFloat){
            
            let bounds = self.bounds
            let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            
            let maskLayer = CAShapeLayer()
            maskLayer.frame = bounds
            maskLayer.path = maskPath.cgPath
            self.layer.mask = maskLayer
            
            let frameLayer = CAShapeLayer()
            frameLayer.frame = bounds
            frameLayer.path = maskPath.cgPath
            //frameLayer.strokeColor = UIColor.fromHex(rgbValue: 0xb3b3b3).cgColor
            frameLayer.fillColor = nil
            
            self.layer.addSublayer(frameLayer)
        }
 
        func roundTopCornersRadius(radius: CGFloat){
            
            self.roundCorners(corners: UIRectCorner.topLeft.union(UIRectCorner.topRight), radius: radius)
        }
        
        func roundBottomCornersRadius(radius: CGFloat){
            
            self.roundCorners(corners: UIRectCorner.bottomLeft.union(UIRectCorner.bottomRight), radius: radius)
        }
        
        func setBorder(){
            
            self.layer.borderColor = UIColor.fromHex(rgbValue: 0xEAEAEA).cgColor
            self.layer.borderWidth = 2.0
        }
        
        func addBorder(edge: UIRectEdge) {
            
            let color = UIColor.fromHex(rgbValue: 0xEAEAEA).cgColor
            let thickness: CGFloat = 2.0
            let border = CALayer()
            
            switch edge {
            case UIRectEdge.top:
                border.frame = CGRect(x: 0, y: 0, width: self.frame.height, height:  thickness)
                break
            case UIRectEdge.bottom:
                border.frame = CGRect(x:0, y: self.frame.height - thickness, width: UIScreen.main.bounds.width, height: thickness)
                break
            case UIRectEdge.left:
                border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
                break
            case UIRectEdge.right:
                border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
                break
            default:
                break
            }
            
            border.backgroundColor = color;
            
            self.layer.addSublayer(border)
        }
        
        func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
            
            let border = CALayer()
            
            switch edge {
            case UIRectEdge.top:
                border.frame = CGRect(x: 0, y: 0, width: self.frame.height, height: thickness)
                break
            case UIRectEdge.bottom:
                border.frame = CGRect(x: 0, y: self.frame.height - thickness, width:  UIScreen.main.bounds.width, height: thickness)
                break
            case UIRectEdge.left:
                border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
                break
            case UIRectEdge.right:
                border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
                break
            default:
                break
            }
            
            border.backgroundColor = color.cgColor;
            
            self.layer.addSublayer(border)
        }
        
        /*

         func setBorder(){
         
         self.layer.borderColor = UIColor.fromHex(rgbValue: 0xEAEAEA).cgColor
         self.layer.borderWidth = 2.0
         }
         func setShadow()
         {
         let containerView:UIView = UIView(frame:self.frame)
         //dont use clear color,fit blue color
         containerView.backgroundColor = UIColor.blue
         //shadow view also need cornerRadius
         containerView.layer.cornerRadius = 10
         containerView.layer.shadowColor = UIColor.lightGray.cgColor
         containerView.layer.shadowOffset = CGSizeMake(-10, 10); //Left-Bottom shadow
         //containerView.layer.shadowOffset = CGSizeMake(10, 10); //Right-Bottom shadow
         containerView.layer.shadowOpacity = 1.0
         containerView.layer.shadowRadius = 2
         
         self.addSubview(containerView)
         }
         
         func setRoundCorners(){
         
         self.layer.cornerRadius = 5
         self.layer.masksToBounds = true
         }
         */
    }
