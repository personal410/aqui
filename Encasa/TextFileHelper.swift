//
//  TextFileHelper.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 8/11/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class TextFileHelper {

    static let fileLog = "log.txt"
    
    static func writeToLogFile(value:String) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let path = documentsPath.appendingPathComponent(fileLog)
        
        let oldValue = self.readFromLogFile()
        var newValue = value
        let currentDate = NSDate()
        var currentDateString = "\(currentDate)"
        currentDateString = currentDateString.substringToIndex(index: 22).trim()
        
        newValue = "\(oldValue) \(currentDateString) \(value)" + "\n"
        
        do
        {
            try newValue.write(toFile: path, atomically: false, encoding: String.Encoding.utf8)
        }
        catch
        {}
        
        NSLog(value)
    }
    
    /*
    static func writeToLogFile(value:String) {
        
    }
    */
    static func readFromLogFile() -> String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let path = documentsPath.appendingPathComponent(fileLog)
        let checkValidation = FileManager.default
        
        var file:String = ""
        
        if checkValidation.fileExists(atPath: path) {
            
            do
            {
                try file = NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue) as String
            }
            catch
            {}
            
        } else {
            file = "*ERROR* \(fileLog) does not exist."
        }
        
        return file
    }

    static func resetLogFile() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let path = documentsPath.appendingPathComponent(fileLog)
        
        let newValue = ""
        
        do
        {
            try newValue.write(toFile: path, atomically: false, encoding: String.Encoding.utf8)
        }
        catch
        {}
    }
    
    func writeToDocumentsFile(fileName:String,value:String) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let path = documentsPath.appendingPathComponent(fileName)

        do
        {
            try value.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
        }
        catch
        {}
    }
    
    func readFromDocumentsFile(fileName:String) -> String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let path = documentsPath.appendingPathComponent(fileName)
        let checkValidation = FileManager.default

        var file:String = ""
        
        if checkValidation.fileExists(atPath: path) {
            
            do
            {
                try file = NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue) as String
            }
            catch
            {}
            
        } else {
            file = "*ERROR* \(fileName) does not exist."
        }
        
        return file
    }
}
