//
//  UIViewController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/22/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

var container: UIView = UIView()
var loadingView: UIView = UIView()
var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
var delegate: LeftMenuProtocol?

extension UIViewController {
    func showActivityIndicator(uiView: UIView) {
        
        container = UIView()
        loadingView = UIView()
        activityIndicator = UIActivityIndicatorView()
        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.5)
        //0xffffff
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height:  80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2);
        //activityIndicator.center = loadingView.center
        //activityIndicator.center = CGPointMake(uiView.frame.size.width / 2.0, uiView.frame.size.height / 2.0);
        
        //----
        container.translatesAutoresizingMaskIntoConstraints = false
        //container.autoresizingMask = UIViewAutoresizing.FlexibleWidth.union(UIViewAutoresizing.FlexibleHeight)
        container.autoresizingMask = UIView.AutoresizingMask.flexibleWidth.union(UIView.AutoresizingMask.flexibleHeight).union(UIView.AutoresizingMask.flexibleLeftMargin).union(UIView.AutoresizingMask.flexibleRightMargin).union(UIView.AutoresizingMask.flexibleTopMargin).union(UIView.AutoresizingMask.flexibleBottomMargin)

        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.autoresizingMask = UIView.AutoresizingMask.flexibleLeftMargin.union(UIView.AutoresizingMask.flexibleRightMargin).union(UIView.AutoresizingMask.flexibleTopMargin).union(UIView.AutoresizingMask.flexibleBottomMargin)
        //----
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        
        activityIndicator.startAnimating()
    }
    
    /*
    Hide activity indicator
    Actually remove activity indicator from its super view
    
    @param uiView - remove activity indicator from this view
    */
    func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    func showAlert(titulo: String, mensaje: String){
        
        let alerta = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alerta, animated: true, completion: nil)
    }
    
    /**
     Esta función permite mostrar el mensaje de error que devuelve el ws.
     
     - parameters:
        - codigo String que indica el código de error.
     */
    func obtenerMensajeError(codigo: String){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.endObtenerMensaje(notification:)), name:NSNotification.Name(rawValue: "endObtenerMensaje"), object: nil)
        
        let parametros = [
            "ErrorWSCod" : codigo
        ]
        
        OriginData.sharedInstance.obtenerMensaje(notificacion: "endObtenerMensaje", parametros: parametros as NSDictionary)
    }
    
    @objc func endObtenerMensaje(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let errorBean = notification.object as! ErrorBean;
        print("endObtenerMensaje: \(errorBean)")
        
        self.showAlert(titulo: "Atención", mensaje: errorBean.ErrorWSDes!)
    }
    
    /*
    Define UIColor from hex value
    
    @param rgbValue - hex color value
    @param alpha - transparency level
    */
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    

    func hideKeyboardWhenTappedAround() {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
    }
        
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
}
