//
//  ExSlideMenuController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/21/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ExSlideMenuController : SlideMenuController {
    
    override func isTagetViewController() -> Bool {
        
//        if let vc = UIApplication.topViewController() {
//            if vc is HomeViewController ||
//                vc is QuienConsultadoViewController ||
//                vc is PreferenciasViewController ||
//                vc is RecomiendaViewController {
//                    return true
//            }
//        }
//        
        
        if let vc = UIApplication.topViewController() {
            if vc is HomeViewController {
                return true
            }
        }

        return false
    }
    
}