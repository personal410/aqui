//
//  WSParent.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/28/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class WSParent {

    static func prepareSessionDataTask(notificacion: String, parametros: NSDictionary, metodo: String, clase: AnyClass, timeoutInterval: TimeInterval = 30){
        
        let configuration = URLSessionConfiguration .default
        configuration.timeoutIntervalForRequest = timeoutInterval
        configuration.timeoutIntervalForResource = timeoutInterval
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let method = metodo
        var urlString : NSString = NSString(format: "\(PATHS.listaservice)RWS_VD_listaWebService" as NSString)
        
        if method != "RWS_VD_listaWebService"{
            var listaBean:ListaInicialBean!
            listaBean = GlobalVariables.sharedManager.infoListaWebServices
            var codigoOk = 0
            for i in 0 ... listaBean.result.count - 1 {
                
                let objetoLista: ListaInicialBean = listaBean.result[i] as! ListaInicialBean
                if objetoLista.nombre as String == metodo{
                    urlString = NSString(format: "\(objetoLista.url)" as NSString)
                    codigoOk = 1
                }
            }
            if codigoOk == 0 {
                urlString = NSString(format: "\(PATHS.webservice)\(method)" as NSString)
            }
        }
        let request = NSMutableURLRequest()
        request.url = URL(string: NSString(format: "%@", urlString) as String)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.timeoutInterval = timeoutInterval
        
        do{
            request.httpBody  = try! JSONSerialization.data(withJSONObject: parametros, options: [])
            session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
                    else {
                        print("error: not a valid http response")
                        return
                }
                
                var objectBean = NSObject()
                
                switch (httpResponse.statusCode)
                {
                case 200:
                    
                    var response = NSString (data: receivedData, encoding: String.Encoding.utf8.rawValue) as! String
                    print("response data: \(response)")
                    
                    switch (clase){
                    case is UsuarioBean.Type:
                        
                        response = (response as NSString).replacingOccurrences(of: "CodigoValidacion", with: "codigoValidacion")
                        
                        objectBean = UsuarioBean(jsonStr: response, clase: UsuarioBean.self)
                    case is NotificationPush.Type:
                        objectBean = NotificationPush(jsonStr: response, clase: NotificationPush.self)
                    case is ErrorBean.Type:
                        objectBean = ErrorBean(jsonStr: response, clase: ErrorBean.self)
                    case is DatoServicioBean.Type:
                        objectBean = DatoServicioBean(jsonStr: response, clase: DatoServicioBean.self)
                    case is SolicitudVerificacionBean.Type:
                        objectBean = SolicitudVerificacionBean(jsonStr: response, clase: SolicitudVerificacionBean.self)
                    case is ResultadoBean.Type:
                        objectBean = ResultadoBean(jsonStr: response, clase: ResultadoBean.self)
                    case is ListaInicialBean.Type:
                        objectBean = ListaInicialBean(jsonStr: response, clase: ListaInicialBean.self)
                    default:
                        objectBean = NSObject(jsonStr: response, clase: NSObject.self)
                    }
                    
                default:
                    print("save profile POST request got response \(httpResponse.statusCode)")
                    
                    switch (clase){
                    case is UsuarioBean.Type:
                        objectBean = UsuarioBean()
                    case is NotificationPush.Type:
                        objectBean = NotificationPush()
                    case is ErrorBean.Type:
                        objectBean = ErrorBean()
                    case is DatoServicioBean.Type:
                        objectBean = DatoServicioBean()
                    case is SolicitudVerificacionBean.Type:
                        objectBean = SolicitudVerificacionBean()
                    case is ResultadoBean.Type:
                        objectBean = ResultadoBean()
                    case is ListaInicialBean.Type:
                        objectBean = ListaInicialBean()
                    default:
                        objectBean = NSObject()
                    }
                    
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: objectBean)
            }.resume()
        }
        catch
        {
            print("Ocurrio un error .....")
        }
    }
}
