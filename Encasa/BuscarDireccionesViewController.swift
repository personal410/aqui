//
//  BuscarDireccionesViewController.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 8/19/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation

class BuscarDireccionesViewController: ParentViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    // MARK: - Variables
    
    @IBOutlet var txtDireccion: TextField!
    @IBOutlet var errorVI: borderShadowView!
    @IBOutlet weak var limpiarBT: UIButton!
    @IBOutlet weak var limpiarIV: UIImageView!
    @IBOutlet weak var direccionesTV: UITableView!
    
    var solicitudBean: SolicitudVerificacionBean!
    let locationManager = CLLocationManager()
    
    // MARK: - GoogleMaps Variables
    
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    var lookupAddressResults: Array<Dictionary<String, Any>>!
    var fetchedFormattedAddress: String!
    var fetchedAddressLongitude: Double!
    var fetchedAddressLatitude: Double!
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicializar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.limpiarControles()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.setearDatos()
    }

    override func viewDidDisappear(_ animated: Bool) {
        
        self.locationManager.stopUpdatingLocation()
        
        self.stopAudio()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.stopAudio()
    }
    
    // MARK: - Methods
    
    func inicializar()
    {
        self.txtDireccion.delegate = self
        self.txtDireccion.addTarget(self, action: #selector(BuscarDireccionesViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        self.direccionesTV.delegate = self
        self.direccionesTV.dataSource = self
        self.direccionesTV.isHidden = true
        
        self.locationManager.delegate = self
        
        self.limpiarBT.isHidden = true
        self.limpiarIV.isHidden = true
        self.errorVI.isHidden = true
        
        self.lookupAddressResults = []
    }
    
    func setearDatos()
    {
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        self.txtDireccion.text = self.solicitudBean.usuario.direccionUsuario!
        
        if (self.solicitudBean.usuario.direccionUsuario! != "")
        {
            self.limpiarBT.isHidden = false
            self.limpiarIV.isHidden = false
            
            self.geocodeAddress(address: self.txtDireccion.text!, esRedirect: false)
        }
        
        self.txtDireccion.becomeFirstResponder()
        
        self.playAudio(nombreAudio: nombreAudio.A5)
    }
    
    func limpiarControles()
    {
        //self.errorVI.isHidden = true
        
        self.txtDireccion.text = ""
        self.lookupAddressResults = []
        self.direccionesTV.reloadData()
    }

    func geocodeAddress(address: String!, esRedirect: Bool)
    {
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress + "&region=PERU&components=country:PE"
            geocodeURLString = (geocodeURLString as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue)!
            
            let url = URL(string: geocodeURLString)!
            let request = URLRequest(url: url)
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) -> Void in
                
                do {
                    
                    let dictionary: Dictionary<String, Any> = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                    
                    let status = dictionary["status"] as! String
                    var success: Bool = false
                    
                    if status == "OK"
                    {
                        let allResults = dictionary["results"] as! Array<Dictionary<String, Any>>
                        var filter: Array<Dictionary<String, Any>>! = []
                        
                        for i in 0 ..< allResults.count
                        {
                            let result = allResults[i]

                            let fullAddress = (result["formatted_address"] as! String)
                                
                            if (fullAddress == "Perú" || fullAddress == "Peru")
                            {
                                    
                            }
                            else
                            {
                                filter.append(result)
                            }
                        }
                        
                        self.lookupAddressResults = filter
                        
                        success = true
                    }
                    
                    DispatchQueue.main.async {
                        if (esRedirect)
                        {
                            self.seleccionarDireccion(indice: 0)
                        }
                        else
                        {
                            if (!success)
                            {
                                //self.errorVI.isHidden = false
                                self.lookupAddressResults = []
                            }
                            
                            self.direccionesTV.reloadData()
                        }
                    }
                    
                } catch let error as NSError {
                    
                    print("error: \(error)")
                }
            }
            
            task.resume()
        }
        else {
            
        }
    }
    
    func seleccionarDireccion(indice: Int)
    {
        if (indice >= self.lookupAddressResults.count - 1)
        {
            let result = self.lookupAddressResults[indice]
            
            let fullAddress = (result["formatted_address"] as! String)
            let geometry = result["geometry"] as! Dictionary<String, Any>
            
            self.fetchedAddressLongitude = ((geometry["location"] as! Dictionary<String, Any>)["lng"] as! NSNumber).doubleValue
            self.fetchedAddressLatitude = ((geometry["location"] as! Dictionary<String, Any>)["lat"] as! NSNumber).doubleValue
            
            self.solicitudBean.usuario.direccionUsuario = fullAddress
            self.solicitudBean.usuario.direccionUsuarioLat = String(format: "%.14f", self.fetchedAddressLatitude)
            self.solicitudBean.usuario.direccionUsuarioLon = String(format: "%.14f", self.fetchedAddressLongitude)
        }
        else
        {
            self.solicitudBean.usuario.direccionUsuario = self.txtDireccion.text
            
            if (self.validarLocalizacion())
            {
                self.solicitudBean.usuario.direccionUsuarioLat = String(format: "%.14f", self.actualLocation.coordinate.latitude)
                self.solicitudBean.usuario.direccionUsuarioLon = String(format: "%.14f", self.actualLocation.coordinate.longitude)
                
                
            }
        }

        GlobalVariables.sharedManager.infoSolicitudVerificacionBean = self.solicitudBean
        
        delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
    }
    
    func validarLocalizacion() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.denied)
            {
                let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado para Aquí, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
                
                alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                    
                    UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!);
                    })
                alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                
                self.present(alertaLocalizacion, animated: true, completion: nil)
                
                return false
            }
            else if (status == CLAuthorizationStatus.notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == CLAuthorizationStatus.authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
            
            alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                
                UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
                })
            alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alertaLocalizacion, animated: true, completion: nil)
            
            return false
        }
    }
    
    // MARK: - Actions
    
    @IBAction func accionLimpiarDireccion(sender: AnyObject) {
        
        self.txtDireccion.text = ""
        
        self.limpiarBT.isHidden = true
        self.limpiarIV.isHidden = true
        //self.errorVI.isHidden = true
        
        self.lookupAddressResults = []
        self.direccionesTV.reloadData()
        
        self.txtDireccion.becomeFirstResponder()
    }
    
    @IBAction func accionRegresar(sender: AnyObject) {
        
        delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
    }
    
    // MARK: - UITableViewDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let cant = self.lookupAddressResults.count
        
        if (cant == 0)
        {
            self.direccionesTV.isHidden = true
            /*
            if (self.txtDireccion.text != "")
            {
                self.errorVI.isHidden = false
            }
            else
            {
                self.errorVI.isHidden = true
            }
             */
        }
        else
        {
            self.direccionesTV.isHidden = false
            //self.errorVI.isHidden = true
        }
        
        return cant
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DireccionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! DireccionTableViewCell
             
             let indice = indexPath.row
             print("cell 1 ini")
             
             if (indice <= self.lookupAddressResults.count - 1)
             {
                 let result = self.lookupAddressResults[indice]
                 print("cell 1 fin")
                 let fullAddress = (result["formatted_address"] as! String)
                 let address_components: NSArray = result["address_components"] as! NSArray
                 
                 var detalle = ""
                 var provincia = ""
                 var departamento = ""
                 
                 for i in 0 ..< address_components.count
                 {
                     let dict: NSDictionary = address_components[i] as! NSDictionary
                     let types: NSArray = dict["types"] as! NSArray
                     let long_name = dict["long_name"] as! String
                     
                     for j in 0 ..< types.count
                     {
                         let type: String = types[j] as! String
                         
                         switch type {
                         case "locality": //provincia
                             if (provincia == "")
                             {
                                 provincia = long_name
                             }
                             
                             break
                         case "administrative_area_level_1": //departamento
                             if (departamento == "")
                             {
                                 departamento = long_name
                             }
                             break
                         case "administrative_area_level_2": //departamento
                             if (departamento == "")
                             {
                                 departamento = long_name
                             }
                             break
                         default:
                             break
                         }
                     }
                 }
                 
                 if (!provincia.isEmpty)
                 {
                     detalle += provincia + ", "
                 }
                 if (!departamento.isEmpty)
                 {
                     detalle += departamento + ", "
                 }
                 
                 if (!detalle.isEmpty)
                 {
                     detalle = detalle.trim()
                    detalle = detalle.substringToIndex(index: detalle.count - 1)
                 }
                 
                 cell.direccionLB.text = fullAddress
                 cell.detalleLB.text = detalle
             }
             
             cell.setBorder()
        
             return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        self.seleccionarDireccion(indice: indexPath.row)
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.view.endEditing(true)
    }
    
    // MARK: - CLLocationManagerDelegate Methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        self.actualLocation = location!
        
        self.solicitudBean.usuario.direccionUsuarioLat = String(format: "%.14f", self.actualLocation.coordinate.latitude)
        self.solicitudBean.usuario.direccionUsuarioLon = String(format: "%.14f", self.actualLocation.coordinate.longitude)
        
        self.locationManager.stopUpdatingLocation()

        GlobalVariables.sharedManager.infoSolicitudVerificacionBean = self.solicitudBean

        delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
    }

    // MARK: - UITextFieldDelegate Methods

    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if (textField == self.txtDireccion)
        {
            if (self.txtDireccion.text == "")
            {
                self.limpiarBT.isHidden = true
                self.limpiarIV.isHidden = true
                //self.errorVI.isHidden = true
                
                self.lookupAddressResults = []
                self.direccionesTV.reloadData()
            }
            else
            {
                self.limpiarBT.isHidden = false
                self.limpiarIV.isHidden = false
                
                if (self.txtDireccion.text!.trim().count >= 3)
                {
                    self.geocodeAddress(address: self.txtDireccion.text!, esRedirect: false)
                }
                
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {if (textField === self.txtDireccion) {
            if(self.txtDireccion.text != ""){
                self.seleccionarDireccion(indice: -10)
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if (textField === self.txtDireccion)
        {
            //self.errorVI.isHidden = true
        }
        
        return true
    }
}
