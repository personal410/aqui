//
//  SolicitudVerificacionBean.swift
//  Encasa
//
//  Created by Rommy Fuentes on 14/06/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class SolicitudVerificacionBean: NSObject {

    var error = [ErrorBean]()
    
    var result = [SolicitudVerificacionBean]()
    
    var solicitud  : String? = ""
    var solicitudEmpresa  : String? = ""
    var empresaTipDoc  : String? = ""
    var empresaNroDoc  : String? = ""
    var empresaRazonSocial  : String? = ""
    var logoUrl  : String? = ""
    var motivo  : String? = ""
    var direccion  : String? = ""
    var distrito  : String? = ""
    var departamento  : String? = ""
    var provincia  : String? = ""
    var pais  : String? = ""
    var horaMin  : String? = ""
    var horaMax  : String? = ""
    var fueraHorario  : String? = ""
    var cantidadHoras  : String? = ""
    var cantidadMinutos : String? = ""
    var ultimaOpcion  : String? = ""
    var nroAlerta  : String? = ""
    var fechaRegistro  : String? = ""
    var estado  : String? = ""

    var usuario: UsuarioBean = UsuarioBean()
    //var usuarioBean: UsuarioBean = UsuarioBean()
    //var usuario : NSDictionary = NSDictionary()
    //var direccionUsuario  : String? = ""
    //var direccionUsuarioLat  : String? = ""
    //var direccionUsuarioLon  : String? = ""
    
    var configuracion: ConfiguracionBean = ConfiguracionBean()
    //var configuracionBean: ConfiguracionBean = ConfiguracionBean()
    //var configuracion : NSDictionary = NSDictionary()
    //var intervaloAlerta  : String? = ""
    //var tiempoEsperaAlerta  : String? = ""
    //var frecuenciaControl  : String? = ""
    //var radioControl  : String? = ""
    
    var fotos = [FotoBean]()
    
    var codigo  : String? = ""
    var descripcion  : String? = ""
    var obligatorio  : String? = ""
    var url  : String? = ""

    //var code  : String? = ""
    //var message  : String? = ""

    var cantSegundosAlerta  : String? = ""
}
