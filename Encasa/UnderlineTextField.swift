//
//  UnderlineTextField.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 31/03/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
extension UITextField{
    func setBottomBorder(color:CGColor){
        self.borderStyle = .none;
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color
        border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }    
}
