//
//  CrearPasswordViewController.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 1/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class CrearPasswordViewController: UIViewController,UITextFieldDelegate {

    
    var  StatusChck = "OFF"

    
    @IBOutlet weak var itxtPasswordUno: UITextField!
    @IBOutlet weak var itxtPasswordDos: UITextField!
    @IBOutlet weak var btnCheck: UIButton!
    
    let grayBorderColor = UIColor.fromHex(rgbValue: 0xEFEFF4).cgColor

    override func viewDidLoad() {
        super.viewDidLoad()
        
        itxtPasswordDos.delegate = self
        itxtPasswordUno.delegate = self
        
        self.hideKeyboardWhenTappedAround()

        itxtPasswordDos.setBottomBorder(color: grayBorderColor)
        itxtPasswordUno.setBottomBorder(color: grayBorderColor)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField === itxtPasswordUno) {
            itxtPasswordDos.becomeFirstResponder()
        } else if (textField === itxtPasswordDos) {
            itxtPasswordDos.resignFirstResponder()
        }
        return true
    }
    
    
    @IBAction func accionCheck(sender: AnyObject) {
        
        if (StatusChck == "OFF"){
            btnCheck.setBackgroundImage(UIImage(named:"check_on"), for: .normal)
            StatusChck = "ON"
            
            itxtPasswordUno.isSecureTextEntry = false
            itxtPasswordDos.isSecureTextEntry = false
            
        }else{
            btnCheck.setBackgroundImage(UIImage(named:"check_off"), for: .normal)
            StatusChck = "OFF"
            
            itxtPasswordUno.isSecureTextEntry = true
            itxtPasswordDos.isSecureTextEntry = true
        }
        
    }
    @IBAction func accionContinuar(sender: AnyObject) {
        
        self.showActivityIndicator(uiView: self.view.superview!)
            
        
        
        if itxtPasswordUno.text!.count >= 6 {
            if(itxtPasswordUno.text == itxtPasswordDos.text){
                
                GlobalVariables.sharedManager.infoUsuarioBean?.UsuCla = itxtPasswordUno.text
                
                self.irCrearPassword()
                
            }else{
                
                self.hideActivityIndicator(uiView: self.view.superview!)
                
                let alertController = UIAlertController(title: "Crear cuenta", message:
                    "El campo contraseña no coincide con el campo confirmar contraseña", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
            }
            
        }else{
            
            self.hideActivityIndicator(uiView: self.view.superview!)
            
            let alertController = UIAlertController(title: "Crear cuenta", message:
                "Su contraseña debe tener más de 5 dígitos", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
  
    }
    
    func irCrearPassword(){
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "TerminosCondicionesVC") as UIViewController
        self.present(vc, animated: false, completion: nil)
        
    }
    
    @IBAction func accionBack(sender: AnyObject) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
}
