//
//  CircularHomeButoon.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 18/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class CircularHomeButoon: UIButton {
    
    let whiteColor = UIColor.fromHex(rgbValue: 0xFFFFFF).cgColor
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = layer.bounds.height/2
        layer.borderWidth = 2.0
        layer.borderColor = whiteColor
    }

}
