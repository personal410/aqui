//
//  UsuarioBean.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/23/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class UsuarioBean : NSObject{

    var SesionId  : String? = ""
    var Usuario  : String? = ""
    var UsuCla  : String? = ""
    var ValidaCookie  : String? = ""
    var Origen  : String? = ""
    var UseCod1  : String? = ""
    var UseCod2  : String? = ""
    var GrpObjIni  : String? = ""
    var GrpSisIni  : String? = ""
    var UseCorTra  : String? = ""
    var UseNomCo  : String? = ""
    var UseFlgClave  : String? = ""
    var UseTipDoc  : String? = ""
    var UseFlgContratoCG  : String? = ""
    var UseSesionDia  : String? = ""
    var FchPub  : String? = ""
    var foto  : String? = ""
    var CnhFchPro  : String? = ""
    var CodigoWs  : String? = ""
    var CodigoWS  : String? = ""
    var UseSeIDSession  : String? = ""
    
    var UseFlgBonif  : String? = ""
    var UseCod : String? = ""
    var UseNom : String? = ""
    var UseApePat : String? = ""
    var UseApeMat : String? = ""
    var UseClave : String? = ""
    var UseKey : String? = ""
    var UseEst : String? = ""
    var UseMail : String? = ""
    var UseTip : String? = ""
    var UseNroDoc : String? = ""
    var UsePerfil : String? = ""
    var UseCel : String? = ""
    
    var ApeNom : String? = ""
    var paterno : String? = ""
    var materno : String? = ""
    var nombre : String? = ""
    var AFMVNomCor : String? = ""
    var UsuarioRegistrado : String? = ""
    var codigoValidacion : String? = ""
    
    var FlgErrorRet : String? = ""
    var MENSAJE : String? = ""
    var CelularValido : String? = ""
    var Mensaje : String? = ""
    
    var AFKey : String? = ""
    var Clave : String? = ""
    var CLAVE_SMS : String? = ""
    var CLAVE_SMS_CORREO : String? = ""
    
    var AFNROSMS : String? = ""
    var AFNROCEL : String? = ""
    var AFNROSMSMAIL : String? = ""
    var CLAVESMSVALIDA : String? = ""

    var AFAfiOrigen : String? = ""
    var NroAleatorio : String? = ""
    var Flag : String? = ""
    var Codigo : String? = ""
    
    var modo: String? = ""
    var EsValido: String? = ""
    var ErrorWSDes: String? = ""
    
    var AFSerCod: String? = ""
    var ServicioValido: String? = ""
    
    //var SDT_Usuario = Dictionary<UsuarioBean, UsuarioBean>()
    var SDT_Usuario: NSDictionary = NSDictionary()
    //var UrlFoto: String? = ""
    var STUsuCodInvitacion: String? = ""

    var STCatNomLis: String? = ""
    var STCatNomCta: String? = ""
    
    var AFSerCPTNDisp: Int32? = 0
    var tipoAccesoCambiarContrasenia: String? = ""
    
    var tipoDocConsultado: String? = ""
    var numeroDocConsultado: String? = ""
    var tipoConsultaConsultado: String? = ""
    var paternoConsultado: String? = ""
    var maternoConsultado: String? = ""
    var nombreConsultado: String? = ""
    
    var clickConsultado: String? = ""
    var tipoConsulta: String? = ""
    var encontro: String? = ""
    
    var string: String? = ""
    
    var direccionescrita: String? = ""
    
    var tipoInicio: String? = ""
    //----------------------------------
    var direccionUsuario  : String? = ""
    var direccionUsuarioLat  : String? = ""
    var direccionUsuarioLon  : String? = ""
    //var referenciaUsuario  : String? = ""
}
