//
//  CambiarContraseniaExternoViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 9/06/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift


class CambiarContraseniaExternoViewController: UIViewController ,UITextFieldDelegate{

    
    weak var delegate: LeftMenuProtocol?
    
    @IBOutlet weak var itxtContrasenaAntigua: UITextField!
    @IBOutlet weak var itxtContrasenaNueva: UITextField!
    @IBOutlet weak var itxtContrasenaConfirmar: UITextField!
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    let grayBorderColor = UIColor.fromHex(rgbValue: 0xEFEFF4).cgColor
    var usuarioBean:UsuarioBean!
    var modo:NSString = ""
    var dniGuardado:NSString = ""
    var contraseniaGuardado:NSString = ""
    
    //si ingresa desde el login o si ingresa del menu
    var tipoAccesoCambiarContrasenia = ""
    
    var  StatusChck = "OFF"
    @IBOutlet weak var btnCheck: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itxtContrasenaAntigua.delegate = self
        itxtContrasenaNueva.delegate = self
        itxtContrasenaConfirmar.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean!
        itxtContrasenaAntigua.setBottomBorder(color: grayBorderColor)
        itxtContrasenaNueva.setBottomBorder(color: grayBorderColor)
        itxtContrasenaConfirmar.setBottomBorder(color: grayBorderColor)
        
        
        itxtContrasenaAntigua.text = ""
        itxtContrasenaNueva.text = ""
        itxtContrasenaConfirmar.text = ""
        
        tipoAccesoCambiarContrasenia = usuarioBean!.tipoAccesoCambiarContrasenia!

        
        if(usuarioBean.UseFlgClave == "N"){
            
            itxtContrasenaAntigua.isHidden = true
            
        }else{
            
            itxtContrasenaAntigua.isHidden = false
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField === itxtContrasenaAntigua) {
            itxtContrasenaNueva.becomeFirstResponder()
        } else if (textField === itxtContrasenaNueva) {
            itxtContrasenaConfirmar.becomeFirstResponder()
        } else if (textField === itxtContrasenaConfirmar) {
            itxtContrasenaConfirmar.resignFirstResponder()
        }
        return true
    }
    
    
    @IBAction func accionCheck(sender: AnyObject) {
        
        if (StatusChck == "OFF"){
            btnCheck.setBackgroundImage(UIImage(named:"check_on"), for: .normal)
            StatusChck = "ON"
            
            itxtContrasenaAntigua.isSecureTextEntry = false
            itxtContrasenaNueva.isSecureTextEntry = false
            itxtContrasenaConfirmar.isSecureTextEntry = false
            
        }else{
            btnCheck.setBackgroundImage(UIImage(named:"check_off"), for: .normal)
            StatusChck = "OFF"
            
            itxtContrasenaAntigua.isSecureTextEntry = true
            itxtContrasenaNueva.isSecureTextEntry = true
            itxtContrasenaConfirmar.isSecureTextEntry = true
            
        }
        
    }
    
    @IBAction func accionContinuar(sender: AnyObject) {
        
        if(usuarioBean.UseFlgClave != "N"){
            
            
            if(itxtContrasenaAntigua.text != "" && itxtContrasenaNueva.text != "" && itxtContrasenaConfirmar.text != ""){
                
                if(itxtContrasenaNueva.text == "" || itxtContrasenaNueva.text!.count <= 5){
                    
                    
                    let alertController = UIAlertController(title: "Atención", message:
                        "Su nueva contraseña debe tener más de 5 dígitos", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else if(itxtContrasenaConfirmar.text != itxtContrasenaNueva.text){
                    
                    
                    let alertController = UIAlertController(title: "Atención", message:
                        "El campo nueva contraseña no coincide con el campo confirmar contraseña", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    
                    self.iniCambiarClave()
                    
                }
                
            }else if(itxtContrasenaAntigua.text == ""){
                
                
                let alertController = UIAlertController(title: "Atención", message:
                    "Debe ingresar su contraseña anterior", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(itxtContrasenaNueva.text == "" || itxtContrasenaNueva.text!.count <= 5){
                
                
                let alertController = UIAlertController(title: "Atención", message:
                    "Su nueva contraseña debe tener más de 5 dígitos", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(itxtContrasenaConfirmar.text != itxtContrasenaNueva.text){
                
                
                let alertController = UIAlertController(title: "Atención", message:
                    "El campo nueva contraseña no coincide con el campo confirmar contraseña", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            
        }else{
            
            
            if(itxtContrasenaNueva.text != "" && itxtContrasenaConfirmar.text != ""){
                
                if(itxtContrasenaNueva.text == "" || itxtContrasenaNueva.text!.count <= 5){
                    
                    
                    let alertController = UIAlertController(title: "Atención", message:
                        "Su nueva contraseña debe tener más de 5 dígitos", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else if(itxtContrasenaConfirmar.text != itxtContrasenaNueva.text){
                    
                    
                    let alertController = UIAlertController(title: "Atención", message:
                        "El campo nueva contraseña no coincide con el campo confirmar contraseña", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    
                    self.iniCambiarClave()
                    
                }
                
            }else if(itxtContrasenaNueva.text == "" || itxtContrasenaNueva.text!.count <= 5){
                
                
                let alertController = UIAlertController(title: "Atención", message:
                    "Su nueva contraseña debe tener más de 5 dígitos", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else if(itxtContrasenaConfirmar.text != itxtContrasenaNueva.text){
                
                
                let alertController = UIAlertController(title: "Atención", message:
                    "El campo nueva contraseña no coincide con el campo confirmar contraseña", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        }
        
        
        
    }
    
    
    func iniCambiarClave(){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        //if(tipoAccesoCambiarContrasenia == "L"){
        if(usuarioBean.UseFlgClave == "N"){
            
            NotificationCenter.default.addObserver(self, selector: #selector(CambiarContrasenaViewController.endCambiarClave(notification:)), name: NSNotification.Name(rawValue: "endCambiarClave"), object: nil)
            
            let parametros = [
                "Usuario" : usuarioBean!.UseNroDoc! as String,
                "IdSesion" : usuarioBean!.UseSeIDSession! as String,
                "UseCod" : usuarioBean!.UseNroDoc! as String,
                "Caso" : "RYA" as String,
                "UseClave" : "" as String,
                "UseClaveNuev" : itxtContrasenaNueva!.text! as String,
                "UseClaveConf" : itxtContrasenaConfirmar!.text! as String,
                ]
            
            OriginData.sharedInstance.cambiarClave(notificacion: "endCambiarClave", parametros: parametros as NSDictionary)
            
        }else{
            
            NotificationCenter.default.addObserver(self, selector: #selector(CambiarContrasenaViewController.endCambiarClave(notification:)), name:NSNotification.Name(rawValue: "endCambiarClave"), object: nil)
            
            let parametros = [
                "Usuario" : usuarioBean!.UseNroDoc! as String,
                "IdSesion" : usuarioBean!.UseSeIDSession! as String,
                "UseCod" : usuarioBean!.UseNroDoc! as String,
                "Caso" : "USU" as String,
                "UseClave" : itxtContrasenaAntigua!.text! as String,
                "UseClaveNuev" : itxtContrasenaNueva!.text! as String,
                "UseClaveConf" : itxtContrasenaConfirmar!.text! as String,
                ]
            
            OriginData.sharedInstance.cambiarClave(notificacion: "endCambiarClave", parametros: parametros as NSDictionary)
            
        }
        
    }
    
    func endCambiarClave(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let data = notification.object as! UsuarioBean;
        
        if(data.Codigo == "0"){
            if(data.EsValido == "N"){
                
                let alertController = UIAlertController(title: "", message:
                    data.MENSAJE, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }else{
                
                let alertController = UIAlertController(title: "Atención", message:
                    "Su contraseña ha sido modificada", preferredStyle: .alert)
                
                //alertController.addAction(UIAlertAction(title:"Terminar", style: .default,handler: nil))
                
                alertController.addAction(UIAlertAction(title: "Terminar", style: UIAlertAction.Style.destructive, handler: { action in
                    
                    self.irHome()
                    
                }))
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        }else{
            
            self.obtenerMensajeError(codigo: data.codigoValidacion!)
        }
        
    }
    
    
    func irHome(){
        

            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let menuLeftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuLeftVC") as! MenuLeftViewController
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        menuLeftViewController.setDefaultViewController(defaultViewController: homeViewController)
            let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: menuLeftViewController)
            
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            SlideMenuOptions.contentViewScale = 1
            
            self.present(slideMenuController, animated: false, completion: nil)
        
        //self.navigationController!.pushViewController(slideMenuController, animated: true)
    
    }
    
    @IBAction func accionMenu(sender: AnyObject) {
        
        self.slideMenuController()?.openLeft()
        
    }
    
    @IBAction func accionHome(sender: AnyObject) {
        
        delegate?.changeViewController(menu: LeftMenu.Home)
    }

}
