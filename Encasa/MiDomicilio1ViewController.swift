//
//  MiDomicilio1ViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 26/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import CoreLocation
import AVFoundation

class MiDomicilio1ViewController: ParentViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UITextFieldDelegate{
    
    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var txtDireccion: TextField!
    @IBOutlet var limpiarIV: UIImageView!
    @IBOutlet weak var limpiarBT: UIButton!
    @IBOutlet var mensajeVI: borderShadowView!
    @IBOutlet var buscarBT: BorderButton!
    @IBOutlet var viewFondo: UIView!
    @IBOutlet var direccionSiVI: UIView!
    @IBOutlet var direccionNoVI: UIView!
    @IBOutlet var errorVI: borderShadowView!
    @IBOutlet weak var marcadorIV: UIImageView!
    @IBOutlet weak var muevaLB: UILabel!
    @IBOutlet weak var referenciaLB: UILabel!
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    var usuarioBean:UsuarioBean!
    var solicitudBean: SolicitudVerificacionBean!
    
    let locationManager = CLLocationManager()
    //var actualLocation = CLLocation()
    var selectLatitud: Double!
    var selectLongitud: Double!
    var esBuscarReferencia: Bool = false
    
    // MARK: - GoogleMaps Variables
    
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    var lookupAddressResults: Dictionary<String, Any>!
    var fetchedFormattedAddress: String!
    var fetchedAddressLongitude: Double!
    var fetchedAddressLatitude: Double!
    
    var estadoAudio = true
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.inicializar()
    }

    override func viewDidAppear(_ animated: Bool) {
        
        if (self.esBuscarReferencia)
        {
            self.setearDireccion()
        }
        else
        {
            self.setearDatos()
        }

        self.esBuscarReferencia = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.locationManager.stopUpdatingLocation()
        
        self.stopAudio()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inicializar()
        self.playAudio(nombreAudio: nombreAudio.A3y4)
    }
    // MARK: - Methods
    
    func inicializar()
    {
        //self.audioIV.animationImageSecuence()
        
//        let image1:UIImage = UIImage(named: "ic_audio1")!
//        let image2:UIImage = UIImage(named: "ic_audio2")!
//        let image3:UIImage = UIImage(named: "ic_audio3")!
//        self.parlanteBT.setImage(image1, forState: .normal)
//        self.parlanteBT.imageView!.animationImages = [image1, image2, image3]
//        self.parlanteBT.imageView!.animationDuration = 1.5
//        self.parlanteBT.imageView!.startAnimating()
        //estadoAudio = true
        
        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)

        
        self.txtDireccion.delegate = self
        self.txtDireccion.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.locationManager.delegate = self
        self.mapView.delegate = self
        
        self.mensajeVI.isHidden = true
        self.marcadorIV.isHidden = true
        
        self.limpiarIV.isHidden = true
        self.limpiarBT.isHidden = true
        
        self.errorVI.isHidden = true
        
        self.viewFondo.backgroundColor = UIColorFromHex(rgbValue: 0x333333, alpha: 0.9)
        
        self.muevaLB.roundCorners(corners: UIRectCorner.allCorners, radius: 6)
        
        self.direccionSiVI.layer.shadowRadius = 2
        self.direccionSiVI.layer.shadowOpacity = 0.5
        self.direccionSiVI.layer.shadowOffset = .zero
        self.direccionSiVI.layer.shadowColor = UIColor.black.cgColor
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        self.configurarMapa()
    }
    
    func setearDatos()
    {
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        self.mensajeVI.isHidden = true
        self.marcadorIV.isHidden = true
        self.errorVI.isHidden = true
        
        self.mostrarDireccionSi(mostrar: false)
        self.mostrarDireccionNo(mostrar: false)
        
        self.viewFondo.isHidden = true
        
        if (self.solicitudBean.usuario.direccionUsuarioLat != "" && self.solicitudBean.usuario.direccionUsuarioLon != "")
        {
            self.mensajeVI.isHidden = false
            self.marcadorIV.isHidden = false
            
            self.buscarBT.setTitle("SIGUIENTE", for: .normal)
        }
        else
        {
            self.buscarBT.setTitle("BUSCAR DIRECCIÓN", for: .normal)
        }
        
        self.validarLocalizacion()
        
        self.txtDireccion.text = self.solicitudBean.usuario.direccionUsuario
        
        if (self.solicitudBean.usuario.direccionUsuarioLat != "" && self.solicitudBean.usuario.direccionUsuarioLon != "")
        {
            let latitud: Double! = Double(self.solicitudBean.usuario.direccionUsuarioLat!)
            let longitud: Double! = Double(self.solicitudBean.usuario.direccionUsuarioLon!)
            
            self.actualizarMapa(newLocation: CLLocation(latitude: latitud, longitude: longitud))
        }
        
        self.validarLimpiar()
        
        self.playAudio(nombreAudio: nombreAudio.A3y4)
    }
    
    func setearDireccion()
    {
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        if (self.solicitudBean.usuario.direccionUsuario! != "")
        {
            self.limpiarBT.isHidden = false
            self.limpiarIV.isHidden = false
        }
        
        if (self.solicitudBean.usuario.direccionUsuarioLat != "" && self.solicitudBean.usuario.direccionUsuarioLon != "")
        {
            let direccion: String! = self.solicitudBean.usuario.direccionUsuario!
            let latitud: Double! = Double(self.solicitudBean.usuario.direccionUsuarioLat!)
            let longitud: Double! = Double(self.solicitudBean.usuario.direccionUsuarioLon!)
            
            self.fetchedFormattedAddress = direccion
            self.fetchedAddressLatitude = latitud
            self.fetchedAddressLongitude = longitud
            
            self.txtDireccion.text = self.solicitudBean.usuario.direccionUsuario
            
            self.actualizarMapa(newLocation: CLLocation(latitude: self.fetchedAddressLatitude, longitude: self.fetchedAddressLongitude))
            
            self.despuesBuscar()
        }
    }
    
    func validarLocalizacion() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == .authorizedWhenInUse || status == .denied)
            {
                let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado para Aquí, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
                
                alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                    
                    UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL);
                    })
                alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                
                self.present(alertaLocalizacion, animated: true, completion: nil)
                
                return false
            }
            else if (status == .notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == .authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
            
            alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                
                UIApplication.shared.openURL(NSURL(string: "prefs:root=LOCATION_SERVICES")! as URL)
                })
            alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alertaLocalizacion, animated: true, completion: nil)
            
            return false
        }
    }
    
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress + "&components=country:PE"
            geocodeURLString = (geocodeURLString as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue)!
            
            let geocodeURL = NSURL(string: geocodeURLString)
            
            DispatchQueue.main.async {
                let geocodingResultsData = NSData(contentsOf: geocodeURL! as URL)
                
                // let error: NSError?
                
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: geocodingResultsData! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                    
                    let status = dictionary["status"] as! String
                    
                    if status == "OK" {
                        let allResults = dictionary["results"] as! Array<Dictionary<String, Any>>
                        self.lookupAddressResults = allResults[0]
                        
                        // Keep the most important values.
                        self.fetchedFormattedAddress = self.lookupAddressResults["formatted_address"] as! String
                        let geometry = self.lookupAddressResults["geometry"] as! Dictionary<String, Any>
                        self.fetchedAddressLongitude = ((geometry["location"] as! Dictionary<String, Any>)["lng"] as! NSNumber).doubleValue
                        self.fetchedAddressLatitude = ((geometry["location"] as! Dictionary<String, Any>)["lat"] as! NSNumber).doubleValue
                        
                        self.locationManager.stopUpdatingLocation()
                        
                        self.actualizarMapa(newLocation: CLLocation(latitude: self.fetchedAddressLatitude, longitude: self.fetchedAddressLongitude))
                        
                        self.despuesBuscar()
                        
                        completionHandler(status, true)
                    }
                    else if (status == "ZERO_RESULTS")
                    {
                        self.errorVI.isHidden = false
                        
                        completionHandler(status, false)
                    }
                    else {
                        
                        self.errorVI.isHidden = false
                        
                        completionHandler(status, false)
                    }
                    
                }catch{
                    
                    self.errorVI.isHidden = false
                    
                    completionHandler("", false)
                    print("error")
                }
            }
        }
        else {
            completionHandler("No valid address.", false)
        }
    }

    private func configurarMapa() {
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.clear()
    }
    
    private func actualizarMapa(newLocation: CLLocation)
    {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: 17.0)
        self.mapView.animate(to: camera)
    }
    
    func validarDistancia()
    {
        let marcadorLocation = CLLocation(latitude: self.selectLatitud, longitude: self.selectLongitud)
        print("validarDistancia lon: \(marcadorLocation.coordinate.longitude) y lat: \(marcadorLocation.coordinate.latitude)")
        
        let distanceInMeters = self.actualLocation.distance(from: marcadorLocation)
        print("Distancia entre puntos: \(distanceInMeters) m.")
        
        // Si esta dentro de radio
        let radioControl = Double(self.solicitudBean.configuracion.radioControl!) ?? 0
        if (distanceInMeters <= radioControl)
        {
            self.mostrarDireccionSi(mostrar: true)
        }
        else
        {
            self.mostrarDireccionNo(mostrar: true)
        }
    }
    
    func mostrarDireccionSi(mostrar: Bool)
    {
        if (mostrar)
        {
            self.viewFondo.isHidden = false
            self.direccionSiVI.isHidden = false
            self.direccionNoVI.isHidden = true
        }
        else
        {
            self.viewFondo.isHidden = true
            self.direccionSiVI.isHidden = true
            self.direccionNoVI.isHidden = true
        }
        
    }
    
    func mostrarDireccionNo(mostrar: Bool)
    {
        if (mostrar)
        {
            self.viewFondo.isHidden = false
            self.direccionSiVI.isHidden = true
            self.direccionNoVI.isHidden = false
        }
        else
        {
            self.viewFondo.isHidden = true
            self.direccionSiVI.isHidden = true
            self.direccionNoVI.isHidden = true
        }
    }
    
    func validarLimpiar()
    {
        if (self.txtDireccion.text == "")
        {
            self.limpiarIV.isHidden = true
            self.limpiarBT.isHidden = true
            
            self.mensajeVI.isHidden = true
            self.marcadorIV.isHidden = true
            
            self.errorVI.isHidden = true
            
            self.buscarBT.setTitle("BUSCAR DIRECCIÓN", for: .normal)
        }
        else
        {
            self.limpiarIV.isHidden = false
            self.limpiarBT.isHidden = false
        }
    }
    
    func despuesBuscar()
    {
        self.mensajeVI.isHidden = false
        self.marcadorIV.isHidden = false
        
        self.buscarBT.setTitle("SIGUIENTE", for: .normal)
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func accionLimpiarDireccion(sender: AnyObject) {
        
        self.solicitudBean.usuario.direccionUsuario = ""
        self.solicitudBean.usuario.direccionUsuarioLat = ""
        self.solicitudBean.usuario.direccionUsuarioLon = ""
        
        self.txtDireccion.text = ""
        self.limpiarIV.isHidden = true
        self.limpiarBT.isHidden = true
        self.mensajeVI.isHidden = true
        self.marcadorIV.isHidden = true
        self.errorVI.isHidden = true
        self.buscarBT.setTitle("BUSCAR DIRECCIÓN", for: .normal)
        
        self.validarLocalizacion()
    }
    
    @IBAction func accionRegresar(sender: AnyObject) {
        
        delegate?.changeViewController(menu: LeftMenu.Home)
    }
    
    @IBAction func accionBuscar(sender: AnyObject) {
        
        if (self.txtDireccion.text == "")
        {
            let alertController = UIAlertController(title: "Notificacion", message:
                "Por favor ingrese la direccion de su domicilio", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            if (self.buscarBT.currentTitle == "BUSCAR DIRECCIÓN")
            {
                self.mapView.clear()
                
                self.geocodeAddress(address: self.txtDireccion.text, withCompletionHandler: { (status, success) -> Void in
                    print("llamado " ,status, success)
                    
                    if (!success)
                    {
                        
                    }
                })
            }
            else
            {
                self.iniIngresarDireccion()
            }
        }
    }
    
    @IBAction func accionBuscarReferencia(sender: AnyObject) {
        
        self.esBuscarReferencia = true
        
        delegate?.changeViewController(menu: LeftMenu.BuscarDirecciones)
    }
    
    @IBAction func accionAceptarSi(sender: AnyObject) {
        
        self.mostrarDireccionSi(mostrar: false)
        
        delegate?.changeViewController(menu: LeftMenu.Fotos)
    }
    
    @IBAction func accionCancelarSi(sender: AnyObject) {
        
        self.mostrarDireccionSi(mostrar: false)
    }
    
    @IBAction func accionAceptarNo(sender: AnyObject) {
        
        self.mostrarDireccionNo(mostrar: false)
        
        delegate?.changeViewController(menu: LeftMenu.MiDomicilio2)
    }
    
    // MARK: - WebServices Methods
    
    func iniIngresarDireccion(){
        
        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MiDomicilio1ViewController.endIngresarDireccion(_:)), name:NSNotification.Name(rawValue: "endIngresarDireccion"), object: nil)
        
        let parametros = [
            "usuario"   :  self.usuarioBean!.UseNroDoc! as String,
            "sesionId"  :  self.usuarioBean!.UseSeIDSession! as String,
            "empresaTipDoc" :  self.solicitudBean.empresaTipDoc! as String,
            "empresaNroDoc" :  self.solicitudBean.empresaNroDoc! as String,
            "solicitudCodigo"   :  self.solicitudBean.solicitud! as String,
            "solicitudCodigoEmp"   :  self.solicitudBean.solicitudEmpresa! as String,
            "direccion" :  self.txtDireccion.text! as String,
            "latitud" : String(format: "%.14f", self.selectLatitud),
            "longitud" :  String(format: "%.14f", self.selectLongitud),
            "plataforma" :  "IOS"
        ]
        
        OriginData.sharedInstance.ingresarDireccion(notificacion: "endIngresarDireccion", parametros: parametros as NSDictionary)
    }
    
    @objc func endIngresarDireccion(_ notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as? ResultadoBean?
        
        if (data!?.error.count == 0)
        {
            self.solicitudBean.usuario.direccionUsuario = self.txtDireccion.text
            self.solicitudBean.usuario.direccionUsuarioLat = String(format: "%.14f", self.selectLatitud)
            self.solicitudBean.usuario.direccionUsuarioLon = String(format: "%.14f", self.selectLongitud)
            
            GlobalVariables.sharedManager.infoSolicitudVerificacionBean = self.solicitudBean
            
            //delegate?.changeViewController(LeftMenu.MiDomicilio2)
            
            self.stopAudio()
            
            self.validarDistancia()
        }
        else
        {
            let errorBean: ErrorBean = data!?.error[0] as! ErrorBean
            
            self.mostrarAlertaErrorWS(codigo: errorBean.code, mensaje: errorBean.message)
        }
    }
    
    // MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        self.dismissKeyboard()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
        
        self.locationManager.stopUpdatingLocation()
        
        print("cameraPosition.target: \(cameraPosition.target)")
        self.selectLatitud = cameraPosition.target.latitude
        self.selectLatitud = self.selectLatitud.roundToPlaces(places: 14)
        self.selectLongitud = cameraPosition.target.longitude
        self.selectLongitud = self.selectLongitud.roundToPlaces(places: 14)
    }

    // MARK: - CLLocationManagerDelegate Methods
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.authorizedAlways)
        {
            print("GPS habilitado siempre.")
            self.mapView.isMyLocationEnabled = true
        }
        else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            print("GPS habilitado cuando esté en uso.")
            self.mapView.isMyLocationEnabled = true
        }
        else if (status == CLAuthorizationStatus.denied)
        {
            print("GPS denegado.")
            //UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let longitud = location!.coordinate.longitude
        let latitud = location!.coordinate.latitude
        
        print("MiDomicilio1 lon: \(longitud) y lat: \(latitud)")
        
        if (self.buscarBT.currentTitle == "BUSCAR DIRECCIÓN")
        {
            self.actualizarMapa(newLocation: CLLocation(latitude: latitud, longitude: longitud))
        }
        
        self.actualLocation = location!
    }
    
    // MARK: - UITextFieldDelegate Methods
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        if (textField == self.txtDireccion)
        {
            self.validarLimpiar()
        }
    }
    
    override func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField === self.txtDireccion) {
            
            if(self.txtDireccion.text != ""){
                
                self.mapView.clear()
                
                self.geocodeAddress(address: self.txtDireccion.text, withCompletionHandler: { (status, success) -> Void in
                    print("llamado " ,status, success)
                    
                    if (!success)
                    {
                        
                    }
                })
                
            }else{
            }
            
            self.txtDireccion.resignFirstResponder()
        }
        
        return true
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if (textField === self.txtDireccion)
        {
            self.mensajeVI.isHidden = true
            self.marcadorIV.isHidden = true
            self.errorVI.isHidden = true
            self.buscarBT.setTitle("BUSCAR DIRECCIÓN", for: .normal)
            
            self.esBuscarReferencia = true
            
            delegate?.changeViewController(menu: LeftMenu.BuscarDirecciones)
        }
        
        return false
        //return true
    }
    
    
    @IBAction func parlanteAction(sender: AnyObject) {
        
        if (self.audioPlayer.isPlaying == false)
        {
            self.audioPlayer.play()
            //self.parlanteBT.imageView!.startAnimating()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
        }
    }
}

