//
//  ActualizarTerminosCondicionesViewController.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 1/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ActualizarTerminosCondicionesViewController: UIViewController {

    
    var  StatusChck = "OFF"
    var usuarioBean:UsuarioBean?
    var  nrocontrato = "1"
    
    @IBOutlet weak var contrato1: UITextView!
    @IBOutlet weak var contrato2: UITextView!
    @IBOutlet weak var btnCheck: UIButton!
    var ClaveGuardad: String!
    var UsuarioGuardad: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        self.agregarTap1()
        self.agregarTap2()
        self.cargarContrato1()
        self.cargarContrato2()
        // Do any additional setup after loading the view.
    }
    func agregarTap1(){
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(ActualizarTerminosCondicionesViewController.textTapped1(recognizer:)))
        tap1.numberOfTapsRequired = 1
        contrato1.addGestureRecognizer(tap1)
        
    }
    
    func agregarTap2(){
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(ActualizarTerminosCondicionesViewController.textTapped2(recognizer:)))
        tap2.numberOfTapsRequired = 1
        contrato2.addGestureRecognizer(tap2)

    }
    func cargarContrato1(){
        
        // 1
        let string = "\(contrato1.text) \n\nSegundo contrato de registro" as NSString
        let attributedString = NSMutableAttributedString(string: string as String)
        
        // 2
        let firstAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.backgroundColor: UIColor.clear]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.backgroundColor: UIColor.clear, NSAttributedString.Key.underlineStyle: 1] as [NSAttributedString.Key : Any]
        
        // 3
        attributedString.addAttributes(firstAttributes, range: string.range(of: contrato1.text))
        attributedString.addAttributes(secondAttributes, range: string.range(of: "Segundo contrato de registro"))
        
        // 4
        contrato1.attributedText = attributedString
        contrato1.textAlignment = NSTextAlignment.justified

    }
    
    func cargarContrato2(){
        
        // 1
        let string = "\(contrato2.text) \n\nVolver al primer contrato" as NSString
        let attributedString = NSMutableAttributedString(string: string as String)
        
        // 2
        let firstAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.backgroundColor: UIColor.clear]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.backgroundColor: UIColor.clear, NSAttributedString.Key.underlineStyle: 1] as [NSAttributedString.Key : Any]
        
        // 3
        attributedString.addAttributes(firstAttributes, range: string.range(of: contrato2.text))
        attributedString.addAttributes(secondAttributes, range: string.range(of: "Volver al primer contrato"))
        
        // 4
        contrato2.attributedText = attributedString
        contrato2.textAlignment = NSTextAlignment.justified


    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func textTapped1(recognizer: UITapGestureRecognizer){
        
        if let textView = recognizer.view as? UITextView {
            
            if let layoutManager: NSLayoutManager = textView.layoutManager {
                
                var location: CGPoint = recognizer.location(in: textView)
                location.x -= textView.textContainerInset.left
                location.y -= textView.textContainerInset.top
                
                let charIndex = layoutManager.characterIndex(for: location, in: textView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
                
                if charIndex < textView.textStorage.length {
                    
                    var range = NSRange()
                    //let range = NSRange(location: 0, length: 0)
                    
                    let attributes : NSDictionary =  textView.textStorage.attributes(at: charIndex, effectiveRange: &range) as NSDictionary
                    
                    let valor = attributes.object(forKey: "NSUnderline")
                    
                    if(valor != nil){
                        self .cambiarContrato()

                    }
                }
            }
        }
    }
    @objc func textTapped2(recognizer: UITapGestureRecognizer){
        
        if let textView = recognizer.view as? UITextView {
            
            if let layoutManager: NSLayoutManager = textView.layoutManager {
                
                var location: CGPoint = recognizer.location(in: textView)
                location.x -= textView.textContainerInset.left
                location.y -= textView.textContainerInset.top
                
                let charIndex = layoutManager.characterIndex(for: location, in: textView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
                
                if charIndex < textView.textStorage.length {
                    
                    var range = NSRange()
                    //let range = NSRange(location: 0, length: 0)
                    
                    let attributes : NSDictionary =  textView.textStorage.attributes(at: charIndex, effectiveRange: &range) as NSDictionary
                    
                    let valor = attributes.object(forKey: "NSUnderline")
                    
                    if(valor != nil){
                        self .cambiarContrato()
                        

                    }
                }
            }
        }
    }

    @IBAction func accionAcepto(sender: AnyObject) {
        
        if(StatusChck == "ON"){
            
            self .iniGenerarPassword()
            
        }else{
            
            let alertController = UIAlertController(title: "Advertencia", message:
                "Tiene que aceptar los terminos y  condiciones", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    func iniGenerarPassword(){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endGenerarPassword(notification:)), name:NSNotification.Name(rawValue: "endGenerarPassword"), object: nil)
        
        let parametros = [
            "clave" : usuarioBean!.UsuCla! as String,
            ]
        OriginData.sharedInstance.generarPassword(notificacion: "endGenerarPassword", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endGenerarPassword(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        GlobalVariables.sharedManager.infoUsuarioBean?.AFKey = dato?.AFKey
        GlobalVariables.sharedManager.infoUsuarioBean?.Clave = dato?.Clave
        
        
        self.iniAceptaContratoUsu()
        
        
    }
    
    func iniAceptaContratoUsu(){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endAceptaContratoUsu(notification:)), name:NSNotification.Name(rawValue: "endAceptaContratoUsu"), object: nil)
        
        let parametros = [
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "UseSeIDSession" : usuarioBean!.SesionId! as String,
            "UseFlgContratoCG" : "S" as String
        ]
        OriginData.sharedInstance.AceptaContratoUsu(notificacion: "endAceptaContratoUsu", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endAceptaContratoUsu(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.CodigoWS == "0"){
            
            self.iniAceptarContrato()
            
        }
        
    }
    
    func iniAceptarContrato(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endAceptaContratoUsu(notification:)), name:NSNotification.Name(rawValue: "endAceptarContrato"), object: nil)
        
        let parametros = [
            "pAFTipDocu":"D" as String,
            "pAFNroDocu": usuarioBean!.UseNroDoc! as String,
            "pAFApePat": usuarioBean!.UseApePat! as String,
            "pAFApeMat": usuarioBean!.UseApeMat! as String,
            "pAFNombres": usuarioBean!.UseNom! as String,
            "pAFNroCel": usuarioBean!.UseCel! as String,
            "pAFEmail":usuarioBean!.UseMail! as String ,
            "pAFNroSMS": usuarioBean!.CLAVE_SMS! as String,
            "pAFEstReg": "P" as String,
            "pAFCodInvita": "" as String,
            "pAFCodInv": "0" as String,
            "AFKey": usuarioBean!.AFKey! as String,
            "AFPswd": usuarioBean!.UsuCla! as String,
            "caso": "REG2" as String,
            "pAFAfiFNac": "" as String,
            "pAFAfiFNacD": "0" as String,
            "pAFAfiFNacM": "0" as String,
            "pAFAfiFNacY": "0" as String,
            "Usuario": usuarioBean!.UseNroDoc! as String,
            "SesionId":usuarioBean!.SesionId! as String ,
            "AFAceCon": "S" as String,
            "AFAceConTip": nrocontrato
        ]
        
        OriginData.sharedInstance.aceptarContrato(notificacion: "endAceptarContrato", parametros: parametros as NSDictionary)
        
    }
    
    func endAceptarContrato(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.codigoValidacion == "0"){
            
            GlobalVariables.sharedManager.infoUsuarioBean?.NroAleatorio = dato?.NroAleatorio
            GlobalVariables.sharedManager.infoUsuarioBean?.AFAfiOrigen = dato?.AFAfiOrigen
            
            self.iniAFPActRegYa()
        }
        
    }
    func iniAFPActRegYa(){
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endAFPActRegYa(notification:)), name:NSNotification.Name(rawValue: "endAFPActRegYa"), object: nil)
        
        let parametros = [
            "modo" : "2" as String,
            "pAFTipDocu" : "D" as String,
            "pAFNroDocu" : usuarioBean!.UseNroDoc! as String,
            "AFKey" : usuarioBean!.AFKey! as String,
            "AFPswd" :usuarioBean!.UsuCla! as String,
            "AFRandom" : usuarioBean!.NroAleatorio! as String,
            "AFAceCon" : "S" as String,
            "pAFNroCel" : usuarioBean!.UseCel! as String,
            "pAFEmail" : usuarioBean!.UseMail! as String,
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "SesionId" : usuarioBean!.SesionId! as String
        ]
        OriginData.sharedInstance.AFPActRegYa(notificacion: "endAFPActRegYa", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endAFPActRegYa(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato!.codigoValidacion == "0"){
            
            self.iniValidarLogin()
            
        }
        
        self.hideActivityIndicator(uiView: self.view.superview!)
    }
    
    
    @IBAction func accionNoAcepto(sender: AnyObject) {
        
        self.iniNoAceptaContratoUsu()
    }
    func iniNoAceptaContratoUsu(){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endNoAceptaContratoUsu(notification:)), name:NSNotification.Name(rawValue: "endNoAceptaContratoUsu"), object: nil)
        
        let parametros = [
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "UseSeIDSession" : usuarioBean!.SesionId! as String,
            "UseFlgContratoCG" : "N" as String
        ]
        OriginData.sharedInstance.AceptaContratoUsu(notificacion: "endNoAceptaContratoUsu", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endNoAceptaContratoUsu(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.CodigoWS == "0"){
            
            self.iniNoAFPActRegYa()
            
        }
    }
    
    func iniNoAFPActRegYa(){
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endNoAFPActRegYa(notification:)), name:NSNotification.Name(rawValue: "endNoAFPActRegYa"), object: nil)
        
        let parametros = [
            "modo" : "2" as String,
            "pAFTipDocu" : "D" as String,
            "pAFNroDocu" : usuarioBean!.UseNroDoc! as String,
            "AFKey" : "" as String,
            "AFPswd" : "" as String,
            "AFRandom" : "0" as String,
            "AFAceCon" : "N" as String,
            "pAFNroCel" : "" as String,
            "pAFEmail" : "" as String,
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "SesionId" : usuarioBean!.SesionId! as String
        ]
        OriginData.sharedInstance.AFPActRegYa(notificacion: "endNoAFPActRegYa", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endNoAFPActRegYa(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let dato = notification.object as? UsuarioBean;
        
        if(dato?.codigoValidacion == "0"){
            
            self.irInicio()
            
        }
        
        self.hideActivityIndicator(uiView: self.view.superview!)
    }
    func irInicio(){
        
        let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as UIViewController
        self.present(vc, animated: false, completion: nil)
        
    }
    
    
    func cambiarContrato(){
        
        
        contrato2.scrollRangeToVisible(NSRange(location:0, length:0))
        contrato1.scrollRangeToVisible(NSRange(location:0, length:0))
        
        contrato2.setContentOffset(CGPoint.zero, animated: true)
        contrato1.setContentOffset(CGPoint.zero, animated: true)
        
        if(nrocontrato == "1"){
            contrato1.isHidden = true
            contrato2.isHidden = false
            nrocontrato = "2"
        }else{
            contrato1.isHidden = false
            contrato2.isHidden = true
            nrocontrato = "1"
        }
        
    }
    @IBAction func accionCheck(sender: AnyObject) {
        
        if (StatusChck == "OFF"){
            btnCheck.setBackgroundImage(UIImage(named:"check_on"), for: .normal)
            StatusChck = "ON"
            
        }else{
            btnCheck.setBackgroundImage(UIImage(named:"check_off"), for: .normal)
            StatusChck = "OFF"
            
        }
    }
    
    @IBAction func accionBack(sender: AnyObject) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        
        
        
        return false
    }
    
    
    //MARK: FUNCIONES==========================================================
    
    func iniValidarLogin(){
        
        usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        ClaveGuardad = usuarioBean?.UsuCla
        UsuarioGuardad = usuarioBean?.UseNroDoc
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endValidarLogin(notification:)), name:NSNotification.Name(rawValue: "endValidarLogin"), object: nil)
        
        let parametros = [
            "Usuario" : usuarioBean!.UseNroDoc! as String,
            "UsuCla" : usuarioBean!.UsuCla! as String,
            "Origen" : "S",
            "Originado" : "IOS"
        ]
        OriginData.sharedInstance.validarLogin(notificacion: "endValidarLogin", parametros: parametros as NSDictionary)
    }
    
    @objc func endValidarLogin(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        usuarioBean = notification.object as? UsuarioBean;
        print("endValidarLogin: \(usuarioBean)")
        
        GlobalVariables.sharedManager.infoUsuarioBean = usuarioBean
        
        let ValorCodigoWS = (usuarioBean!.CodigoWs! as NSString).integerValue
        
        if(ValorCodigoWS == 0 || ValorCodigoWS == 2){
            print("cero")
            //self.showAlert("Atención", mensaje: "cero")
            self .iniObtenerLoginDetalle()
        }else{
            self.obtenerMensajeError(codigo: String(ValorCodigoWS))
            
        }
//        switch ValorCodigoWS {
//            
//            
//        case 0:
//            print("cero")
//            //self.showAlert("Atención", mensaje: "cero")
//            self .iniObtenerLoginDetalle()
//        case 2:
//            self .iniObtenerLoginDetalle()
//        case 1:
//            ValorMensaje = "El usuario no existe o la clave no es correcta."
//            AlertaMensaje(ValorTitle, Mensaje: ValorMensaje, Boton: ValorBoton)
//        case 14:
//            ValorMensaje = "Ya expiró la fecha de activación"
//            AlertaMensaje(ValorTitle, Mensaje: ValorMensaje, Boton: ValorBoton)
//        case 15:
//            ValorMensaje = "Usuario no activo"
//            AlertaMensaje(ValorTitle, Mensaje: ValorMensaje, Boton: ValorBoton)
//        default:
//            ValorMensaje = "Problemas en la conexión, vuelva a intentar"
//            AlertaMensaje(ValorTitle, Mensaje: ValorMensaje, Boton: ValorBoton)
//            //self.obtenerMensajeError(usuarioBean.CodigoWs!)
//        }
//        
//        
//        
        
        
    }
    func iniObtenerLoginDetalle(){
        
        // usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endObtenerLoginDetalle(notification:)), name:NSNotification.Name(rawValue: "endObtenerLoginDetalle"), object: nil)
        
        let parametros = [
            "Usuario" : UsuarioGuardad! as String,
            "SesionId" : usuarioBean!.UseSeIDSession! as String
        ]
        OriginData.sharedInstance.obtenerLoginDetalle(notificacion: "endObtenerLoginDetalle", parametros: parametros as NSDictionary)
    }
    
    @objc func endObtenerLoginDetalle(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let data = notification.object as! UsuarioBean;
        
        GlobalVariables.sharedManager.infoUsuarioBean?.SDT_Usuario = data.SDT_Usuario
        
        GlobalVariables.sharedManager.infoUsuarioBean?.UseNroDoc = data.SDT_Usuario.object(forKey: "UseNroDoc") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseFlgBonif = data.SDT_Usuario.object(forKey: "UseFlgBonif") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseApePat = data.SDT_Usuario.object(forKey: "UseApePat") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseNomCo = data.SDT_Usuario.object(forKey: "UseNomCo") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseClave = data.SDT_Usuario.object(forKey: "UseClave") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseApeMat = data.SDT_Usuario.object(forKey: "UseApeMat") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseMail = data.SDT_Usuario.object(forKey: "UseMail") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseNom = data.SDT_Usuario.object(forKey: "UseNom") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseEst = data.SDT_Usuario.object(forKey: "UseEst") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseTip = data.SDT_Usuario.object(forKey: "UseTip") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseCod = data.SDT_Usuario.object(forKey: "UseCod") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UseKey = data.SDT_Usuario.object(forKey: "UseKey") as! String?
        GlobalVariables.sharedManager.infoUsuarioBean?.UsePerfil = data.SDT_Usuario.object(forKey: "UsePerfil") as! String?
        
        GlobalVariables.sharedManager.infoUsuarioBean?.UsuCla = ClaveGuardad
        
        GlobalVariables.sharedManager.infoUsuarioBean?.UseNroDoc = UsuarioGuardad
        GlobalVariables.sharedManager.infoUsuarioBean?.Usuario = UsuarioGuardad
        
        self.iniSetearToken()
        
    }
    
    func iniSetearToken(){
        
        //usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        let epicToken = MCEpicToken()
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActualizarTerminosCondicionesViewController.endSetearToken(notification:)), name:NSNotification.Name(rawValue: "endSetearToken"), object: nil)
        
        let valorEpicToken = epicToken.getToken()
        
        let parametros = [
            "Usuario" :  UsuarioGuardad as String,
            "IdSesion" : usuarioBean!.UseSeIDSession! as String,
            "UseTokenApp" : valorEpicToken as String
        ]
        
        print("parametros: \(parametros)")
        
        OriginData.sharedInstance.setearToken(notificacion: "endSetearToken", parametros: parametros as NSDictionary)
    }
    
    @objc func endSetearToken(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let data = notification.object as! NotificationPush;
        
        var CodigoWS :String! = ""
        
        if(data.CodigoValidacion == ""){
            
            if(data.Existe == ""){
                if(data.Codigo == ""){
                    
                    
                }else{
                    CodigoWS = data.Codigo
                }
            }else{
                CodigoWS = data.Existe
            }
            
        }else{
            CodigoWS = data.CodigoValidacion
        }
        
        print("el codiho ws es: \(CodigoWS)")
        
        self.loginLoad()
    }
    
    func loginLoad(){
        
        let ValorCodigoWS = (usuarioBean!.CodigoWs! as NSString).integerValue

        if(ValorCodigoWS == 0 || ValorCodigoWS == 2){
            
            if(usuarioBean?.UseFlgClave == "S"){
                if !(usuarioBean?.UseFlgContratoCG == "S"){
                    
                    let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                    let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ActualizarDNIVC") as UIViewController
                    self.present(vc, animated: false, completion: nil)
                    
                }else{
                    
                    self.irHome()
                    
                }
            }else{
                
                GlobalVariables.sharedManager.infoUsuarioBean?.tipoAccesoCambiarContrasenia = "L"
                
                let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaExternoVC") as UIViewController
                self.present(vc, animated: false, completion: nil)
            }
            
            
        }
        
    }
    
    func AlertaMensaje (Title: String, Mensaje: String, Boton: String){
        
        let alertController = UIAlertController(title: Title, message:
            Mensaje, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Boton, style: .default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func irHome(){
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let menuLeftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuLeftVC") as! MenuLeftViewController
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        menuLeftViewController.setDefaultViewController(defaultViewController: homeViewController)
        let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: menuLeftViewController)
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        SlideMenuOptions.contentViewScale = 1
        
        self.present(slideMenuController, animated: false, completion: nil)
    }
}
