//
//  Define.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/23/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

struct PATHS {
    static let webservice = "http://www.sentinelconsultagratuita.com/WSAPPV2/rest/"
    static let consulta = "https://www.sentinelperu.com/qa/logeoMS.aspx"
    static let fotolink = "http://www2.sentinelperu.com/ManagerFileServer/WsFileServer.asmx?wsdl"
    static let newfotolink = "http://www2.sentinelperu.com/ManagerFileServerQA/WsFileServer.asmx?wsdl"
    //static let webservice = "http://www.sentinelconsultagratuita.com/wsapp/rest/"
    //static let consulta = "https://www.sentinelperu.com/cliente/logeoMS.aspx"
    static let newservice = "http://208.43.231.70/vcasawsqa/rest/"
    static let prodservice = "http://208.43.231.70/vcasaws/rest/"
    static let pruebaservice = "http://208.43.231.70/vcasawsqa/rest/"
    
    static let listaserviceqa = "http://208.43.231.70/vcasawsqa/rest/"
    static let listaservice = "http://208.43.231.70/vcasaws/rest/"
}

