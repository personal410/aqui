//
//  BorderLabel.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 19/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class BorderLabel: UILabel {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 6

    }
}
