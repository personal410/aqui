//
//  ParentViewController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/21/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation

enum BgrImage: Int {
    case Green = 0
    case White
}

class ParentViewController: UIViewController, AVAudioPlayerDelegate {
    
    // MARK: - Variables
    
    weak var delegate: LeftMenuProtocol?
    var actualLocation = CLLocation()
    let MENSAJE_OK: String = "OK"
    var audioPlayer = AVAudioPlayer()
    //var player: AVQueuePlayer!
    var isPlayFirstTime = false
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ParentViewController.endInternetConnection(notification:)), name:NSNotification.Name(rawValue: "endInternetConnection"), object: nil)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.pauseAudio()
        self.view.superview?.endEditing(true)
    }

    @objc func endInternetConnection(notification: NSNotification){
        
        self.hideActivityIndicator(uiView: self.view)
        
        self.showAlertConnectivity()
    }
    
    func showAlertConnectivity(){
        
        let alerta = UIAlertController(title: Constants.aplicacionNombre, message: "Verifique su conexión a internet e inténtelo nuevamente.", preferredStyle: .alert)
        alerta.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        self.present(alerta, animated: true, completion: nil)
    }
    
    func mostrarAlertaErrorWS(codigo: String, mensaje: String, menu: LeftMenu = LeftMenu.Ninguna){
        
        let alerta = UIAlertController(title: Constants.aplicacionNombre, message: mensaje, preferredStyle: .alert)
        
        switch codigo {
        case "36", "37", "99":
            
            alerta.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                
                self.delegate?.changeViewController(menu: LeftMenu.CerrarSesion)
            }))
            break
        case "48":
            
            alerta.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                
//                let storyboard = UIStoryboard(name: "Login", bundle: nil)
//                let vc = storyboard.instantiateViewControllerWithIdentifier("LoginVC") as! LoginViewController
//                
//                self.navigationController?.popToViewController(vc, animated: true)
                
                
                //let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                //self.navigationController!.popToViewController(viewControllers[viewControllers.count - 7], animated: true)
                /*
                AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_EMPRESA_TIPO_DOC, value: "")
                AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_EMPRESA_NRO_DOC, value: "")
                AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_CODIGO, value: "")
                AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_EMPRESA_CODIGO, value: "")
                */
                AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
                AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
                AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_CODIGO)
                AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
                
                let empresaTipDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
                let empresaNroDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
                let solicitudCodigo = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_CODIGO)
                let solicitudCodigoEmp = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
                
                print(empresaTipDoc)
                print(empresaNroDoc)
                print(solicitudCodigo)
                print(solicitudCodigoEmp)
                
               // let storyboard = UIStoryboard(name: "Login", bundle: nil)
//                let vc = self.storyboard!.instantiateViewControllerWithIdentifier("ProcesoFinalizadoVC") as! ProcesoFinalizadoViewController
//                vc.indice = 1
//                
//                self.navigationController?.pushViewController(vc, animated: true)
                
                self.delegate?.changeViewController(menu: LeftMenu.Home)
            }))
            break
        default:
            
            alerta.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                
                if (menu != LeftMenu.Ninguna)
                {
                    self.delegate?.changeViewController(menu: LeftMenu.Home)
                }
            }))
            break
        }
        
        self.present(alerta, animated: true, completion: nil)
    }
    
    func iniInsertarEventoFueraPerimetro(){
        
        let solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        let usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        if (solicitudBean != nil)
        {
            self.showActivityIndicator(uiView: self.view)
            
            NotificationCenter.default.addObserver(self, selector: #selector(ParentViewController.endInsertarEventoFueraPerimetro(notification:)), name:NSNotification.Name(rawValue: "endInsertarEventoFueraPerimetro"), object: nil)

            let parametros = [
                "usuario"   :  usuarioBean!.UseNroDoc! as String,
                "sesionId"  :  usuarioBean!.UseSeIDSession! as String,
                "empresaTipDoc" :  solicitudBean!.empresaTipDoc! as String,
                "empresaNroDoc" :  solicitudBean!.empresaNroDoc! as String,
                "solicitudCodigo"   :  solicitudBean!.solicitud! as String,
                "solicitudCodigoEmp"  :  solicitudBean!.solicitudEmpresa! as String,
                "tipo" :  "FUEPER",
                "latitud" : String(format: "%.14f", self.actualLocation.coordinate.latitude),
                "longitud" :  String(format: "%.14f", self.actualLocation.coordinate.longitude),
                "plataforma" :  "IOS"
            ]
            
            OriginData.sharedInstance.insertarEvento(notificacion: "endInsertarEventoFueraPerimetro", parametros: parametros as NSDictionary)
        }
        else
        {
            self.delegate?.changeViewController(menu: LeftMenu.CerrarSesion)
        }
    }
    
    @objc func endInsertarEventoFueraPerimetro(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as? ResultadoBean
        
        if(data!.error.count == 0)
        {
            self.delegate?.changeViewController(menu: LeftMenu.CerrarSesion)
        }
        else
        {
            self.delegate?.changeViewController(menu: LeftMenu.CerrarSesion)
        }
    }
    
    func playAudio(nombreAudio: String)
    {
        self.isPlayFirstTime = true
        
        let path = Bundle.main.url(forResource: nombreAudio, withExtension: "mp3")

        do{
            self.audioPlayer = try AVAudioPlayer(contentsOf: path!)
            self.audioPlayer.delegate = self
            self.audioPlayer.play()
        }
        catch{
        }
    }
    
    func pauseAudio()
    {
        if (self.isPlayFirstTime)
        {
            if (self.audioPlayer.isPlaying)
            {
                self.audioPlayer.pause()
                //self.audioPlayer.currentTime = 0
            }
        }
    }
    
    func stopAudio()
    {
        if (self.isPlayFirstTime)
        {
            if (self.audioPlayer.isPlaying)
            {
                self.audioPlayer.stop()
                self.audioPlayer.currentTime = 0
            }
        }
    }
    
    func resumeAudio(tiempo : Double)
    {
        if (self.isPlayFirstTime)
        {
            if (self.audioPlayer.isPlaying)
            {
                self.audioPlayer.play(atTime: tiempo)
                
            }
        }
    }
}
