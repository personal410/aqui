//
//  InicioVerificacionViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 25/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import GoogleMaps
import AVFoundation

class InicioVerificacionViewController: ParentViewController, CLLocationManagerDelegate  {

    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    var usuarioBean: UsuarioBean!
    var solicitudBean: SolicitudVerificacionBean!

    let locationManager = CLLocationManager()
    //var actualLocation = CLLocation()
    var marcadorLocation = CLLocation()
    
    var alertaFueraPerimetro = UIAlertController()
    let mensajeFueraPerimetro = "Usted no se encuentra dentro del rango de verificación de su domicilio, para iniciar usted deberá retornar a su domicilio en verificación."
    
    @IBOutlet weak var alertaTextField: UILabel!
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.inicializar()
        
    }

    override func viewDidAppear(_ animated: Bool)
    {
        self.setearDatos()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.locationManager.stopUpdatingLocation()
        
        self.stopAudio()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inicializar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func inicializar()
    {
//        let image1:UIImage = UIImage(named: "ic_audio1")!
//        let image2:UIImage = UIImage(named: "ic_audio2")!
//        let image3:UIImage = UIImage(named: "ic_audio3")!
//        self.parlanteBT.setImage(image1, forState: .normal)
//        self.parlanteBT.imageView!.animationImages = [image1, image2, image3]
//        self.parlanteBT.imageView!.animationDuration = 1.5
//        self.parlanteBT.imageView!.startAnimating()

        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        //---
        self.alertaFueraPerimetro = UIAlertController(title: Constants.aplicacionNombre, message: self.mensajeFueraPerimetro, preferredStyle: .alert)
        self.alertaFueraPerimetro.addAction(UIAlertAction(title: "Reintentar", style: .default, handler: { (action: UIAlertAction!) in

            
        }))
        self.alertaFueraPerimetro.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
            
            self.delegate?.changeViewController(menu: LeftMenu.Home)
        }))
        //---
    }
    
    func setearDatos()
    {
        self.validarLocalizacion()
        
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        self.atributoTextField()
        
        if (self.solicitudBean.usuario.direccionUsuarioLat == "" && self.solicitudBean.usuario.direccionUsuarioLon == "")
        {
            delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
        }
        else
        {
            let selectLatitud = Double(self.solicitudBean.usuario.direccionUsuarioLat!)!
            let selectLongitud = Double(self.solicitudBean.usuario.direccionUsuarioLon!)!
            
            self.marcadorLocation = CLLocation(latitude: selectLatitud, longitude: selectLongitud)
        }
        
        self.playAudio(nombreAudio: nombreAudio.A9)
    }
    
    func atributoTextField(){
        
        var myString = ""
        var esUno = true
        var cantidadHoras = 0
        var cantidadMinutosTotal = 0
        var cantidadMinutos = 0
        
        cantidadMinutosTotal = Int(self.solicitudBean.cantidadHoras!)!
        cantidadHoras = Int(cantidadMinutosTotal / 60)
        cantidadMinutos = Int(cantidadMinutosTotal % 60)
        
        if cantidadHoras == 0 && cantidadMinutos != 0{
            myString = "Además, recibirá 3 validaciones en el transcurso de \(cantidadMinutos) minutos las cuales debe confirmar para completar su proceso de auto-verificación."
            esUno = false

        }
        else
        {
            if cantidadMinutos == 0 {
                if cantidadHoras == 1 {
                    myString = "Además, recibirá 3 validaciones en el transcurso de \(cantidadHoras) hora las cuales debe confirmar para completar su proceso de auto-verificación."
                    esUno = true
                }
                else
                {
                    myString = "Además, recibirá 3 validaciones en el transcurso de \(cantidadHoras) horas las cuales debe confirmar para completar su proceso de auto-verificación."
                    esUno = false
                }
            }
            else
            {
                if cantidadHoras == 1 {
                    myString = "Además, recibirá 3 validaciones en el transcurso de \(cantidadHoras) hora y \(cantidadMinutos) minutos las cuales debe confirmar para completar su proceso de auto-verificación."
                    esUno = true
                }
                else
                {
                    myString = "Además, recibirá 3 validaciones en el transcurso de \(cantidadHoras) horas y \(cantidadMinutos) minutos los cuales debe confirmar para completar su proceso de auto-verificación."
                    esUno = false
                }
            }
        }
        
        
//        if (self.solicitudBean.cantidadHoras == "1")
//        {
//            myString = "Además, recibirá 3 validaciones en el transcurso de \(cantidadHoras) hora y \(cantidadMinutos) minutos los cuales debe confirmar para completar su proceso de auto-verificación."
//            
//            esUno = true
//        }
//        else
//        {
//            myString = "Además, recibirá 3 validaciones en el transcurso de \(self.solicitudBean.cantidadHoras!) horas y \(cantidadMinutos) minutos los cuales debe confirmar para completar su proceso de auto-verificación."
//            
//            esUno = false
//        }
        
        var myMutableString = NSMutableAttributedString()

        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "HelveticaNeue-Medium", size: 13.0)!])
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.fromHex(rgbValue: 0x005BA5).cgColor, range: NSRange(location: 18,length: 2))
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 13), range: NSRange(location: 18,length: 2))

        
        if (esUno)
        {
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.fromHex(rgbValue: 0x005BA5).cgColor, range: NSRange(location: 54,length: self.solicitudBean.cantidadHoras!.count + 5))
            myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 13), range: NSRange(location: 54,length: self.solicitudBean.cantidadHoras!.count + 5))
        }
        else
        {
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.fromHex(rgbValue: 0x005BA5).cgColor, range: NSRange(location: 54,length: self.solicitudBean.cantidadHoras!.count + 6))
            myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 13), range: NSRange(location: 54,length: self.solicitudBean.cantidadHoras!.count + 6))
        }
        
        // set label Attribute
        self.alertaTextField.attributedText = myMutableString
    }
    
    func validarLocalizacion() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.denied)
            {
                let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado para Aquí, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
                
                alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                    
                    UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!);
                    })
                alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                
                self.present(alertaLocalizacion, animated: true, completion: nil)
                
                return false
            }
            else if (status == CLAuthorizationStatus.notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == CLAuthorizationStatus.authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
            
            alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                
                UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
                })
            alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alertaLocalizacion, animated: true, completion: nil)
            
            return false
        }
    }
    
    func validarPerimetro()
    {
        if (self.validarLocalizacion())
        {
            let distanceInMeters = self.actualLocation.distance(from: self.marcadorLocation)
            
            print("Distancia entre puntos: \(distanceInMeters) m.")
            
            // Si esta dentro de radio
            let radioControl = Double(self.solicitudBean.configuracion.radioControl!)!
            if (distanceInMeters <= radioControl)
            {
                self.iniValidarVigencia()
            }
            else
            {
                self.present(self.alertaFueraPerimetro, animated: true, completion: nil)
            }
        }
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func accionAceptar(sender: AnyObject) {
        
        self.validarPerimetro()
        //delegate?.changeViewController(LeftMenu.alertas)
    }

    // MARK: - WebServices Methods
    
    func iniValidarVigencia(){
        
        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(InicioVerificacionViewController.endValidarVigencia(notification:)), name:NSNotification.Name(rawValue: "endValidarVigencia"), object: nil)
        
        let parametros = [
            "usuario"   :  usuarioBean!.UseNroDoc! as String,
            "sesionId"  :  usuarioBean!.UseSeIDSession! as String,
            "empresaTipDoc" :  solicitudBean.empresaTipDoc! as String,
            "empresaNroDoc" :  solicitudBean.empresaNroDoc! as String,
            "solicitudCodigo"   :  solicitudBean.solicitud! as String,
            "solicitudCodigoEmp"   :  solicitudBean.solicitudEmpresa! as String
        ]
        
        OriginData.sharedInstance.validarVigenciaSolicitud(notificacion: "endValidarVigencia", parametros: parametros as NSDictionary)
    }
    
    @objc func endValidarVigencia(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as! ResultadoBean?;
        
        if (data!.result == self.MENSAJE_OK && data!.error.count == 0)
        {
            self.iniInsertarEvento()
        }
        else
        {
            let errorBean: ErrorBean = data!.error[0] as! ErrorBean
            
            self.mostrarAlertaErrorWS(codigo: errorBean.code, mensaje: errorBean.message)
        }
    }

    func iniInsertarEvento(){
        
        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(InicioVerificacionViewController.endInsertarEvento(notification:)), name:NSNotification.Name(rawValue: "endInsertarEvento"), object: nil)
        
        let parametros = [
            "usuario"   :  usuarioBean!.UseNroDoc! as String,
            "sesionId"  :  usuarioBean!.UseSeIDSession! as String,
            "empresaTipDoc" :  self.solicitudBean.empresaTipDoc! as String,
            "empresaNroDoc" :  self.solicitudBean.empresaNroDoc! as String,
            "solicitudCodigo"   :  self.solicitudBean.solicitud! as String,
            "solicitudCodigoEmp"   :  self.solicitudBean.solicitudEmpresa! as String,
            "tipo" :  "INIVER",
            "latitud" : String(format: "%.14f", self.actualLocation.coordinate.latitude),
            "longitud" :  String(format: "%.14f", self.actualLocation.coordinate.longitude),
            "plataforma" :  "IOS"
        ]
        
        OriginData.sharedInstance.insertarEvento(notificacion: "endInsertarEvento", parametros: parametros as NSDictionary)
    }
    
    @objc func endInsertarEvento(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object! as! ResultadoBean
        
        if (data.error.count == 0)
        {
            Variables.esLog = false
            
            AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC, value: self.solicitudBean.empresaTipDoc!)
            AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC, value: self.solicitudBean.empresaNroDoc!)
            AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_CODIGO, value: self.solicitudBean.solicitud!)
            AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO, value: self.solicitudBean.solicitudEmpresa!)
            
            delegate?.changeViewController(menu: LeftMenu.Alertas)
            /*
            let nav = self.storyboard?.instantiateViewControllerWithIdentifier("AlertaVC") as! AlertaViewController
            
            self.navigationController?.pushViewController(nav, animated: true)
             */
        }
        else
        {
            let errorBean: ErrorBean = data.error[0] as! ErrorBean
            
            self.mostrarAlertaErrorWS(codigo: errorBean.code, mensaje: errorBean.message)
        }
    }

    @IBAction func BACKbt(sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - CLLocationManagerDelegate Methods
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if (status == CLAuthorizationStatus.authorizedAlways)
        {
            print("GPS habilitado siempre.")
        }
        else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            print("GPS habilitado cuando esté en uso.")
        }
        else if (status == CLAuthorizationStatus.denied)
        {
            print("GPS denegado.")
            //UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let longitud = location!.coordinate.longitude
        let latitud = location!.coordinate.latitude
        
        print("InicioVerificacion lon: \(longitud) y lat: \(latitud)")
        
        self.actualLocation = location!
        
    }
    
    @IBAction func parlanteAction(sender: UIButton) {
        
        if (self.audioPlayer.isPlaying == false)
        {
            self.audioPlayer.play()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
        }
    }


}
