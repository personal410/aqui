//
//  WSLogin.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/23/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class WSLogin: WSParent {

    static let sharedInstance = WSLogin()
    
    static func validarLogin(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "iniciarSesion", clase: UsuarioBean.self)
    }
    
    static func obtenerLoginDetalle(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "obtenerUsuario", clase: UsuarioBean.self)
    }
    static func solicitarCambiarClave(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "nuevaClave", clase: UsuarioBean.self)
    }
    static func cambiarClave(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "cambiarClave", clase: UsuarioBean.self)
    }
    static func consultarServicio(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ServUsuario", clase: UsuarioBean.self)
    }
    static func consultaDatosServicio(notificacion: String, parametros: NSDictionary){
    
    self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWSDatosServicio", clase: DatoServicioBean.self)
    }
    static func setearToken(notificacion: String, parametros: NSDictionary){
        
        //self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ActTokenUsu", clase: NotificationPush.self)
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ActTokenUsuAPP", clase: NotificationPush.self)
    }
    
    // Ultimos web services
    
    static func actualizarToken(notificacion: String, parametros: NSDictionary){
        
        //self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ActTokenUsu", clase: NotificationPush.self)
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "actualizarTokenApp", clase: NotificationPush.self)
    }
    
    static func setToken(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_RegistraToken", clase: NotificationPush.self)
    }
    
    static func cargarMisServicios(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_CargaCombo", clase: NotificationPush.self)
    }

    static func validarDNI(notificacion: String, parametros: NSDictionary){
    
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarDNI", clase: UsuarioBean.self)
    }
    static func validarMail(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarCorreo", clase: UsuarioBean.self)
    }

    static func validarCelular(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarCelular", clase: UsuarioBean.self)
    }
    static func generarClaveSMS(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "generarClaveSMS", clase: UsuarioBean.self)
    }

    static func generarPassword(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "generarClave", clase: UsuarioBean.self)
    }
    static func validarCodigo(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarEnvioSMS", clase: UsuarioBean.self)
    }

    static func generarAFPInsInvA(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "registrarConsultaGratuita", clase: UsuarioBean.self)
    }

    static func AceptaContratoUsu(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "registroContrato", clase: UsuarioBean.self)
    }
    static func aceptarContrato(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "aceptarContrato2", clase: UsuarioBean.self)
    }

    static func AFPActRegYa(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "aceptarContrato", clase: UsuarioBean.self)
    }

    static func obtenerMensaje(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ObtErrorWS", clase: ErrorBean.self)
    }
    //===========
    static func listarSolicitudVerificacion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "obtenerListaSolicitud", clase: SolicitudVerificacionBean.self)
    }

    static func obtenerSolicitudVerificacion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "obtenerSolicitud", clase: SolicitudVerificacionBean.self)
    }
    
    static func ingresarDireccion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "insertarDireccion", clase: ResultadoBean.self)
    }
    static func validarDireccion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarDirecciones", clase: ResultadoBean.self)
    }
    
    static func insertarEvento(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "insertarEvento", clase: ResultadoBean.self)
    }
    
    static func validarVigenciaSolicitud(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarVigenciaSolicitud", clase: ResultadoBean.self)
    }
    
    static func cargaListaWS(notification: String, parametros: NSDictionary){
        self.prepareSessionDataTask(notificacion: notification, parametros: parametros, metodo: "RWS_VD_listaWebService", clase: ListaInicialBean.self)
    }
  
}
