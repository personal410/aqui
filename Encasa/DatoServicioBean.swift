//
//  DatoServicioBean.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 3/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class DatoServicioBean: NSObject {
    

    var codigoWs  : String? = ""
    
    var SDTDatosservicio : NSDictionary = NSDictionary()
    var AFSerCod  : String? = ""
    var AFSerTip  : String? = ""
    var AFSerSit  : String? = ""
    var AFSerCPTReg  : String? = ""
    var AFSerCPTTot  : String? = ""
    var AFSerCPTDis  : String? = ""
    var AFSerFecFinServ  : String? = ""
    var AFSerGraTCon  : String? = ""
    var AFSerGraCon  : String? = ""
    var AFSerGraDis  : String? = ""
    var AFSerGraFchfin  : String? = ""
    var AFSerEst  : String? = ""
    var AFSerConTer  : Int32? = 0
    var AFSerConTerPer  : Int32? = 0
    
    var AFSerCPTNDisp  : String? = ""
}
