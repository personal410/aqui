//
//  DiaCollectionViewCell.swift
//  Tuit-Inves
//
//  Created by Juan Alberto Carlos Vera on 5/4/16.
//  Copyright © 2016 Orbita. All rights reserved.
//

import UIKit

class JCVDayDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var dayLB: UILabel!
    
    @IBOutlet var detailLB: UILabel!
    
    @IBOutlet var valueLB: UILabel!
    
}
