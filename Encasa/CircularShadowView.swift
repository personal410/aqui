//
//  CircularShadowView.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 19/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class CircularShadowView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = layer.bounds.height/2
        layer.shadowRadius = 1
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.gray.cgColor
    }

}
