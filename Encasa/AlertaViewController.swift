//
//  AlertaViewController.swift
//  Encasa
//
//  Created by Rommy Fuentes on 25/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import GoogleMaps
import AVFoundation
import AudioToolbox
import AVFoundation

class AlertaViewController: ParentViewController, CLLocationManagerDelegate  {

    // MARK: - Variables
    
    //weak var delegate: LeftMenuProtocol?
    
    @IBOutlet weak var esperadoIV: UIImageView!
    @IBOutlet weak var btnCampana1: BorderButton!
    @IBOutlet weak var btnCampana2: BorderButton!
    @IBOutlet weak var btnCampana3: BorderButton!
    @IBOutlet weak var imgCampana1: UIImageView!
    @IBOutlet weak var imgCampana2: UIImageView!
    @IBOutlet weak var imgCampana3: UIImageView!
    @IBOutlet weak var txtCampana: UILabel!
    @IBOutlet weak var contadorVI: UIView!
    @IBOutlet weak var contadorLB: UILabel!
    @IBOutlet weak var cargandoVI: UIView!
    @IBOutlet weak var cargandoIV: UIImageView!
    @IBOutlet weak var audioIV: UIImageView!
    @IBOutlet weak var parlanteBT: UIButton!
    
    var usuarioBean: UsuarioBean!
    var solicitudBean: SolicitudVerificacionBean!
    
    var campana1 : String = "gris"
    var campana2 : String = "gris"
    var campana3 : String = "gris"
    var alertaActiva: Int = 0
    
    var valorEventoActual = ""
    
    let locationManager = CLLocationManager()
    //var actualLocation = CLLocation()
    var marcadorLocation = CLLocation()
    
    var updateTimerPerimetro = Timer()
    var updateTimerAlertas = Timer()
    var updateTimerTiempoEspera = Timer()
    var updateTimerEnProceso = Timer()
    
    var backgroundTaskEnProceso = BackgroundTask()
    var backgroundTaskAlertas = BackgroundTask()
    var backgroundTaskPerimetro = BackgroundTask()
    var backgroundTaskTiempoEspera = BackgroundTask()
    
    var intervaloPerimetroSegundos: Double = 0
    var intervaloAlertaSegundos: Double = 0
    var intervaloTiempoEsperaSegundos: Double = 0
    var intervaloEnProceso: Double = 0

    var alertaFueraPerimetro = UIAlertController()
    var alertaFueraTiempo = UIAlertController()
    var alertaGPSDisp = UIAlertController()
    var alertaGPSApp = UIAlertController()
    var alertaGPSDetener = UIAlertController()
    var alertaFueraPerimetroSinInternet = UIAlertController()
    var contadorGPS: Int = 0
    
    let mensajeAlerta1 = "Ha recibido la primera de sus 3 validaciones, presione en la campana para continuar."
    let mensajeAlerta2 = "Ha recibido la segunda de sus 3 validaciones, presione en la campana para continuar."
    let mensajeAlerta3 = "Ha recibido su última validación, presione en la campana para continuar."
    let mensajeFueraPerimetro = "Usted no se encuentra dentro del rango de verificación de su domicilio, para continuar usted deberá retornar a su domicilio en verificación."
    let mensajeFueraTiempo = "Usted no ha marcado su validación, presione en la campana para continuar."
    var mensajeGPS = "Para continuar con la verificación usted debe activar su GPS."
    let mensajeGPSDetener = "Se detuvo su verificación, active su GPS y vuelva a iniciar."
    let mensajeFueraPerimetroSinInternet = "Usted no se encuentra dentro del rango de verificación de su domicilio, para continuar deberá retornar a su domicilio en verificación y verificar su conexión a internet."
    
    var contadorBucleFueraPerimetro: Int = 0
    var contadorFueraPerimetro: Int = 0
    var contadorEnProceso: Double = 0.0
    
    var esSolicitudTerminada: Bool = false
    
    // MARK: - ViewController Cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.inicializar()
    }
    /*
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    */
    override func viewDidAppear(_ animated: Bool)
    {
        TextFileHelper.writeToLogFile(value: "func viewDidAppear")
        
        self.mostrarCargando(mostrar: false)
        
        if (Variables.esLog == false)
        {
            //self.setearDatos()
            
            self.iniObtenerSolicitudVerificacion()
        }
        else
        {
            self.startSpinning()
        }
        
        Variables.esLog = false
    }
    
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        
        //UIApplication.shared.cancelAllLocalNotifications()
        //UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.startSpinning()
    }
    
    @IBAction func accionDemo(sender: AnyObject) {
        
        self.delegate?.changeViewController(menu: LeftMenu.Home)
        
        TextFileHelper.writeToLogFile(value: "func accionDemo")
    }
    override func viewDidDisappear(_ animated: Bool) {

        TextFileHelper.writeToLogFile(value: "func viewDidDisappear")
        
        if (Variables.esLog == false)
        {
            self.callBackgroundTaskEnProceso(activar: false)
            self.callBackgroundTaskPerimetro(activar: false)
            self.callBackgroundTaskAlertas(activar: false)
            self.callBackgroundTaskTiempoEspera(activar: false)
            
            self.locationManager.stopUpdatingLocation()
            
            UIApplication.shared.cancelAllLocalNotifications()
        }
        
        if (!self.esSolicitudTerminada)
        {
            if (self.solicitudBean != nil)
            {
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC, value: self.solicitudBean.empresaTipDoc!)
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC, value: self.solicitudBean.empresaNroDoc!)
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_CODIGO, value: self.solicitudBean.solicitud!)
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO, value: self.solicitudBean.solicitudEmpresa!)
            }
        }
        
        self.stopAudio()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inicializar()
    }
    
    // MARK: - Methods
    
    func inicializar()
    {
        let image1:UIImage = UIImage(named: "ic_pause")!
        self.parlanteBT.setImage(image1, for: .normal)
    
        self.contadorVI.roundCorners(corners: UIRectCorner.allCorners, radius: 8.0)
        self.contadorLB.roundCorners(corners: UIRectCorner.allCorners, radius: 8.0)
        
        self.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        
        self.alertaFueraTiempo = UIAlertController(title: Constants.aplicacionNombre, message: self.mensajeFueraTiempo, preferredStyle: .alert)
        self.alertaFueraTiempo.addAction(UIAlertAction(title: "Continuar", style: .cancel, handler: { (action: UIAlertAction!) in
            
            //self.handleFueraTiempoSi()
        }))
        /*
        self.alertaFueraTiempo.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            
            self.handleFueraTiempoNo()
        }))
        */
        //---
        self.alertaFueraPerimetro = UIAlertController(title: Constants.aplicacionNombre, message: self.mensajeFueraPerimetro, preferredStyle: .alert)
        self.alertaFueraPerimetro.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { (action: UIAlertAction!) in
            
            self.handleFueraPerimetroSi()
        }))
        self.alertaFueraPerimetro.addAction(UIAlertAction(title: "En otro momento", style: .default, handler: { (action: UIAlertAction!) in
            
            self.handleFueraPerimetroNo()
        }))
        //---
        self.alertaGPSDisp = UIAlertController(title: "Configuración de GPS", message: self.mensajeGPS, preferredStyle: .alert)
        self.alertaGPSDisp.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
            
            UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
        })
        self.alertaGPSDisp.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        //---
        self.alertaGPSApp = UIAlertController(title: "Configuración de GPS", message: self.mensajeGPS, preferredStyle: .alert)
        self.alertaGPSApp.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
            
            UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!);
        })
        self.alertaGPSApp.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        //---
        self.alertaGPSDetener = UIAlertController(title: Constants.aplicacionNombre, message: self.mensajeGPSDetener, preferredStyle: .alert)
        self.alertaGPSDetener.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { (action: UIAlertAction!) in
        
            self.delegate?.changeViewController(menu: LeftMenu.Home)
        }))
        //---
        self.alertaFueraPerimetroSinInternet = UIAlertController(title: Constants.aplicacionNombre, message: self.mensajeFueraPerimetroSinInternet, preferredStyle: .alert)
        self.alertaFueraPerimetroSinInternet.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
            
            self.delegate?.changeViewController(menu: LeftMenu.CerrarSesion)
        }))
        //---
        
        self.setupNotificationPerimetro()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AlertaViewController.handleFueraPerimetroSi), name: NSNotification.Name(rawValue: "fueraPerimetroSiNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AlertaViewController.handleFueraPerimetroNo), name: NSNotification.Name(rawValue: "fueraPerimetroNoNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AlertaViewController.applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    func setearDatos()
    {
        UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.solicitudBean = GlobalVariables.sharedManager.infoSolicitudVerificacionBean
        
        self.intervaloAlertaSegundos = Double(self.solicitudBean.cantSegundosAlerta!)!
        self.alertaActiva = Int(self.solicitudBean.nroAlerta!)!
        
        //Borrar

        //self.intervaloAlertaSegundos = Double(self.solicitudBean.configuracion.intervaloAlerta)! * 60
        self.intervaloPerimetroSegundos = Double(self.solicitudBean.configuracion.frecuenciaControl)! * 60
        self.intervaloTiempoEsperaSegundos = Double(self.solicitudBean.configuracion.tiempoEsperaAlerta)! * 60
        
        /*
        self.intervaloPerimetroSegundos = 300
        self.intervaloAlertaSegundos = 30
        self.intervaloTiempoEsperaSegundos = 20
        */
        
        self.intervaloEnProceso = 1
        
        if (self.solicitudBean.usuario.direccionUsuarioLat == "" && self.solicitudBean.usuario.direccionUsuarioLon == "")
        {
            delegate?.changeViewController(menu: LeftMenu.MiDomicilio1)
        }
        else
        {
            let selectLatitud = Double(self.solicitudBean.usuario.direccionUsuarioLat!)!
            let selectLongitud = Double(self.solicitudBean.usuario.direccionUsuarioLon!)!
            
            self.marcadorLocation = CLLocation(latitude: selectLatitud, longitude: selectLongitud)
        }
        
        TextFileHelper.resetLogFile()
        TextFileHelper.writeToLogFile(value: "---------- * ----------")
        TextFileHelper.writeToLogFile(value: "SETTING")
        TextFileHelper.writeToLogFile(value: "codigo de solicitud: \(self.solicitudBean.solicitud!)")
        TextFileHelper.writeToLogFile(value: "intervaloAlertaSegundos: \(intervaloAlertaSegundos) segs.")
        TextFileHelper.writeToLogFile(value: "intervaloPerimetroSegundos: \(intervaloPerimetroSegundos) segs.")
        TextFileHelper.writeToLogFile(value: "intervaloTiempoEsperaSegundos: \(intervaloTiempoEsperaSegundos) segs.")
        TextFileHelper.writeToLogFile(value: "nroAlerta: \(alertaActiva)")
        
        if (self.validarLocalizacion())
        {
            TextFileHelper.writeToLogFile(value: "INICIANDO TAREAS")
            self.reiniciarTareas()
        }
    }
    
    func reiniciarTareas()
    {
        self.startSpinning()
        
        self.contadorLB.text = self.stringFromTimeInterval(interval: self.intervaloAlertaSegundos) as String
        self.contadorEnProceso = 0.0
        self.contadorGPS = 0
        //self.alertaActiva = 1

        self.esSolicitudTerminada = false
        
        switch self.alertaActiva {
        case 0:
            self.alertaActiva = 1
            
            self.campana1 = "gris"
            self.campana2 = "gris"
            self.campana3 = "gris"
            
            self.btnCampana1.backgroundColor = UIColorFromHex(rgbValue: 0xDDDDDD)
            self.imgCampana1.image = UIImage(named:"ic_campana")
            
            self.btnCampana2.backgroundColor = UIColorFromHex(rgbValue: 0xDDDDDD)
            self.imgCampana2.image = UIImage(named:"ic_campana")
            
            self.btnCampana3.backgroundColor = UIColorFromHex(rgbValue: 0xDDDDDD)
            self.imgCampana3.image = UIImage(named:"ic_campana")
            
            self.txtCampana.text = "En breve recibirá la primera validación."
            
            self.callBackgroundTaskEnProceso(activar: true)
            self.callBackgroundTaskPerimetro(activar: true)
            self.callBackgroundTaskAlertas(activar: true)
            
            self.playAudio(nombreAudio: nombreAudio.A10y11)
            
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            break

        case 1:
            self.alertaActiva = 2
            
            self.campana1 = "azul"
            self.campana2 = "gris"
            self.campana3 = "gris"
            
            self.btnCampana1.backgroundColor = UIColorFromHex(rgbValue: 0x0074FF)
            self.imgCampana1.image = UIImage(named:"ic_check_grande")
            
            self.btnCampana2.backgroundColor = UIColorFromHex(rgbValue: 0xDDDDDD)
            self.imgCampana2.image = UIImage(named:"ic_campana")
            
            self.btnCampana3.backgroundColor = UIColorFromHex(rgbValue: 0xDDDDDD)
            self.imgCampana3.image = UIImage(named:"ic_campana")
            
            self.txtCampana.text = "Faltan 2 validaciones"
            
            self.callBackgroundTaskEnProceso(activar: true)
            self.callBackgroundTaskTiempoEspera(activar: false)
            self.callBackgroundTaskAlertas(activar: true)
            
            self.playAudio(nombreAudio: nombreAudio.A12)
            
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            break
            
        case 2:
            self.alertaActiva = 3
            
            self.campana1 = "azul"
            self.campana2 = "azul"
            self.campana3 = "gris"
            
            self.btnCampana1.backgroundColor = UIColorFromHex(rgbValue: 0x0074FF)
            self.imgCampana1.image = UIImage(named:"ic_check_grande")
            
            self.btnCampana2.backgroundColor = UIColorFromHex(rgbValue: 0x0074FF)
            self.imgCampana2.image = UIImage(named:"ic_check_grande")
            
            self.btnCampana3.backgroundColor = UIColorFromHex(rgbValue: 0xDDDDDD)
            self.imgCampana3.image = UIImage(named:"ic_campana")
            
            self.txtCampana.text = "Falta 1 validación"
            
            self.callBackgroundTaskEnProceso(activar: true)
            self.callBackgroundTaskTiempoEspera(activar: false)
            self.callBackgroundTaskAlertas(activar: true)
            
            self.playAudio(nombreAudio: nombreAudio.A14)
            
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            break
        default:
            break
        }

    }
    
    func validarLocalizacion() -> Bool
    {
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.denied)
            {
                let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado para Aquí, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
                
                alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                    
                    UIApplication.shared.openURL(URL(string:UIApplication.openSettingsURLString)!);
                })
                alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel) { value in
                    
                    self.delegate?.changeViewController(menu: LeftMenu.Home)
                })
                
                self.present(alertaLocalizacion, animated: true, completion: nil)
                
                return false
            }
            else if (status == CLAuthorizationStatus.notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == CLAuthorizationStatus.authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            let alertaLocalizacion = UIAlertController(title: "Configuración de GPS", message: "GPS no habilitado, ¿Desea ir al menú de configuración?", preferredStyle: .alert)
            
            alertaLocalizacion.addAction(UIAlertAction(title: "Configurar", style: .default) { value in
                
                UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
                })
            alertaLocalizacion.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alertaLocalizacion, animated: true, completion: nil)
            
            return false
        }
    }

    func validarLocalizacionEnAlertas() -> Bool
    {
        self.alertaGPSApp.dismiss(animated: false, completion: nil)
        self.alertaGPSDisp.dismiss(animated: false, completion: nil)
        
        if CLLocationManager.locationServicesEnabled() {
            
            print("GPS habilitado")
            
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            // For use in background
            self.locationManager.requestAlwaysAuthorization()
            // For use in foreground
            //self.locationManager.requestWhenInUseAuthorization()
            
            let status:CLAuthorizationStatus = CLLocationManager.authorizationStatus()
            
            if (status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.denied)
            {
                if (self.contadorGPS == 0)
                {
                    self.present(self.alertaGPSApp, animated: true, completion: nil)
                }
                
                return false
            }
            else if (status == CLAuthorizationStatus.notDetermined)
            {
                self.locationManager.requestAlwaysAuthorization()
                
                return false
            }
            else if (status == CLAuthorizationStatus.authorizedAlways)
            {
                self.locationManager.startUpdatingLocation()
                
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            print("GPS no habilitado")
            
            if (self.contadorGPS == 0)
            {
                self.present(self.alertaGPSDisp, animated: true, completion: nil)
            }
            
            return false
        }
    }
    
    func startSpinning() {
        self.esperadoIV.image = UIImage(named:"ic_spin")
        self.esperadoIV.startRotating()
        
        self.cargandoIV.startRotating()
    }
    
    func activarAlerta(nroAlerta: Int)
    {
        switch nroAlerta {
        case 0:
            break
            
        case 1:
            
            switch UIApplication.shared.applicationState {
            case .active:
                TextFileHelper.writeToLogFile(value: "activarAlerta is active.")
            case .background:
                TextFileHelper.writeToLogFile(value: "activarAlerta is backgrounded.")
            case .inactive:
                TextFileHelper.writeToLogFile(value: "activarAlerta is inactive.")
                break
            }
            
            self.activarSonidoAlertaActiva()
            self.activarVibracion()
            
            self.btnCampana1.backgroundColor = UIColorFromHex(rgbValue: 0x16A85D)
            self.campana1 = "verde"
            self.txtCampana.text = self.mensajeAlerta1
            self.imgCampana1.image = UIImage(named:"ic_campana")
            
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            self.playAudio(nombreAudio: nombreAudio.A19)
            
            break
            
        case 2:
            
            switch UIApplication.shared.applicationState {
            case .active:
                TextFileHelper.writeToLogFile(value: "activarAlerta is active.")
            case .background:
                TextFileHelper.writeToLogFile(value: "activarAlerta is backgrounded.")
                //self.alertaLocalNotification(self.mensajeAlerta2)
            case .inactive:
                TextFileHelper.writeToLogFile(value: "activarAlerta is inactive.")
                //self.alertaLocalNotification(self.mensajeAlerta2)
                break
            }
            
            self.activarSonidoAlertaActiva()
            self.activarVibracion()
            
            self.btnCampana2.backgroundColor = UIColorFromHex(rgbValue: 0x16A85D)
            self.campana2 = "verde"
            self.txtCampana.text = self.mensajeAlerta2
            self.imgCampana2.image = UIImage(named:"ic_campana")
            
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            self.playAudio(nombreAudio: nombreAudio.A13)
            
            break
            
        case 3:
            
            switch UIApplication.shared.applicationState {
            case .active:
                TextFileHelper.writeToLogFile(value: "activarAlerta is active.")
            case .background:
                TextFileHelper.writeToLogFile(value: "activarAlerta is backgrounded.")
                //self.alertaLocalNotification(self.mensajeAlerta3)
            case .inactive:
                TextFileHelper.writeToLogFile(value: "activarAlerta is inactive.")
                //self.alertaLocalNotification(self.mensajeAlerta3)
                break
            }
            
            self.activarSonidoAlertaActiva()
            self.activarVibracion()
            
            self.btnCampana3.backgroundColor = UIColorFromHex(rgbValue: 0x16A85D)
            self.campana3 = "verde"
            self.txtCampana.text = self.mensajeAlerta3
            self.imgCampana3.image = UIImage(named:"ic_campana")
            
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            self.playAudio(nombreAudio: nombreAudio.A15)
            
            break
        default:
            break
        }
    }
    
    func presionarAlerta(nroAlerta: Int)
    {
        switch nroAlerta {
        case 0:
            break
            
        case 1:
            
            if (self.campana1 == "verde")
            {
                UIApplication.shared.cancelAllLocalNotifications()
                
                self.callBackgroundTaskAlertas(activar: false)
                
                self.valorEventoActual = tipoEvento.ALERT1
                
                self.iniInsertarEvento()
            }
            
            break
            
        case 2:
            
            if (self.campana2 == "verde")
            {
                UIApplication.shared.cancelAllLocalNotifications()
                
                
                self.callBackgroundTaskAlertas(activar: false)
                
                self.valorEventoActual = tipoEvento.ALERT2
                
                self.iniInsertarEvento()
            }
            
            break
            
        case 3:
            
            if (self.campana3 == "verde")
            {
                UIApplication.shared.cancelAllLocalNotifications()
                
                self.callBackgroundTaskAlertas(activar: false)
                
                self.callBackgroundTaskPerimetro(activar: false)
                
                self.callBackgroundTaskTiempoEspera(activar: false)
                
                self.valorEventoActual = tipoEvento.ALERT3
                
                self.iniInsertarEvento()
            }
            
            break
            
        default:
            break
        }
    }
    
    func setupNotificationPerimetro() {
        
        // Specify the notification types.
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.sound]
        
        // Specify the notification actions.
        let fueraPerimetroSiAction = UIMutableUserNotificationAction()
        fueraPerimetroSiAction.identifier = "fueraPerimetroSi"
        fueraPerimetroSiAction.title = "Si"
        fueraPerimetroSiAction.activationMode = UIUserNotificationActivationMode.foreground
        fueraPerimetroSiAction.isDestructive = false
        fueraPerimetroSiAction.isAuthenticationRequired = true
        
        let fueraPerimetroNoAction = UIMutableUserNotificationAction()
        fueraPerimetroNoAction.identifier = "fueraPerimetroNo"
        fueraPerimetroNoAction.title = "No"
        fueraPerimetroNoAction.activationMode = UIUserNotificationActivationMode.background
        fueraPerimetroNoAction.isDestructive = true
        fueraPerimetroNoAction.isAuthenticationRequired = true
        
        let actionsArray = NSArray(objects: fueraPerimetroSiAction, fueraPerimetroNoAction)
        let actionsArrayMinimal = NSArray(objects: fueraPerimetroSiAction, fueraPerimetroNoAction)
        
        // Specify the category related to the above actions.
        let perimetroReminderCategory = UIMutableUserNotificationCategory()
        perimetroReminderCategory.identifier = "perimetroReminderCategory"
        perimetroReminderCategory.setActions(actionsArray as? [UIUserNotificationAction], for: UIUserNotificationActionContext.default)
        perimetroReminderCategory.setActions(actionsArrayMinimal as? [UIUserNotificationAction], for: UIUserNotificationActionContext.minimal)
        
        let alertaReminderCategory = UIMutableUserNotificationCategory()
        alertaReminderCategory.identifier = "alertaReminderCategory"
        
        let categoriesForSettings = NSSet(objects: perimetroReminderCategory, alertaReminderCategory)
        
        // Register the notification settings.
        let newNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: categoriesForSettings as? Set<UIUserNotificationCategory>)
        UIApplication.shared.registerUserNotificationSettings(newNotificationSettings)
        //}
    }
    
    func perimetroLocalNotification() {
        
        let localNotification = UILocalNotification()
        
        localNotification.alertBody = self.mensajeFueraPerimetro
        localNotification.alertAction = "Ver Perímetro"
        localNotification.category = "perimetroReminderCategory"
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
        
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }

    func alertaLocalNotification(mensaje: String) {
        
        let localNotification = UILocalNotification()
        
        localNotification.alertBody = mensaje
        localNotification.alertAction = "Ver Alerta"
        localNotification.category = "alertaReminderCategory"
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
        
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    
    func activarSonidoAlertaActiva()
    {
        let systemSoundID: SystemSoundID = 1304

        AudioServicesPlaySystemSound(systemSoundID)
    }
    
    func activarSonidoAlertaPresionada()
    {
        let systemSoundID: SystemSoundID = 1054
        
        AudioServicesPlaySystemSound(systemSoundID)
    }
    
    func activarVibracion()
    {
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }
    
    // MARK: - Background task perímetro
    
    func callBackgroundTaskPerimetro(activar: Bool)
    {
        TextFileHelper.writeToLogFile(value: "func callBackgroundTaskPerimetro.")
        
        if (activar)
        {
            backgroundTaskPerimetro.startBackgroundTask()
            updateTimerPerimetro = Timer.scheduledTimer(timeInterval: self.intervaloPerimetroSegundos, target: self,
                                                                        selector: #selector(AlertaViewController.validarPerimetro), userInfo: nil, repeats: true)
        }
        else
        {
            updateTimerPerimetro.invalidate()
            backgroundTaskPerimetro.stopBackgroundTask()
        }
    }
    @objc func validarPerimetro()
    {
        TextFileHelper.writeToLogFile(value: "func validarPerimetro.")
        
        switch UIApplication.shared.applicationState {
        case .active:
            TextFileHelper.writeToLogFile(value: "Background task Perimetro is active.")
        case .background:
            TextFileHelper.writeToLogFile(value: "Background task Perimetro is backgrounded.")
        case .inactive:
            TextFileHelper.writeToLogFile(value: "Background task Perimetro is inactive.")
            break
        }

        self.contadorBucleFueraPerimetro = 0
        
        if (self.validarLocalizacionEnAlertas())
        {
            self.subValidarPerimetro(primeraVez: true)
        }
        else
        {
            self.contadorGPS += 1
            
            if (self.contadorGPS == 1)
            {
                switch UIApplication.shared.applicationState {
                case .active:
                    print("GPS activar is active.")
                    
                case .background:
                    print("GPS activar is backgrounded.")
                    self.alertaLocalNotification(mensaje: self.mensajeGPS)
                    
                case .inactive:
                    print("GPS activar is inactive.")
                    self.alertaLocalNotification(mensaje: self.mensajeGPS)
                    break
                }
            }
            else if (self.contadorGPS >= 2)
            {
                self.callBackgroundTaskPerimetro(activar: false)
                self.callBackgroundTaskAlertas(activar: false)
                self.callBackgroundTaskTiempoEspera(activar: false)
                
                switch UIApplication.shared.applicationState {
                case .active:
                    print("GPS activar is active.")
                case .background:
                    print("GPS activar is backgrounded.")
                    self.alertaLocalNotification(mensaje: self.mensajeGPSDetener)
                case .inactive:
                    print("GPS activar is inactive.")
                    self.alertaLocalNotification(mensaje: self.mensajeGPSDetener)
                    break
                }
                
                self.alertaGPSApp.dismiss(animated: false, completion: nil)
                self.alertaGPSDisp.dismiss(animated: false, completion: nil)
                
                self.present(self.alertaGPSDetener, animated: true, completion: nil)
            }
        }
    }
    
    func subValidarPerimetro(primeraVez: Bool)
    {
        self.contadorGPS = 0
        
        let distanceInMeters = self.actualLocation.distance(from: self.marcadorLocation)
        
        TextFileHelper.writeToLogFile(value: "Distancia entre puntos: \(distanceInMeters) m.")
        
        // Si esta dentro de radio
        var radioControl = Double(self.solicitudBean.configuracion.radioControl!)
        radioControl = radioControl! * 2
        
        if (distanceInMeters <= radioControl!)
        {
            TextFileHelper.writeToLogFile(value: "Perimetro: Dentro")
            self.contadorBucleFueraPerimetro = 0
            self.contadorFueraPerimetro = 0
        }
        else
        {
            self.contadorBucleFueraPerimetro += 1
         
            TextFileHelper.writeToLogFile(value: "Perimetro: contadorBucleFueraPerimetro - \(contadorBucleFueraPerimetro)")
            
            self.contadorFueraPerimetro += 1
            
            TextFileHelper.writeToLogFile(value: "Perimetro: contadorFueraPerimetro - \(self.contadorFueraPerimetro)")
            
            if (self.contadorBucleFueraPerimetro >= 100 && self.contadorFueraPerimetro >= 2)
            {
                self.contadorBucleFueraPerimetro = 0
                
                self.contadorFueraPerimetro = 0
                
                self.valorEventoActual = tipoEvento.FUEPER
                
                self.alertaActiva = 0
                
                self.iniInsertarEvento()
            }
            else
            {
                self.subValidarPerimetro(primeraVez: false)
            }
        }
    }
    
    @objc func handleFueraPerimetroSi()
    {
        self.alertaFueraPerimetro.dismiss(animated: false, completion: nil)
        
        self.valorEventoActual = tipoEvento.RFUPER
        
        self.alertaActiva = -1
        
        self.iniInsertarEvento()
    }
    
    @objc func handleFueraPerimetroNo()
    {
        self.alertaFueraPerimetro.dismiss(animated: false, completion: nil)
        
        self.valorEventoActual = tipoEvento.NRFPER
        
        self.alertaActiva = -2
        
        self.iniInsertarEvento()
    }
    /*
    func handleFueraTiempoSi()
    {
        self.alertaFueraTiempo.dismiss(animated: false, completion: nil)
        
        self.delegate?.changeViewController(LeftMenu.alertas)
    }
    
    func handleFueraTiempoNo()
    {
        self.alertaFueraTiempo.dismiss(animated: false, completion: nil)
        
        self.delegate?.changeViewController(LeftMenu.Home)
    }
    */
    
    func handleActualizarCronometro()
    {
        self.contadorLB.text = "00 : 00 : 00"
    }
    
    func mostrarCargando(mostrar: Bool)
    {
        if (mostrar)
        {
            self.startSpinning()
            
            self.contadorVI.isHidden = true
            self.cargandoVI.isHidden = false
        }
        else
        {
            self.contadorVI.isHidden = false
            self.cargandoVI.isHidden = true
        }
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        
        let imagen = UIImage(named: "ic_play")
        self.parlanteBT.setImage(imagen, for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func accionCampanaUno(sender: AnyObject) {
        
        if (self.validarLocalizacionEnAlertas())
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            self.presionarAlerta(nroAlerta: 1)
        }
    }

    @IBAction func accionCampanaDos(sender: AnyObject) {
        
        if (self.validarLocalizacionEnAlertas())
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            self.presionarAlerta(nroAlerta: 2)
        }
    }
    
    @IBAction func accionCampanaTres(sender: AnyObject) {
        
        if (self.validarLocalizacionEnAlertas())
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
            
            self.presionarAlerta(nroAlerta: 3)
        }
    }
    
    func accionContinuar(sender: AnyObject) {
        
        delegate?.changeViewController(menu: LeftMenu.ConfirmarDomicilio)
    }
    
    @IBAction func accionDetener(sender: AnyObject) {
        
        //Borrar
        //self.valorEventoActual = "FUEPER"
        
        //self.alertaActiva = 0
        
        //self.iniInsertarEvento()
    }

    var intentosLog: Int = 0
    @IBAction func accionTitulo(sender: AnyObject) {
        
        self.intentosLog += 1
        
        if (self.intentosLog == 5)
        {
            self.intentosLog = 0
            Variables.esLog = true
            delegate?.changeViewController(menu: LeftMenu.TextLog, backTo: LeftMenu.Alertas)
        }
    }
    
    // MARK: - Background task alertas
    
    func callBackgroundTaskAlertas(activar: Bool)
    {
        TextFileHelper.writeToLogFile(value: "func callBackgroundTaskAlertas.")
        
        if (activar)
        {
            backgroundTaskAlertas.startBackgroundTask()
            updateTimerAlertas = Timer.scheduledTimer(timeInterval: self.intervaloAlertaSegundos, target: self,
                                                                        selector: #selector(AlertaViewController.validarAlertas), userInfo: nil, repeats: true)
            /*
            resetAlertas()
            updateTimerAlertas = NSTimer.scheduledTimerWithTimeInterval(self.intervaloAlertaSegundos, target: self,
                                                                        selector: #selector(AlertaViewController.validarAlertas), userInfo: nil, repeats: true)
            registerBackgroundTaskAlertas()
            */
        }
        else
        {
            updateTimerAlertas.invalidate()
            backgroundTaskAlertas.stopBackgroundTask()
            
            /*
            updateTimerAlertas?.invalidate()
            updateTimerAlertas = nil
            
            if backgroundTaskAlertas != UIBackgroundTaskInvalid
            {
                endBackgroundTaskAlertas()
            }
            */
        }
    }
    /*
    func reinstateBackgroundTaskAlertas()
    {
        if updateTimerAlertas != nil && (backgroundTaskAlertas == UIBackgroundTaskInvalid)
        {
            registerBackgroundTaskAlertas()
        }
    }
    
    func resetAlertas() {
        
    }
    
    func registerBackgroundTaskAlertas()
    {
        backgroundTaskAlertas = UIApplication.shared.beginBackgroundTaskWithExpirationHandler {
            [unowned self] in
            self.endBackgroundTaskAlertas()
        }
        assert(backgroundTaskAlertas != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTaskAlertas()
    {
        //if (self.esValidarAlertas)
        //{
            TextFileHelper.writeToLogFile("func endBackgroundTaskAlertas.")
            
            UIApplication.shared.endBackgroundTask(backgroundTaskAlertas)
            backgroundTaskAlertas = UIBackgroundTaskInvalid
        //}
    }
    */
    @objc func validarAlertas()
    {
        TextFileHelper.writeToLogFile(value: "func validarAlertas.")
        
        //self.esValidarAlertas = true
        
        switch UIApplication.shared.applicationState {
        case .active:
            TextFileHelper.writeToLogFile(value: "Background task Alertas is active.")
        case .background:
            TextFileHelper.writeToLogFile(value: "Background task Alertas is backgrounded.")
        case .inactive:
            TextFileHelper.writeToLogFile(value: "Background task Alertas is inactive.")
            break
        }
        
        self.contadorLB.text = "00 : 00 : 00"
        
        switch self.alertaActiva {
        case 0:
            break
            
        case 1:
            
            self.activarAlerta(nroAlerta: 1)
            
            self.callBackgroundTaskEnProceso(activar: false)
            self.callBackgroundTaskAlertas(activar: false)
            self.callBackgroundTaskTiempoEspera(activar: true)
            
            break
            
        case 2:
            
            self.activarAlerta(nroAlerta: 2)
            
            self.callBackgroundTaskEnProceso(activar: false)
            self.callBackgroundTaskAlertas(activar: false)
            self.callBackgroundTaskTiempoEspera(activar: true)
            
            break
            
        case 3:
            
            self.activarAlerta(nroAlerta: 3)
            
            self.callBackgroundTaskEnProceso(activar: false)
            self.callBackgroundTaskAlertas(activar: false)
            self.callBackgroundTaskTiempoEspera(activar: true)
            
            break
            
        default:
            break
        }
        
        //self.esValidarAlertas = false
    }
    
    // MARK: - Background task alertas
    
    func callBackgroundTaskTiempoEspera(activar: Bool)
    {
        TextFileHelper.writeToLogFile(value: "func callBackgroundTaskTiempoEspera.")
        
        if (activar)
        {
            backgroundTaskTiempoEspera.startBackgroundTask()
            updateTimerTiempoEspera = Timer.scheduledTimer(timeInterval: self.intervaloTiempoEsperaSegundos, target: self,
                                                                        selector: #selector(AlertaViewController.validarTiempoEspera), userInfo: nil, repeats: true)
        }
        else
        {
            updateTimerTiempoEspera.invalidate()
            backgroundTaskTiempoEspera.stopBackgroundTask()
        }
    }
    /*
    func reinstateBackgroundTaskTiempoEspera()
    {
        if updateTimerTiempoEspera != nil && (backgroundTaskTiempoEspera == UIBackgroundTaskInvalid)
        {
            registerBackgroundTaskTiempoEspera()
        }
    }
    
    func resetTiempoEspera() {
        
    }
    
    func registerBackgroundTaskTiempoEspera()
    {
        backgroundTaskTiempoEspera = UIApplication.shared.beginBackgroundTaskWithExpirationHandler {
            [unowned self] in
            self.endBackgroundTaskTiempoEspera()
        }
        assert(backgroundTaskTiempoEspera != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTaskTiempoEspera()
    {
        //if (self.esValidarTiempoEspera)
        //{
            TextFileHelper.writeToLogFile("func endBackgroundTaskTiempoEspera.")
            UIApplication.shared.endBackgroundTask(backgroundTaskTiempoEspera)
            backgroundTaskTiempoEspera = UIBackgroundTaskInvalid
        //}
    }
    */
    @objc func validarTiempoEspera()
    {
        TextFileHelper.writeToLogFile(value: "func validarTiempoEspera.")
        
        //self.esValidarTiempoEspera = true
         
        switch UIApplication.shared.applicationState {
        case .active:
            TextFileHelper.writeToLogFile(value: "Background task Tiempo Espera is active.")
        case .background:
            TextFileHelper.writeToLogFile(value: "Background task Tiempo Espera is backgrounded.")
        case .inactive:
            TextFileHelper.writeToLogFile(value: "Background task Tiempo Espera is inactive.")
            break
        }
        
        switch self.alertaActiva {
        case 0:
            break
            
        case 1:
            
            self.callBackgroundTaskEnProceso(activar: false)
            self.callBackgroundTaskAlertas(activar: false)
            self.callBackgroundTaskTiempoEspera(activar: false)
            
            switch UIApplication.shared.applicationState {
            case .active:
                self.present(self.alertaFueraTiempo, animated: true, completion: nil)
                
            case .background:
                self.alertaLocalNotification(mensaje: self.mensajeFueraTiempo)
                
            case .inactive:
                self.alertaLocalNotification(mensaje: self.mensajeFueraTiempo)
                
                
                break
            }
            //delegate?.changeViewController(LeftMenu.Home)
            
            break
            
        case 2:
            
            self.callBackgroundTaskEnProceso(activar: false)
            self.callBackgroundTaskAlertas(activar: false)
            self.callBackgroundTaskTiempoEspera(activar: false)
            
            switch UIApplication.shared.applicationState {
            case .active:
                self.present(self.alertaFueraTiempo, animated: true, completion: nil)
                
            case .background:
                self.alertaLocalNotification(mensaje: self.mensajeFueraTiempo)
                
            case .inactive:
                self.alertaLocalNotification(mensaje: self.mensajeFueraTiempo)
                
                break
            }
            //delegate?.changeViewController(LeftMenu.Home)
            
            break
            
        case 3:
            
            self.callBackgroundTaskEnProceso(activar: false)
            self.callBackgroundTaskAlertas(activar: false)
            self.callBackgroundTaskTiempoEspera(activar: false)
            
            switch UIApplication.shared.applicationState {
            case .active:
                self.present(self.alertaFueraTiempo, animated: true, completion: nil)
                
            case .background:
                self.alertaLocalNotification(mensaje: self.mensajeFueraTiempo)
                
            case .inactive:
                self.alertaLocalNotification(mensaje: self.mensajeFueraTiempo)
                
                break
            }
            //delegate?.changeViewController(LeftMenu.Home)
            break
            
        default:
            break
        }
        
        //self.esValidarTiempoEspera = false
    }
    
    func callBackgroundTaskEnProceso(activar: Bool)
    {
        TextFileHelper.writeToLogFile(value: "func callBackgroundTaskEnProceso.")
        
        //self.esEnProceso = true
        
        if (activar)
        {
            resetEnProceso()
            
            backgroundTaskEnProceso.startBackgroundTask()
            
            updateTimerEnProceso = Timer.scheduledTimer(timeInterval: self.intervaloEnProceso, target: self,
                                                                          selector: #selector(AlertaViewController.validarEnProceso), userInfo: nil, repeats: true)
        }
        else
        {
            updateTimerEnProceso.invalidate()
            backgroundTaskEnProceso.stopBackgroundTask()
        }
        
        //self.esEnProceso = false
    }
    
    func resetEnProceso() {
        
        self.contadorEnProceso = 0
        self.contadorLB.text = self.stringFromTimeInterval(interval: self.intervaloAlertaSegundos) as String
    }
    
    @objc func validarEnProceso()
    {
        self.contadorEnProceso += 1
        
        var diferenciaSegs = self.intervaloAlertaSegundos - self.contadorEnProceso
        
        if (diferenciaSegs < 0)
        {
            diferenciaSegs = 0
        }
        
        let tiempo = self.stringFromTimeInterval(interval: diferenciaSegs) as String
        
        self.contadorLB.text = tiempo
    }
    
    func stringFromTimeInterval(interval:TimeInterval) -> String {
        
        let ti = NSInteger(interval)

        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return NSString(format: "%0.2d : %0.2d : %0.2d",hours,minutes,seconds) as String
    }
    
    // MARK: - WebServices Methods
    
    func iniObtenerSolicitudVerificacion(){
        
        self.mostrarCargando(mostrar: true)
        
        let empresaTipDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
        let empresaNroDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
        let solicitudCodigo = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_CODIGO)
        let solicitudCodigoEmp = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AlertaViewController.endObtenerSolicitudVerificacion(notification:)), name:NSNotification.Name(rawValue: "endObtenerSolicitudVerificacion"), object: nil)
        
        let parametros = [
            "usuario" : usuarioBean!.UseNroDoc! as String,
            "sesionId" : usuarioBean!.UseSeIDSession! as String,
            "empresaTipDoc" : empresaTipDoc as String,
            "empresaNroDoc" : empresaNroDoc as String,
            "solicitudCodigo" : solicitudCodigo as String,
            "solicitudCodigoEmp" : solicitudCodigoEmp as String,
            "plataforma" : "IOS"
        ]
        
        print(usuarioBean!.UseSeIDSession!)
        OriginData.sharedInstance.obtenerSolicitudVerificacion(notificacion: "endObtenerSolicitudVerificacion", parametros: parametros as NSDictionary)
    }
    
    @objc func endObtenerSolicitudVerificacion(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        //self.hideActivityIndicator(uiView: self.view)
        self.mostrarCargando(mostrar: false)
        
        let data = notification.object as! SolicitudVerificacionBean
        
        if (data.error.count == 0)
        {
            let listaSolicitud = data.result
            
            if (listaSolicitud.count > 0)
            {
                self.solicitudBean = listaSolicitud[0]
                
                GlobalVariables.sharedManager.infoSolicitudVerificacionBean = self.solicitudBean
                
                self.setearDatos()
            }
        }
        else
        {
            let errorBean: ErrorBean = data.error[0]
            
            let alert = UIAlertController(title: Constants.aplicacionNombre, message: errorBean.message, preferredStyle: .alert)
            
            switch errorBean.code {
            case "36", "37", "99":
                
                alert.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                    
                    self.delegate?.changeViewController(menu: LeftMenu.CerrarSesion)
                }))
                break
            case "48":
                
                alert.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                    
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_CODIGO)
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
                    
                    let empresaTipDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
                    let empresaNroDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
                    let solicitudCodigo = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_CODIGO)
                    let solicitudCodigoEmp = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
                    
                    print(empresaTipDoc)
                    print(empresaNroDoc)
                    print(solicitudCodigo)
                    print(solicitudCodigoEmp)
                    
                    self.delegate?.changeViewController(menu: LeftMenu.Home)
                }))
                break
            default:
                
                alert.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                    self.delegate?.changeViewController(menu: LeftMenu.Home)
                }))
                break
            }
            self.present(alert, animated: true, completion: nil)
        
    
            
//            self.mostrarAlertaErrorWS(errorBean.code, mensaje: errorBean.message)
//            
//            AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_EMPRESA_TIPO_DOC, value: "")
//            AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_EMPRESA_NRO_DOC, value: "")
//            AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_CODIGO, value: "")
//            AccessUserDefaults.setUserDefaults(defaultsKeys.SOL_EMPRESA_CODIGO, value: "")
            
            //delegate?.changeViewController(LeftMenu.Home)
        }
        
    }
    
    func iniInsertarEvento(){
        
        TextFileHelper.writeToLogFile(value: "func iniInsertarEvento.")
        
        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AlertaViewController.endInsertarEvento(notification:)), name:NSNotification.Name(rawValue: "endInsertarEvento"), object: nil)
        
        if (self.valorEventoActual == tipoEvento.FUEPER)
        {
            NotificationCenter.default.addObserver(self, selector: #selector(AlertaViewController.endInternetConnectionEvento(notification:)), name:NSNotification.Name(rawValue: "endInternetConnectionEvento"), object: nil)
            
            let parametros = [
                "usuario"   :  self.usuarioBean!.UseNroDoc! as String,
                "sesionId"  :  self.usuarioBean!.UseSeIDSession! as String,
                "empresaTipDoc" :  self.solicitudBean.empresaTipDoc! as String,
                "empresaNroDoc" :  self.solicitudBean.empresaNroDoc! as String,
                "solicitudCodigo"   :  self.solicitudBean.solicitud! as String,
                "solicitudCodigoEmp"   :  self.solicitudBean.solicitudEmpresa! as String,
                "tipo" :  self.valorEventoActual as String,
                "latitud" : String(format: "%.14f", self.actualLocation.coordinate.latitude),
                "longitud" :  String(format: "%.14f", self.actualLocation.coordinate.longitude),
                "plataforma" :  "IOS"
            ]
            
            OriginData.sharedInstance.insertarEvento(notificacion: "endInsertarEvento", parametros: parametros as NSDictionary, notificationConnectivity: "endInternetConnectionEvento")
        }
        else
        {
            let parametros = [
                "usuario"   :  self.usuarioBean!.UseNroDoc! as String,
                "sesionId"  :  self.usuarioBean!.UseSeIDSession! as String,
                "empresaTipDoc" :  self.solicitudBean.empresaTipDoc! as String,
                "empresaNroDoc" :  self.solicitudBean.empresaNroDoc! as String,
                "solicitudCodigo"   :  self.solicitudBean.solicitud! as String,
                "solicitudCodigoEmp"   :  self.solicitudBean.solicitudEmpresa! as String,
                "tipo" :  self.valorEventoActual as String,
                "latitud" : String(format: "%.14f", self.actualLocation.coordinate.latitude),
                "longitud" :  String(format: "%.14f", self.actualLocation.coordinate.longitude),
                "plataforma" :  "IOS"
            ]
            
            OriginData.sharedInstance.insertarEvento(notificacion: "endInsertarEvento", parametros: parametros as NSDictionary)
        }
    }
    
    @objc func endInsertarEvento(notification: NSNotification){
        
        TextFileHelper.writeToLogFile(value: "func endInsertarEvento.")
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as! ResultadoBean
        
        if (data.error.count == 0)
        {
            switch self.alertaActiva {
            case 1:
                self.activarSonidoAlertaPresionada()
                
                UIApplication.shared.cancelAllLocalNotifications()
                UIApplication.shared.applicationIconBadgeNumber = 0
                
                self.iniObtenerSolicitudVerificacion()
                
                break
                
            case 2:
                self.activarSonidoAlertaPresionada()
                
                UIApplication.shared.cancelAllLocalNotifications()
                UIApplication.shared.applicationIconBadgeNumber = 0
                
                self.iniObtenerSolicitudVerificacion()
                
                break
                
            case 3:
                
                self.btnCampana3.backgroundColor = UIColorFromHex(rgbValue: 0x005BA5)
                self.imgCampana3.image = UIImage(named:"ic_check_grande")
                self.campana3 = "azul"
                
                self.activarSonidoAlertaPresionada()
                
                self.callBackgroundTaskTiempoEspera(activar: false)
                
                UIApplication.shared.cancelAllLocalNotifications()
                UIApplication.shared.applicationIconBadgeNumber = 0
                
                self.esSolicitudTerminada = true
                
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC, value: "")
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC, value: "")
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_CODIGO, value: "")
                AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO, value: "")
                
                delegate?.changeViewController(menu: LeftMenu.ConfirmarDomicilio)
                
                break
                
            case 0:
                
                self.callBackgroundTaskEnProceso(activar: false)
                self.callBackgroundTaskPerimetro(activar: false)
                self.callBackgroundTaskAlertas(activar: false)
                self.callBackgroundTaskTiempoEspera(activar: false)
                
                switch UIApplication.shared.applicationState {
                case .active:
                    TextFileHelper.writeToLogFile(value: "Background task Tiempo Espera is active.")
                case .background:
                    TextFileHelper.writeToLogFile(value: "Background task Tiempo Espera is backgrounded.")
                    self.perimetroLocalNotification()
                case .inactive:
                    TextFileHelper.writeToLogFile(value: "Background task Tiempo Espera is inactive.")
                    self.perimetroLocalNotification()
                    break
                }
                
                self.present(self.alertaFueraPerimetro, animated: true, completion: nil)
                
                break
                
            case -1:
                
                self.callBackgroundTaskEnProceso(activar: false)
                self.callBackgroundTaskPerimetro(activar: false)
                self.callBackgroundTaskAlertas(activar: false)
                self.callBackgroundTaskTiempoEspera(activar: false)
                
                self.delegate?.changeViewController(menu: LeftMenu.Alertas)
                
                break
                
            case -2:
                
                self.callBackgroundTaskEnProceso(activar: false)
                self.callBackgroundTaskPerimetro(activar: false)
                self.callBackgroundTaskAlertas(activar: false)
                self.callBackgroundTaskTiempoEspera(activar: false)
                
                self.delegate?.changeViewController(menu: LeftMenu.Home)
                
                break
                
            default:
                break
            }
        }
        else
        {
            let errorBean: ErrorBean = data.error[0] as! ErrorBean
            
            //self.mostrarAlertaErrorWS(errorBean.code, mensaje: errorBean.message, menu: LeftMenu.alertas)
            
            let alert = UIAlertController(title: Constants.aplicacionNombre, message: errorBean.message, preferredStyle: .alert)
            
            switch errorBean.code {
            case "36", "37", "99":
                
                alert.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                    
                    self.delegate?.changeViewController(menu: LeftMenu.CerrarSesion)
                }))
                break
            case "48":
                
                alert.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                    
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_CODIGO)
                    AccessUserDefaults.removeUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
                    
                    let empresaTipDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_TIPO_DOC)
                    let empresaNroDoc = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_NRO_DOC)
                    let solicitudCodigo = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_CODIGO)
                    let solicitudCodigoEmp = AccessUserDefaults.getUserDefaults(key: defaultsKeys.SOL_EMPRESA_CODIGO)
                    
                    print(empresaTipDoc)
                    print(empresaNroDoc)
                    print(solicitudCodigo)
                    print(solicitudCodigoEmp)
                    
                    self.delegate?.changeViewController(menu: LeftMenu.Home)
                }))
                break
            default:
                
                alert.addAction(UIAlertAction(title: "Terminar", style: .cancel, handler: { (action: UIAlertAction!) in
                    self.delegate?.changeViewController(menu: LeftMenu.Home)
                }))
                break
            }
            self.present(alert, animated: true, completion: nil)            
        }
    }
    
    @objc func endInternetConnectionEvento(notification: NSNotification)
    {
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.present(self.alertaFueraPerimetroSinInternet, animated: true, completion: nil)
        
        self.callBackgroundTaskPerimetro(activar: false)
        self.callBackgroundTaskAlertas(activar: false)
        self.callBackgroundTaskTiempoEspera(activar: false)
    }
    
    // MARK: - CLLocationManagerDelegate Methods
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if (status == CLAuthorizationStatus.authorizedAlways)
        {
            print("GPS habilitado siempre.")
        }
        else if (status == CLAuthorizationStatus.authorizedWhenInUse)
        {
            print("GPS habilitado cuando esté en uso.")
        }
        else if (status == CLAuthorizationStatus.denied)
        {
            print("GPS denegado.")
            //UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)!);
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        //let longitud = location!.coordinate.longitude
        //let latitud = location!.coordinate.latitude
        
        //print("Alertas lon: \(longitud) y lat: \(latitud)")
        
        self.actualLocation = location!
    }
    
    @IBAction func parlanteAction(sender: UIButton) {
        
        if (self.audioPlayer.isPlaying == false)
        {
            self.audioPlayer.play()
            let image1:UIImage = UIImage(named: "ic_pause")!
            self.parlanteBT.setImage(image1, for: .normal)
            
        }
        else
        {
            self.pauseAudio()
            let image1:UIImage = UIImage(named: "ic_play")!
            self.parlanteBT.setImage(image1, for: .normal)
        }
    }
}
