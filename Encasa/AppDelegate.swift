//
//  AppDelegate.swift
//  Encasa
//
//  Created by Rommy Fuentes on 26/05/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
import CoreData
import GoogleMaps
@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        TextFileHelper.writeToLogFile(value: "func application didFinishLaunchingWithOptions")
            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        
            self.registerForPushNotifications(application: application)
            GMSServices.provideAPIKey("AIzaSyDipWRBn3pBahAt9wC3PHfYFoHsrwyPNtI")
            return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        //TextFileHelper.writeToLogFile("func applicationWillResignActive")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //TextFileHelper.writeToLogFile("func applicationDidEnterBackground")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
        //TextFileHelper.writeToLogFile("func applicationWillEnterForeground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //TextFileHelper.writeToLogFile("func applicationDidBecomeActive")
    }
    
    func applicationWillTerminate(_ application: UIApplication){
        TextFileHelper.writeToLogFile(value: "func applicationWillTerminate")
        UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func isMultitaskingSupported() -> Bool
    {
        return UIDevice.current.isMultitaskingSupported
    }
    
    // MARK: Register for push
    func registerForPushNotifications(application: UIApplication) {
        let viewAction = UIMutableUserNotificationAction()
        viewAction.identifier = "VIEW_IDENTIFIER"
        viewAction.title = "View"
        viewAction.activationMode = .foreground
        
        let newsCategory = UIMutableUserNotificationCategory()
        newsCategory.identifier = "NEWS_CATEGORY"
        newsCategory.setActions([viewAction], for: .default)
        
        let categories: Set<UIUserNotificationCategory> = [newsCategory]
        
        let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: categories)
        application.registerUserNotificationSettings(notificationSettings)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types == .alert || notificationSettings.types == .badge || notificationSettings.types == .sound {
            application.registerForRemoteNotifications()
        }
    }
    func application(_ application:UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken:Data) {
        let userToken = AccessUserDefaults.getUserDefaults(key: defaultsKeys.DEVICE_TOKEN_KEY)
        if(userToken == ""){
            AccessUserDefaults.setUserDefaults(key: defaultsKeys.DEVICE_TOKEN_KEY, value: String(data: deviceToken, encoding: .utf8)!)
        }
    }
    func application(_ application:UIApplication, didFailToRegisterForRemoteNotificationsWithError error:Error){
        print("Failed to register:", error)
    }
    func application(_ application:UIApplication, didReceive notification:UILocalNotification){
        print("Received Local Notification:")
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: () -> Void) {
        
        if identifier == "fueraPerimetroSi" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fueraPerimetroSiNotification"), object: nil)
        }
        else if identifier == "fueraPerimetroNo" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fueraPerimetroNoNotification"), object: nil)
        }
        else if identifier == "fueraTiempoSi" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fueraTiempoSitNotification"), object: nil)
        }
        else if identifier == "fueraTiempoNo" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fueraTiempoNoNotification"), object: nil)
        }
        
        completionHandler()
    }

    // implemented in your application delegate
    /*
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("Got token data! \(deviceToken)")
        
        let epicToken:MCEpicToken = MCEpicToken()
        
        var token:String = epicToken.getToken()
        
        if(token == ""){
            
            token = NSString(format:"%@", deviceToken) as String
            
            epicToken.setToken(token)
            
            if(epicToken.isInServer() == false){
                
                self.iniSetToken(epicToken.getToken())
                /*
                NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.endSetToken(_:)), name:"endSetToken", object: nil)
                
                let parametros = [
                    "UseTokenApp" : epicToken.getToken() as String,
                    ]
                OriginData.sharedInstance.setearToken("endSetToken", parametros: parametros)
                */
            }else{
                
                NSLog("el token ya esta en el servidor");
            }
            
        }else{
            
            if(epicToken.isInServer() == false){
                
                
                
            }else{
                
                NSLog("el token ya esta en el servidor");
            }
            
        }
        
    }
    */

    /*
    func iniSetToken(token: String){
    
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.endSetToken(_:)), name:"endSetToken", object: nil)
        
        let parametros = [
            //"UseTokenApp" : token as String,
            "usuario" : "46287024",
            "sesionId" : "44683890429354065909",
            "tokenId" : "xxxxxxxxxxxxxxxxxxxxx",
            "so" : "IOS",
            "appId" : "4"
            ]
        OriginData.sharedInstance.setearToken("endSetToken", parametros: parametros)
    }
    
    func endSetToken(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        if (notification.object != nil)
        {
            //let noti = notification.object as! NotificationPush
            
            /*
            var epicToken:MCEpicToken?
            
            if (noti.Existe == "S") {
                
                epicToken?.inServer(true)
                
            }else{
                
                epicToken?.inServer(true)
            }
             */
        }
    }
    */

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        TextFileHelper.writeToLogFile(value: "func application didReceiveRemoteNotification")
    }
    
    // MARK: Handle notifications
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        TextFileHelper.writeToLogFile(value: "func application didReceiveRemoteNotification fetchCompletionHandler")
        completionHandler(.newData)
    }
    // MARK: Handle notification action
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        TextFileHelper.writeToLogFile(value: "func application identifier forRemoteNotification")
        completionHandler()
        
    }
    
    func irValidaciones(codigo_solicitud: String)
    {
        if (codigo_solicitud != "")
        {
            AccessUserDefaults.setUserDefaults(key: defaultsKeys.SOL_CODIGO, value: codigo_solicitud)
        }
    }
    
    func loadViewFromPush(userInfo:NSDictionary){
        
        UIApplication.shared.cancelAllLocalNotifications()
        
        let ls_data = userInfo.object(forKey: "data")! as! String
        
        print(ls_data)
        
        let notification = (userInfo.object(forKey: "aps") as! [String: Any])["alert"] as! String
        
        let alertController = UIAlertController(title: "Sentinel", message: notification, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            print("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction!) in
            print("you have pressed OK button");
        }
        alertController.addAction(OKAction)
    }
}
